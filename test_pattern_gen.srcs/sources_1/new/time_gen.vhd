----------------------------------------------------------------------------------
-- Company:  None
-- Engineer: Eric Wong
-- 
-- Create Date: 17.05.2020 12:31:42
-- Design Name: time_gen_old
-- Module Name: time_gen - Behavioral
-- Project Name: Checkered TPG
-- Target Devices: ALINX (AX7035) Artix 7 (XC7A35T FGG484-1)
-- Tool Versions: 
-- Description: 
--  This align the video data according to the CEA861D format. Note that
--  it is not tested when the the video source is faulty. The reset in this timing
--  generater should be connected to the reset in the TPG.
--  
--  Default resolution is 1280 x 780.
--
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: 
--  Upgrades should be made to account for faulty video sources and mechanics to
--  deal with misalignment.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity time_gen is
    port(
        pixel_clk_i : in std_logic;    -- System clock (1x)
        rst_i : in std_ulogic;          -- Reset (Active Low)
        tdata_i : in std_ulogic_vector (23 downto 0);   -- 24-bit RGB data
        tuser_i : in std_ulogic;        -- Signal high when first pixel of a frame
        tlast_i : in std_ulogic;        -- Signal high when last pixel of the line
        tvalid_i : in std_ulogic;       -- Signal high when data is valid
        
        tdata_o : out std_ulogic_vector (23 downto 0);  -- 24-bit RGB data output to TMDS encoder
        hsync_o : out std_ulogic;       -- hsync following CEA861D format
        vsync_o : out std_ulogic;       -- vsync following CEA861D format
        data_en_o : out std_ulogic;     -- Data enable when signal high
        tready_o : out std_ulogic       -- Ready to accept data from video source
    );
end time_gen;

architecture rtl of time_gen is
    -- Hsync setting (Follows CEA861D format):
    constant hsync_cyc_c : natural := 1650;     -- Hsync cycle
    constant hsync_front_c : natural := 110;    -- Hsync front porch
    constant hsync_period_c : natural := 40;    -- Hsync signal high length
    constant hsync_back_c : natural := 220;     -- Hsync back porch
    constant hsync_total_c : natural := hsync_front_c + hsync_period_c + hsync_back_c;  -- Sum of front, period and back
    
    -- Vsync setting (Follows CEA861D format):
    constant vsync_cyc_c : natural := 750;     -- Vsync cycle
    constant vsync_front_c : natural := 5;      -- Vsync front porch
    constant vsync_period_c : natural := 5;     -- Vsync signal high length
    constant vsync_back_c : natural := 20;      -- Vsync back porch
    constant vsync_total_c : natural := vsync_front_c + vsync_period_c + vsync_back_c;    -- Sum of front, period and back

    -- Counter for hsync and vsync:
    signal hsync_cyc_s : natural range 0 to hsync_cyc_c;
    signal vsync_cyc_s : natural range 0 to vsync_cyc_c;
    
    attribute mark_debug : string;
    attribute keep : string;
    attribute mark_debug of hsync_cyc_s     : signal is "true";
    attribute mark_debug of vsync_cyc_s  : signal is "true";

begin
    -- This process updates the hsync and vsync cycle counter:
    p_update : process (pixel_clk_i, rst_i) is
    begin
        if rst_i = '0' then
            hsync_cyc_s <= hsync_cyc_c - 1;
            vsync_cyc_s <= vsync_cyc_c - 1;
        
        elsif rising_edge(pixel_clk_i) then
            if hsync_cyc_s = hsync_cyc_c - 1 then
                hsync_cyc_s <= 0;

                if vsync_cyc_s = vsync_cyc_c - 1 then
                    vsync_cyc_s <= 0;
                else 
                    vsync_cyc_s <= vsync_cyc_s + 1;
                end if;
            else
                hsync_cyc_s <= hsync_cyc_s + 1;
            end if;
        end if;
    end process p_update;

    -- This process sets the hsync signal accoriding to the CEA861D format:
    p_hsync : process (hsync_cyc_s) is
    begin
        if hsync_cyc_s >= hsync_front_c and hsync_cyc_s < hsync_front_c + hsync_period_c then
            hsync_o <= '1';
        else
            hsync_o <= '0';
        end if;
    end process p_hsync;

    -- This process sets the vsync signal accoriding to the CEA861D format:
    p_vsync : process (hsync_cyc_s, vsync_cyc_s) is
    begin
        if hsync_cyc_s = hsync_front_c then
            if vsync_cyc_s = vsync_front_c then
                vsync_o <= '1';
            elsif vsync_cyc_s = vsync_front_c + vsync_period_c then
                vsync_o <= '0';
            end if;
        elsif vsync_cyc_s = 0 then
            vsync_o <= '0';
        end if;
    end process p_vsync;

    -- This process sets the data enable signal accoriding to the CEA861D format:
    p_data_en : process (hsync_cyc_s, vsync_cyc_s) is
    begin
        if hsync_cyc_s = 0 then
            data_en_o <= '0';
        elsif hsync_cyc_s = hsync_total_c then
            if vsync_cyc_s >= vsync_total_c then
                data_en_o <= '1';
            end if;
        end if;
    end process p_data_en;
    
    -- This process decides when to tell the video source 
    -- (TPG in our case) time gen is accepting data:
    p_ready : process (hsync_cyc_s, vsync_cyc_s) is
    begin
        if hsync_cyc_s = hsync_cyc_c - 2 then
            tready_o <= '0';
        elsif hsync_cyc_s = hsync_total_c - 2 then
            if vsync_cyc_s >= vsync_total_c then
                tready_o <= '1';
            end if;
        end if;
    end process p_ready;
    
    -- This process connect data input to data output:
    p_data : process (hsync_cyc_s) is
    begin
        if tvalid_i = '1' then
            tdata_o <= tdata_i;
        else
        end if;
    end process p_data;

end rtl;
