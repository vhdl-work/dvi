----------------------------------------------------------------------------------
-- Company:  None
-- Engineer: Eric Wong
-- 
-- Create Date: 02.05.2020 12:35:59
-- Design Name: tpg
-- Module Name: test_pattern_generator - Behavioral
-- Project Name: Checkered TPG
-- Target Devices: ALINX (AX7035) Artix 7 (XC7A35T FGG484-1)
-- Tool Versions: 
-- Description: 
--  This is a test pattern genenrator. Aims at producing a checkered pattern.
--  Follows AXI stream standard.
--
--  Default resolution is 1280 x 780.
--
-- Dependencies: 
-- 
-- Revision:
-- Revision 1.00   -- Produced a working test pattern video source.
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity test_pattern_generator is
    port(
        pixel_clk_i : in std_logic;    -- System clock (1x)
        rst_i : in std_ulogic;          -- Reset
        tready_i : in std_ulogic;       -- Signal high indicate ready to receive next pixel
        tdata_o : out std_ulogic_vector (23 downto 0);  -- 24-bit RGB data
        tuser_o : out std_ulogic;       -- Signal high indicates it is the first pixel of a frame
        tlast_o : out std_ulogic;       -- Signal high indicates it is the last pixel of the line
        tvalid_o : out std_ulogic       -- Signal high indicates data is valid
    );
end test_pattern_generator;

architecture rtl of test_pattern_generator is
    -- Resolution:
    constant width_c : natural := 1280;  -- Width of the frame.
    constant height_c : natural := 720; -- Height of the frame.

    -- Pixel coordinate:
    signal width_s : natural range 0 to width_c; -- The horizontal coordinate of the concerned pixel.
    signal height_s : natural range 0 to height_c; -- The vertical coordinate of the concerned pixel.

    -- Pattern parameters:
    constant square_c : natural := 40;  -- Width of the checkered box test pattern.
    constant fr_cnt_c : natural := 6* square_c - 1;
    signal fr_cnt_s : natural range 0 to fr_cnt_c;

begin
    -- This process updates the coordinate of the concerned pixel if tready is high:
    p_coordinate : process (pixel_clk_i, rst_i) is
    begin
        if rst_i = '0' then
            -- Resetting the coordinate of the display:
            width_s <= width_c - 1;
            height_s <= height_c - 1;
            fr_cnt_s <= 3* square_c - 1;

            -- Resetting the AXI stream standard parameters:
            tuser_o <= '0';
            tlast_o <= '0';
            tvalid_o <= '0';

        elsif rising_edge(pixel_clk_i) then
            -- Setting tlast and tuser low unless conditions are met:
            tlast_o <= '0';
            tuser_o <= '0';

            -- Condition for tuser and tlast to be high:
            if width_s = width_c - 1 then   
                if height_s = height_c - 1 then
                    tuser_o <= '1';
                end if;
            elsif width_s = width_c - 2 then
                tlast_o <= '1';
            end if;

            -- We set tvalid to be high all the time (for simplicity):
            tvalid_o <= '1';
            
            -- If tready is high, we will go to the next pixel:
            if tready_i = '1' then
                width_s <= width_s + 1;
                
                -- When we reach the edge, we move to the beginning of the next line:
                if width_s = width_c - 1 then
                    width_s <= 0;
                    height_s <= height_s + 1;
                    
                    -- When we reach the bottom of the screen, we move back to the fist line:
                    if height_s = height_c - 1 then
                        height_s <= 0;
                        fr_cnt_s <= fr_cnt_s + 1;
                        if fr_cnt_s = fr_cnt_c then
                            fr_cnt_s <= 0;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process p_coordinate;

    -- If the width is updated, we generate required data for our test pattern.
    p_pattern : process (width_s, height_s, rst_i) is
    begin
        if rst_i = '0' then
            tdata_o <= (others => '0');
        else
            tdata_o <= (others => '0');

            if ((width_s + fr_cnt_s)/ square_c) mod 2 = 0 then
                if ((height_s + fr_cnt_s) / square_c) mod 2 = 0 then
                    tdata_o (23 downto 16) <= (others => '1');
                else
                    tdata_o (15 downto 8) <= (others => '1');
                end if;
            else
                if ((height_s + fr_cnt_s) / square_c) mod 2 = 0 then
                    tdata_o (7 downto 0) <= (others => '1');
                end if;
            end if;
        end if;
    end process p_pattern;
end rtl;
