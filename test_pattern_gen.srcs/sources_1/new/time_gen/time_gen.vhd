----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.05.2020 12:31:42
-- Design Name: 
-- Module Name: time_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity time_gen is
    port(
        pixel_clk_i : in std_logic;    -- System clock (1x)
        rst_i : in std_ulogic;          -- Reset (Active Low)
        tdata_i : in std_ulogic_vector (23 downto 0);   -- 24-bit RGB data
        tuser_i : in std_ulogic;        -- Signal high when first pixel of a frame
        tlast_i : in std_ulogic;        -- Signal high when last pixel of the line
        tvalid_i : in std_ulogic;       -- Signal high when data is valid
        
        tdata_o : out std_ulogic_vector (23 downto 0);  -- 24-bit RGB data output to TMDS encoder
        hsync_o : out std_ulogic;       -- hsync following CEA861D format
        vsync_o : out std_ulogic;       -- vsync following CEA861D format
        data_en_o : out std_ulogic;     -- Data enable when signal high
        tready_o : out std_ulogic       -- Ready to accept data from video source
    );
end time_gen;

architecture rtl of time_gen is

     -- Hsync setting (Follows CEA861D format):
    constant hsync_cyc_c : natural := 1650;     -- Hsync cycle
    constant hsync_front_c : natural := 110;     -- Hsync front porch
    constant hsync_period_c : natural := 40;    -- Hsync signal high length
    constant hsync_back_c : natural := 220;     -- Hsync back porch
    constant hsync_total_c : natural := hsync_front_c + hsync_period_c + hsync_back_c;  -- Sum of front, period and back
    
    -- Vsync setting (Follows CEA861D format):
    constant vsync_cyc_c : natural := 750;     -- Vsync cycle
    constant vsync_front_c : natural := 5;      -- Vsync front porch
    constant vsync_period_c : natural := 5;     -- Vsync signal high length
    constant vsync_back_c : natural := 20;      -- Vsync back porch
    constant vsync_total_c : natural := vsync_front_c + vsync_period_c + vsync_back_c;    -- Sum of front, period and back

    signal hsync_cyc_s : natural range 0 to hsync_cyc_c;
    signal vsync_cyc_s : natural range 0 to vsync_cyc_c;
    
    attribute mark_debug : string;
    attribute keep : string;
    attribute mark_debug of hsync_cyc_s     : signal is "true";
    attribute mark_debug of vsync_cyc_s  : signal is "true";
    
    signal tdata_s : std_ulogic_vector (23 downto 0);
    
    signal tready_s : std_ulogic;
    signal src_rdy_s : std_ulogic;
    signal wait_s : std_ulogic;
    signal store_s : std_ulogic;

begin
    p_main : process (pixel_clk_i, rst_i) is
    begin
        if rst_i = '0' then
            tready_o <= '0';
            tready_s <= '0';
            data_en_o <= '0';
            hsync_o <= '0';
            vsync_o <= '0';
            hsync_cyc_s <= 0;
            vsync_cyc_s <= 0;
            src_rdy_s <= '1';
            wait_s <= '0';
            store_s <= '0';

        elsif rising_edge(pixel_clk_i) then
            hsync_cyc_s <= hsync_cyc_s + 1;
            hsync_o <= '0';
            data_en_o <= '1';
    
            
            if hsync_cyc_s < hsync_total_c then
                if hsync_cyc_s > hsync_front_c - 1 and hsync_cyc_s < hsync_front_c + hsync_period_c then
                    hsync_o <= '1';
                elsif hsync_cyc_s = hsync_total_c - 2 then
                    if store_s = '0' then
                        tready_o <= '1';
                        tready_s <= '1';
                    end if;
                elsif hsync_cyc_s = hsync_total_c - 1 then
                    if vsync_cyc_s = vsync_total_c and wait_s = '1'then
                        wait_s <= '0';
                    end if;
                end if;
                data_en_o <= '0';
            
            elsif hsync_cyc_s = hsync_total_c then
                tready_o <= '1';
                tready_s <= '1';
            elsif hsync_cyc_s = hsync_cyc_c - 2 then
                tready_o <= '0';
                tready_s <= '0';
            elsif hsync_cyc_s = hsync_cyc_c - 1 then
                hsync_cyc_s <= 0;
                vsync_cyc_s <= vsync_cyc_s + 1;
            end if;
            
            if vsync_cyc_s < vsync_total_c then
                data_en_o <= '0';
                tready_o <= '0';
                tready_s <= '0';
                if vsync_cyc_s = vsync_front_c and hsync_cyc_s = hsync_front_c then
                    vsync_o <= '1';
                elsif vsync_cyc_s = vsync_front_c + vsync_period_c - 1 and hsync_cyc_s = hsync_front_c + hsync_period_c - 1 then
                    vsync_o <= '0';
                end if;
            elsif vsync_cyc_s = vsync_cyc_c then
                vsync_cyc_s <= 0;
            end if;
            
            if tvalid_i = '1' then
                if src_rdy_s = '0'and wait_s = '0' then
                    tdata_o <= (others => '0');
                    tready_o <= '1';
                    tready_s <= '1';
                    if tuser_i = '1' then
                        src_rdy_s <= '1';
                        wait_s <= '1';
                        store_s <= '1';
                        tdata_s <= tdata_i;
                        tready_o <= '0';
                        tready_s <= '0';
                    end if;
                elsif wait_s = '1' then
                    tdata_o <= (others => '0');
                    tready_o <= '0';
                    tready_s <= '0';
                else
                    if store_s = '1' then
                        tdata_o <= tdata_s;
                        store_s <= '0';
                    else
                        tdata_o <= tdata_i;
                    end if;
                end if;
            else
                if tready_s = '1' then
                    src_rdy_s <= '0';
                end if;
                tdata_o <= (others => '0');
            end if;    
        end if; 
    end process p_main;    
end rtl;
