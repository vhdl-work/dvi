// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Fri Jul 17 19:49:39 2020
// Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode funcsim
//               /mnt/Data1/FPGA_Projects/DVI/test_pattern_gen.srcs/sources_1/bd/design_1/ip/design_1_time_gen_0_0/design_1_time_gen_0_0_sim_netlist.v
// Design      : design_1_time_gen_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_time_gen_0_0,time_gen,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "time_gen,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module design_1_time_gen_0_0
   (pixel_clk_i,
    rst_i,
    tdata_i,
    tuser_i,
    tlast_i,
    tvalid_i,
    tdata_o,
    hsync_o,
    vsync_o,
    data_en_o,
    tready_o);
  input pixel_clk_i;
  input rst_i;
  input [23:0]tdata_i;
  input tuser_i;
  input tlast_i;
  input tvalid_i;
  output [23:0]tdata_o;
  output hsync_o;
  output vsync_o;
  output data_en_o;
  output tready_o;

  wire data_en_o;
  wire hsync_o;
  wire pixel_clk_i;
  wire rst_i;
  wire [23:0]tdata_i;
  wire [23:0]tdata_o;
  wire tready_o;
  wire tvalid_i;
  wire vsync_o;

  design_1_time_gen_0_0_time_gen U0
       (.data_en_o(data_en_o),
        .hsync_o(hsync_o),
        .pixel_clk_i(pixel_clk_i),
        .rst_i(rst_i),
        .tdata_i(tdata_i),
        .tdata_o(tdata_o),
        .tready_o(tready_o),
        .tvalid_i(tvalid_i),
        .vsync_o(vsync_o));
endmodule

(* ORIG_REF_NAME = "time_gen" *) 
module design_1_time_gen_0_0_time_gen
   (tdata_o,
    vsync_o,
    data_en_o,
    tready_o,
    hsync_o,
    tdata_i,
    tvalid_i,
    pixel_clk_i,
    rst_i);
  output [23:0]tdata_o;
  output vsync_o;
  output data_en_o;
  output tready_o;
  output hsync_o;
  input [23:0]tdata_i;
  input tvalid_i;
  input pixel_clk_i;
  input rst_i;

  wire data_en_o;
  wire data_en_o_reg_i_1_n_0;
  wire data_en_o_reg_i_2_n_0;
  wire data_en_o_reg_i_3_n_0;
  wire data_en_o_reg_i_4_n_0;
  wire data_en_o_reg_i_5_n_0;
  wire data_en_o_reg_i_6_n_0;
  wire data_en_o_reg_i_7_n_0;
  wire data_en_o_reg_i_8_n_0;
  (* MARK_DEBUG *) wire [10:0]hsync_cyc_s;
  wire \hsync_cyc_s[10]_i_2_n_0 ;
  wire \hsync_cyc_s[10]_i_3_n_0 ;
  wire \hsync_cyc_s[10]_i_4_n_0 ;
  wire \hsync_cyc_s[10]_i_5_n_0 ;
  wire \hsync_cyc_s[10]_i_6_n_0 ;
  wire \hsync_cyc_s[4]_i_2_n_0 ;
  wire \hsync_cyc_s[5]_i_2_n_0 ;
  wire \hsync_cyc_s[6]_i_2_n_0 ;
  wire \hsync_cyc_s[9]_i_2_n_0 ;
  wire [10:0]hsync_cyc_s__0;
  wire hsync_o;
  wire hsync_o_INST_0_i_1_n_0;
  wire hsync_o_INST_0_i_2_n_0;
  wire hsync_o_INST_0_i_3_n_0;
  wire hsync_o_INST_0_i_4_n_0;
  wire hsync_o_INST_0_i_5_n_0;
  wire hsync_o_INST_0_i_6_n_0;
  wire pixel_clk_i;
  wire rst_i;
  wire [23:0]tdata_i;
  wire [23:0]tdata_o;
  wire tready_o;
  wire tready_o_reg_i_1_n_0;
  wire tready_o_reg_i_2_n_0;
  wire tready_o_reg_i_3_n_0;
  wire tvalid_i;
  (* MARK_DEBUG *) wire [9:0]vsync_cyc_s;
  wire \vsync_cyc_s[0]_i_1_n_0 ;
  wire \vsync_cyc_s[1]_i_1_n_0 ;
  wire \vsync_cyc_s[2]_i_1_n_0 ;
  wire \vsync_cyc_s[3]_i_1_n_0 ;
  wire \vsync_cyc_s[4]_i_1_n_0 ;
  wire \vsync_cyc_s[5]_i_1_n_0 ;
  wire \vsync_cyc_s[5]_i_2_n_0 ;
  wire \vsync_cyc_s[6]_i_1_n_0 ;
  wire \vsync_cyc_s[7]_i_1_n_0 ;
  wire \vsync_cyc_s[7]_i_2_n_0 ;
  wire \vsync_cyc_s[8]_i_1_n_0 ;
  wire \vsync_cyc_s[8]_i_2_n_0 ;
  wire \vsync_cyc_s[9]_i_2_n_0 ;
  wire \vsync_cyc_s[9]_i_3_n_0 ;
  wire \vsync_cyc_s[9]_i_4_n_0 ;
  wire \vsync_cyc_s[9]_i_5_n_0 ;
  wire vsync_cyc_s__0;
  wire vsync_o;
  wire vsync_o_reg_i_1_n_0;
  wire vsync_o_reg_i_2_n_0;
  wire vsync_o_reg_i_3_n_0;
  wire vsync_o_reg_i_4_n_0;
  wire vsync_o_reg_i_5_n_0;
  wire vsync_o_reg_i_6_n_0;
  wire vsync_o_reg_i_7_n_0;

  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    data_en_o_reg
       (.CLR(data_en_o_reg_i_3_n_0),
        .D(data_en_o_reg_i_1_n_0),
        .G(data_en_o_reg_i_2_n_0),
        .GE(1'b1),
        .Q(data_en_o));
  LUT5 #(
    .INIT(32'hFFFF8000)) 
    data_en_o_reg_i_1
       (.I0(vsync_cyc_s[3]),
        .I1(vsync_cyc_s[4]),
        .I2(vsync_cyc_s[1]),
        .I3(vsync_cyc_s[2]),
        .I4(vsync_o_reg_i_5_n_0),
        .O(data_en_o_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    data_en_o_reg_i_2
       (.I0(data_en_o_reg_i_1_n_0),
        .I1(data_en_o_reg_i_4_n_0),
        .I2(hsync_cyc_s[1]),
        .I3(hsync_cyc_s[3]),
        .I4(data_en_o_reg_i_5_n_0),
        .I5(data_en_o_reg_i_6_n_0),
        .O(data_en_o_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000010000)) 
    data_en_o_reg_i_3
       (.I0(data_en_o_reg_i_7_n_0),
        .I1(hsync_cyc_s[1]),
        .I2(hsync_cyc_s[2]),
        .I3(hsync_cyc_s[4]),
        .I4(data_en_o_reg_i_8_n_0),
        .I5(data_en_o_reg_i_5_n_0),
        .O(data_en_o_reg_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    data_en_o_reg_i_4
       (.I0(hsync_cyc_s[8]),
        .I1(hsync_cyc_s[7]),
        .O(data_en_o_reg_i_4_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    data_en_o_reg_i_5
       (.I0(hsync_cyc_s[10]),
        .I1(hsync_cyc_s[9]),
        .I2(hsync_cyc_s[0]),
        .O(data_en_o_reg_i_5_n_0));
  LUT4 #(
    .INIT(16'hFF7F)) 
    data_en_o_reg_i_6
       (.I0(hsync_cyc_s[4]),
        .I1(hsync_cyc_s[5]),
        .I2(hsync_cyc_s[6]),
        .I3(hsync_cyc_s[2]),
        .O(data_en_o_reg_i_6_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    data_en_o_reg_i_7
       (.I0(hsync_cyc_s[8]),
        .I1(hsync_cyc_s[7]),
        .I2(hsync_cyc_s[3]),
        .O(data_en_o_reg_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    data_en_o_reg_i_8
       (.I0(hsync_cyc_s[5]),
        .I1(hsync_cyc_s[6]),
        .O(data_en_o_reg_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \hsync_cyc_s[0]_i_1 
       (.I0(hsync_cyc_s[0]),
        .O(hsync_cyc_s__0[0]));
  LUT6 #(
    .INIT(64'hFF8CFF8CFF8CFFBC)) 
    \hsync_cyc_s[10]_i_1 
       (.I0(\hsync_cyc_s[10]_i_3_n_0 ),
        .I1(hsync_cyc_s[10]),
        .I2(hsync_cyc_s[9]),
        .I3(\hsync_cyc_s[10]_i_4_n_0 ),
        .I4(\hsync_cyc_s[10]_i_5_n_0 ),
        .I5(\hsync_cyc_s[10]_i_6_n_0 ),
        .O(hsync_cyc_s__0[10]));
  LUT1 #(
    .INIT(2'h1)) 
    \hsync_cyc_s[10]_i_2 
       (.I0(rst_i),
        .O(\hsync_cyc_s[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF7CFCFCFC)) 
    \hsync_cyc_s[10]_i_3 
       (.I0(hsync_cyc_s[3]),
        .I1(hsync_cyc_s[2]),
        .I2(hsync_cyc_s[1]),
        .I3(hsync_cyc_s[8]),
        .I4(hsync_cyc_s[7]),
        .I5(\vsync_cyc_s[9]_i_3_n_0 ),
        .O(\hsync_cyc_s[10]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h44444440)) 
    \hsync_cyc_s[10]_i_4 
       (.I0(hsync_cyc_s[2]),
        .I1(hsync_cyc_s[10]),
        .I2(hsync_cyc_s[3]),
        .I3(hsync_cyc_s[7]),
        .I4(hsync_cyc_s[8]),
        .O(\hsync_cyc_s[10]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \hsync_cyc_s[10]_i_5 
       (.I0(hsync_cyc_s[0]),
        .I1(hsync_cyc_s[6]),
        .I2(hsync_cyc_s[5]),
        .I3(hsync_cyc_s[4]),
        .I4(hsync_o_INST_0_i_4_n_0),
        .O(\hsync_cyc_s[10]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \hsync_cyc_s[10]_i_6 
       (.I0(hsync_cyc_s[7]),
        .I1(hsync_cyc_s[8]),
        .O(\hsync_cyc_s[10]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h0FE0)) 
    \hsync_cyc_s[1]_i_1 
       (.I0(data_en_o_reg_i_6_n_0),
        .I1(tready_o_reg_i_3_n_0),
        .I2(hsync_cyc_s[0]),
        .I3(hsync_cyc_s[1]),
        .O(hsync_cyc_s__0[1]));
  LUT3 #(
    .INIT(8'h78)) 
    \hsync_cyc_s[2]_i_1 
       (.I0(hsync_cyc_s[1]),
        .I1(hsync_cyc_s[0]),
        .I2(hsync_cyc_s[2]),
        .O(hsync_cyc_s__0[2]));
  LUT4 #(
    .INIT(16'h7F80)) 
    \hsync_cyc_s[3]_i_1 
       (.I0(hsync_cyc_s[0]),
        .I1(hsync_cyc_s[1]),
        .I2(hsync_cyc_s[2]),
        .I3(hsync_cyc_s[3]),
        .O(hsync_cyc_s__0[3]));
  LUT6 #(
    .INIT(64'hFFFF00F0444400F0)) 
    \hsync_cyc_s[4]_i_1 
       (.I0(hsync_cyc_s[2]),
        .I1(hsync_o_INST_0_i_3_n_0),
        .I2(hsync_cyc_s[0]),
        .I3(hsync_o_INST_0_i_4_n_0),
        .I4(hsync_cyc_s[4]),
        .I5(\hsync_cyc_s[4]_i_2_n_0 ),
        .O(hsync_cyc_s__0[4]));
  LUT5 #(
    .INIT(32'h5FFDFFFD)) 
    \hsync_cyc_s[4]_i_2 
       (.I0(hsync_cyc_s[0]),
        .I1(tready_o_reg_i_3_n_0),
        .I2(hsync_cyc_s[1]),
        .I3(hsync_cyc_s[2]),
        .I4(hsync_cyc_s[3]),
        .O(\hsync_cyc_s[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88B888B888B8CCFC)) 
    \hsync_cyc_s[5]_i_1 
       (.I0(\hsync_cyc_s[6]_i_2_n_0 ),
        .I1(hsync_cyc_s[5]),
        .I2(\hsync_cyc_s[5]_i_2_n_0 ),
        .I3(hsync_o_INST_0_i_4_n_0),
        .I4(hsync_cyc_s[6]),
        .I5(hsync_cyc_s[2]),
        .O(hsync_cyc_s__0[5]));
  LUT2 #(
    .INIT(4'h8)) 
    \hsync_cyc_s[5]_i_2 
       (.I0(hsync_cyc_s[0]),
        .I1(hsync_cyc_s[4]),
        .O(\hsync_cyc_s[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF00800F0F0080)) 
    \hsync_cyc_s[6]_i_1 
       (.I0(hsync_cyc_s[0]),
        .I1(hsync_cyc_s[4]),
        .I2(hsync_cyc_s[5]),
        .I3(hsync_o_INST_0_i_4_n_0),
        .I4(hsync_cyc_s[6]),
        .I5(\hsync_cyc_s[6]_i_2_n_0 ),
        .O(hsync_cyc_s__0[6]));
  LUT6 #(
    .INIT(64'h7F7CFFFFFFFFFFFF)) 
    \hsync_cyc_s[6]_i_2 
       (.I0(hsync_cyc_s[3]),
        .I1(hsync_cyc_s[2]),
        .I2(hsync_cyc_s[1]),
        .I3(tready_o_reg_i_3_n_0),
        .I4(hsync_cyc_s[0]),
        .I5(hsync_cyc_s[4]),
        .O(\hsync_cyc_s[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFF40000000)) 
    \hsync_cyc_s[7]_i_1 
       (.I0(hsync_o_INST_0_i_4_n_0),
        .I1(hsync_cyc_s[4]),
        .I2(hsync_cyc_s[5]),
        .I3(hsync_cyc_s[6]),
        .I4(hsync_cyc_s[0]),
        .I5(hsync_cyc_s[7]),
        .O(hsync_cyc_s__0[7]));
  LUT3 #(
    .INIT(8'hD2)) 
    \hsync_cyc_s[8]_i_1 
       (.I0(hsync_cyc_s[7]),
        .I1(\hsync_cyc_s[10]_i_5_n_0 ),
        .I2(hsync_cyc_s[8]),
        .O(hsync_cyc_s__0[8]));
  LUT6 #(
    .INIT(64'hFFFF000F4444000F)) 
    \hsync_cyc_s[9]_i_1 
       (.I0(hsync_cyc_s[2]),
        .I1(\hsync_cyc_s[9]_i_2_n_0 ),
        .I2(\hsync_cyc_s[10]_i_6_n_0 ),
        .I3(\hsync_cyc_s[10]_i_5_n_0 ),
        .I4(hsync_cyc_s[9]),
        .I5(\hsync_cyc_s[10]_i_3_n_0 ),
        .O(hsync_cyc_s__0[9]));
  LUT4 #(
    .INIT(16'hFEFF)) 
    \hsync_cyc_s[9]_i_2 
       (.I0(hsync_cyc_s[3]),
        .I1(hsync_cyc_s[7]),
        .I2(hsync_cyc_s[8]),
        .I3(hsync_cyc_s[10]),
        .O(\hsync_cyc_s[9]_i_2_n_0 ));
  (* KEEP = "yes" *) 
  FDPE \hsync_cyc_s_reg[0] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .D(hsync_cyc_s__0[0]),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(hsync_cyc_s[0]));
  (* KEEP = "yes" *) 
  FDPE \hsync_cyc_s_reg[10] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .D(hsync_cyc_s__0[10]),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(hsync_cyc_s[10]));
  (* KEEP = "yes" *) 
  FDCE \hsync_cyc_s_reg[1] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(hsync_cyc_s__0[1]),
        .Q(hsync_cyc_s[1]));
  (* KEEP = "yes" *) 
  FDCE \hsync_cyc_s_reg[2] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(hsync_cyc_s__0[2]),
        .Q(hsync_cyc_s[2]));
  (* KEEP = "yes" *) 
  FDCE \hsync_cyc_s_reg[3] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(hsync_cyc_s__0[3]),
        .Q(hsync_cyc_s[3]));
  (* KEEP = "yes" *) 
  FDPE \hsync_cyc_s_reg[4] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .D(hsync_cyc_s__0[4]),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(hsync_cyc_s[4]));
  (* KEEP = "yes" *) 
  FDPE \hsync_cyc_s_reg[5] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .D(hsync_cyc_s__0[5]),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(hsync_cyc_s[5]));
  (* KEEP = "yes" *) 
  FDPE \hsync_cyc_s_reg[6] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .D(hsync_cyc_s__0[6]),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(hsync_cyc_s[6]));
  (* KEEP = "yes" *) 
  FDCE \hsync_cyc_s_reg[7] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(hsync_cyc_s__0[7]),
        .Q(hsync_cyc_s[7]));
  (* KEEP = "yes" *) 
  FDCE \hsync_cyc_s_reg[8] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(hsync_cyc_s__0[8]),
        .Q(hsync_cyc_s[8]));
  (* KEEP = "yes" *) 
  FDPE \hsync_cyc_s_reg[9] 
       (.C(pixel_clk_i),
        .CE(1'b1),
        .D(hsync_cyc_s__0[9]),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(hsync_cyc_s[9]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAEAAAF)) 
    hsync_o_INST_0
       (.I0(hsync_o_INST_0_i_1_n_0),
        .I1(hsync_cyc_s[4]),
        .I2(hsync_o_INST_0_i_2_n_0),
        .I3(hsync_o_INST_0_i_3_n_0),
        .I4(hsync_o_INST_0_i_4_n_0),
        .I5(hsync_o_INST_0_i_5_n_0),
        .O(hsync_o));
  LUT5 #(
    .INIT(32'h22222AAA)) 
    hsync_o_INST_0_i_1
       (.I0(hsync_o_INST_0_i_6_n_0),
        .I1(hsync_cyc_s[4]),
        .I2(hsync_cyc_s[2]),
        .I3(hsync_cyc_s[1]),
        .I4(hsync_cyc_s[3]),
        .O(hsync_o_INST_0_i_1_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    hsync_o_INST_0_i_2
       (.I0(hsync_cyc_s[7]),
        .I1(hsync_cyc_s[8]),
        .O(hsync_o_INST_0_i_2_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    hsync_o_INST_0_i_3
       (.I0(hsync_cyc_s[5]),
        .I1(hsync_cyc_s[6]),
        .O(hsync_o_INST_0_i_3_n_0));
  LUT3 #(
    .INIT(8'h7F)) 
    hsync_o_INST_0_i_4
       (.I0(hsync_cyc_s[3]),
        .I1(hsync_cyc_s[1]),
        .I2(hsync_cyc_s[2]),
        .O(hsync_o_INST_0_i_4_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    hsync_o_INST_0_i_5
       (.I0(hsync_cyc_s[9]),
        .I1(hsync_cyc_s[10]),
        .O(hsync_o_INST_0_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    hsync_o_INST_0_i_6
       (.I0(hsync_cyc_s[7]),
        .I1(hsync_cyc_s[8]),
        .I2(hsync_cyc_s[5]),
        .I3(hsync_cyc_s[6]),
        .I4(hsync_cyc_s[10]),
        .I5(hsync_cyc_s[9]),
        .O(hsync_o_INST_0_i_6_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[0] 
       (.CLR(1'b0),
        .D(tdata_i[0]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[10] 
       (.CLR(1'b0),
        .D(tdata_i[10]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[11] 
       (.CLR(1'b0),
        .D(tdata_i[11]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[12] 
       (.CLR(1'b0),
        .D(tdata_i[12]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[13] 
       (.CLR(1'b0),
        .D(tdata_i[13]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[14] 
       (.CLR(1'b0),
        .D(tdata_i[14]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[15] 
       (.CLR(1'b0),
        .D(tdata_i[15]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[16] 
       (.CLR(1'b0),
        .D(tdata_i[16]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[17] 
       (.CLR(1'b0),
        .D(tdata_i[17]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[18] 
       (.CLR(1'b0),
        .D(tdata_i[18]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[19] 
       (.CLR(1'b0),
        .D(tdata_i[19]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[1] 
       (.CLR(1'b0),
        .D(tdata_i[1]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[20] 
       (.CLR(1'b0),
        .D(tdata_i[20]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[21] 
       (.CLR(1'b0),
        .D(tdata_i[21]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[22] 
       (.CLR(1'b0),
        .D(tdata_i[22]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[23] 
       (.CLR(1'b0),
        .D(tdata_i[23]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[2] 
       (.CLR(1'b0),
        .D(tdata_i[2]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[3] 
       (.CLR(1'b0),
        .D(tdata_i[3]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[4] 
       (.CLR(1'b0),
        .D(tdata_i[4]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[5] 
       (.CLR(1'b0),
        .D(tdata_i[5]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[6] 
       (.CLR(1'b0),
        .D(tdata_i[6]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[7] 
       (.CLR(1'b0),
        .D(tdata_i[7]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[8] 
       (.CLR(1'b0),
        .D(tdata_i[8]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \tdata_o_reg[9] 
       (.CLR(1'b0),
        .D(tdata_i[9]),
        .G(tvalid_i),
        .GE(1'b1),
        .Q(tdata_o[9]));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    tready_o_reg
       (.CLR(tready_o_reg_i_2_n_0),
        .D(data_en_o_reg_i_1_n_0),
        .G(tready_o_reg_i_1_n_0),
        .GE(1'b1),
        .Q(tready_o));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    tready_o_reg_i_1
       (.I0(data_en_o_reg_i_1_n_0),
        .I1(data_en_o_reg_i_4_n_0),
        .I2(hsync_cyc_s[1]),
        .I3(hsync_cyc_s[3]),
        .I4(data_en_o_reg_i_5_n_0),
        .I5(data_en_o_reg_i_6_n_0),
        .O(tready_o_reg_i_1_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    tready_o_reg_i_2
       (.I0(hsync_cyc_s[1]),
        .I1(hsync_cyc_s[0]),
        .I2(tready_o_reg_i_3_n_0),
        .I3(data_en_o_reg_i_6_n_0),
        .O(tready_o_reg_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFDFFFF)) 
    tready_o_reg_i_3
       (.I0(hsync_cyc_s[10]),
        .I1(hsync_cyc_s[8]),
        .I2(hsync_cyc_s[7]),
        .I3(hsync_cyc_s[3]),
        .I4(hsync_cyc_s[9]),
        .O(tready_o_reg_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \vsync_cyc_s[0]_i_1 
       (.I0(vsync_cyc_s[0]),
        .O(\vsync_cyc_s[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFFF7FF0000)) 
    \vsync_cyc_s[1]_i_1 
       (.I0(vsync_cyc_s[3]),
        .I1(vsync_cyc_s[5]),
        .I2(\vsync_cyc_s[5]_i_2_n_0 ),
        .I3(vsync_cyc_s[2]),
        .I4(vsync_cyc_s[0]),
        .I5(vsync_cyc_s[1]),
        .O(\vsync_cyc_s[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h5515FFFFAAAA0000)) 
    \vsync_cyc_s[2]_i_1 
       (.I0(vsync_cyc_s[1]),
        .I1(vsync_cyc_s[3]),
        .I2(vsync_cyc_s[5]),
        .I3(\vsync_cyc_s[5]_i_2_n_0 ),
        .I4(vsync_cyc_s[0]),
        .I5(vsync_cyc_s[2]),
        .O(\vsync_cyc_s[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h51AAFF00FF00FF00)) 
    \vsync_cyc_s[3]_i_1 
       (.I0(vsync_cyc_s[1]),
        .I1(vsync_cyc_s[5]),
        .I2(\vsync_cyc_s[5]_i_2_n_0 ),
        .I3(vsync_cyc_s[3]),
        .I4(vsync_cyc_s[0]),
        .I5(vsync_cyc_s[2]),
        .O(\vsync_cyc_s[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \vsync_cyc_s[4]_i_1 
       (.I0(vsync_cyc_s[1]),
        .I1(vsync_cyc_s[2]),
        .I2(vsync_cyc_s[0]),
        .I3(vsync_cyc_s[3]),
        .I4(vsync_cyc_s[4]),
        .O(\vsync_cyc_s[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCCCCC62C8CCCC)) 
    \vsync_cyc_s[5]_i_1 
       (.I0(vsync_cyc_s[1]),
        .I1(vsync_cyc_s[5]),
        .I2(\vsync_cyc_s[5]_i_2_n_0 ),
        .I3(vsync_cyc_s[4]),
        .I4(vsync_cyc_s[3]),
        .I5(vsync_o_reg_i_6_n_0),
        .O(\vsync_cyc_s[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFDFFFFFF)) 
    \vsync_cyc_s[5]_i_2 
       (.I0(vsync_cyc_s[9]),
        .I1(vsync_cyc_s[4]),
        .I2(vsync_cyc_s[8]),
        .I3(vsync_cyc_s[7]),
        .I4(vsync_cyc_s[6]),
        .O(\vsync_cyc_s[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0F0F00FF0B0B0)) 
    \vsync_cyc_s[6]_i_1 
       (.I0(\vsync_cyc_s[7]_i_2_n_0 ),
        .I1(vsync_cyc_s[7]),
        .I2(vsync_cyc_s[6]),
        .I3(vsync_cyc_s[4]),
        .I4(vsync_cyc_s[1]),
        .I5(\vsync_cyc_s[9]_i_4_n_0 ),
        .O(\vsync_cyc_s[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF7F4FFFF08080000)) 
    \vsync_cyc_s[7]_i_1 
       (.I0(vsync_cyc_s[4]),
        .I1(vsync_cyc_s[1]),
        .I2(\vsync_cyc_s[9]_i_4_n_0 ),
        .I3(\vsync_cyc_s[7]_i_2_n_0 ),
        .I4(vsync_cyc_s[6]),
        .I5(vsync_cyc_s[7]),
        .O(\vsync_cyc_s[7]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hEF)) 
    \vsync_cyc_s[7]_i_2 
       (.I0(vsync_cyc_s[8]),
        .I1(vsync_cyc_s[4]),
        .I2(vsync_cyc_s[9]),
        .O(\vsync_cyc_s[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF708)) 
    \vsync_cyc_s[8]_i_1 
       (.I0(vsync_cyc_s[6]),
        .I1(vsync_cyc_s[7]),
        .I2(\vsync_cyc_s[8]_i_2_n_0 ),
        .I3(vsync_cyc_s[8]),
        .O(\vsync_cyc_s[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \vsync_cyc_s[8]_i_2 
       (.I0(vsync_cyc_s[4]),
        .I1(vsync_cyc_s[1]),
        .I2(vsync_cyc_s[0]),
        .I3(vsync_cyc_s[2]),
        .I4(vsync_cyc_s[3]),
        .I5(vsync_cyc_s[5]),
        .O(\vsync_cyc_s[8]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \vsync_cyc_s[9]_i_1 
       (.I0(\vsync_cyc_s[9]_i_3_n_0 ),
        .I1(hsync_cyc_s[1]),
        .I2(hsync_cyc_s[2]),
        .I3(tready_o_reg_i_3_n_0),
        .O(vsync_cyc_s__0));
  LUT6 #(
    .INIT(64'hFFBFFFFE00400000)) 
    \vsync_cyc_s[9]_i_2 
       (.I0(\vsync_cyc_s[9]_i_4_n_0 ),
        .I1(vsync_cyc_s[1]),
        .I2(vsync_cyc_s[4]),
        .I3(\vsync_cyc_s[9]_i_5_n_0 ),
        .I4(vsync_cyc_s[8]),
        .I5(vsync_cyc_s[9]),
        .O(\vsync_cyc_s[9]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \vsync_cyc_s[9]_i_3 
       (.I0(hsync_cyc_s[4]),
        .I1(hsync_cyc_s[5]),
        .I2(hsync_cyc_s[6]),
        .I3(hsync_cyc_s[0]),
        .O(\vsync_cyc_s[9]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \vsync_cyc_s[9]_i_4 
       (.I0(vsync_cyc_s[5]),
        .I1(vsync_cyc_s[3]),
        .I2(vsync_cyc_s[2]),
        .I3(vsync_cyc_s[0]),
        .O(\vsync_cyc_s[9]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \vsync_cyc_s[9]_i_5 
       (.I0(vsync_cyc_s[6]),
        .I1(vsync_cyc_s[7]),
        .O(\vsync_cyc_s[9]_i_5_n_0 ));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDPE \vsync_cyc_s_reg[0] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .D(\vsync_cyc_s[0]_i_1_n_0 ),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(vsync_cyc_s[0]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDCE \vsync_cyc_s_reg[1] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(\vsync_cyc_s[1]_i_1_n_0 ),
        .Q(vsync_cyc_s[1]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDPE \vsync_cyc_s_reg[2] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .D(\vsync_cyc_s[2]_i_1_n_0 ),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(vsync_cyc_s[2]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDPE \vsync_cyc_s_reg[3] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .D(\vsync_cyc_s[3]_i_1_n_0 ),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(vsync_cyc_s[3]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDCE \vsync_cyc_s_reg[4] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(\vsync_cyc_s[4]_i_1_n_0 ),
        .Q(vsync_cyc_s[4]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDPE \vsync_cyc_s_reg[5] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .D(\vsync_cyc_s[5]_i_1_n_0 ),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(vsync_cyc_s[5]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDPE \vsync_cyc_s_reg[6] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .D(\vsync_cyc_s[6]_i_1_n_0 ),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(vsync_cyc_s[6]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDPE \vsync_cyc_s_reg[7] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .D(\vsync_cyc_s[7]_i_1_n_0 ),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(vsync_cyc_s[7]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDCE \vsync_cyc_s_reg[8] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .CLR(\hsync_cyc_s[10]_i_2_n_0 ),
        .D(\vsync_cyc_s[8]_i_1_n_0 ),
        .Q(vsync_cyc_s[8]));
  (* KEEP = "yes" *) 
  (* mark_debug = "true" *) 
  FDPE \vsync_cyc_s_reg[9] 
       (.C(pixel_clk_i),
        .CE(vsync_cyc_s__0),
        .D(\vsync_cyc_s[9]_i_2_n_0 ),
        .PRE(\hsync_cyc_s[10]_i_2_n_0 ),
        .Q(vsync_cyc_s[9]));
  (* XILINX_LEGACY_PRIM = "LDP" *) 
  LDPE #(
    .INIT(1'b1)) 
    vsync_o_reg
       (.D(1'b0),
        .G(vsync_o_reg_i_1_n_0),
        .GE(1'b1),
        .PRE(vsync_o_reg_i_2_n_0),
        .Q(vsync_o));
  LUT4 #(
    .INIT(16'h0280)) 
    vsync_o_reg_i_1
       (.I0(vsync_o_reg_i_3_n_0),
        .I1(vsync_cyc_s[3]),
        .I2(vsync_cyc_s[1]),
        .I3(vsync_o_reg_i_4_n_0),
        .O(vsync_o_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    vsync_o_reg_i_2
       (.I0(vsync_o_reg_i_5_n_0),
        .I1(vsync_cyc_s[1]),
        .I2(vsync_cyc_s[3]),
        .I3(vsync_cyc_s[4]),
        .I4(vsync_o_reg_i_6_n_0),
        .I5(vsync_o_reg_i_4_n_0),
        .O(vsync_o_reg_i_2_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    vsync_o_reg_i_3
       (.I0(vsync_cyc_s[0]),
        .I1(vsync_cyc_s[2]),
        .I2(vsync_cyc_s[4]),
        .I3(vsync_o_reg_i_5_n_0),
        .O(vsync_o_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    vsync_o_reg_i_4
       (.I0(hsync_cyc_s[10]),
        .I1(hsync_cyc_s[9]),
        .I2(hsync_cyc_s[0]),
        .I3(vsync_o_reg_i_7_n_0),
        .I4(hsync_cyc_s[4]),
        .I5(hsync_o_INST_0_i_4_n_0),
        .O(vsync_o_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    vsync_o_reg_i_5
       (.I0(vsync_cyc_s[5]),
        .I1(vsync_cyc_s[8]),
        .I2(vsync_cyc_s[9]),
        .I3(vsync_cyc_s[7]),
        .I4(vsync_cyc_s[6]),
        .O(vsync_o_reg_i_5_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    vsync_o_reg_i_6
       (.I0(vsync_cyc_s[0]),
        .I1(vsync_cyc_s[2]),
        .O(vsync_o_reg_i_6_n_0));
  LUT4 #(
    .INIT(16'hEFFF)) 
    vsync_o_reg_i_7
       (.I0(hsync_cyc_s[8]),
        .I1(hsync_cyc_s[7]),
        .I2(hsync_cyc_s[6]),
        .I3(hsync_cyc_s[5]),
        .O(vsync_o_reg_i_7_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
