// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Fri Jul 17 19:49:38 2020
// Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode synth_stub
//               /mnt/Data1/FPGA_Projects/DVI/test_pattern_gen.srcs/sources_1/bd/design_1/ip/design_1_time_gen_0_0/design_1_time_gen_0_0_stub.v
// Design      : design_1_time_gen_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tfgg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "time_gen,Vivado 2019.2" *)
module design_1_time_gen_0_0(pixel_clk_i, rst_i, tdata_i, tuser_i, tlast_i, 
  tvalid_i, tdata_o, hsync_o, vsync_o, data_en_o, tready_o)
/* synthesis syn_black_box black_box_pad_pin="pixel_clk_i,rst_i,tdata_i[23:0],tuser_i,tlast_i,tvalid_i,tdata_o[23:0],hsync_o,vsync_o,data_en_o,tready_o" */;
  input pixel_clk_i;
  input rst_i;
  input [23:0]tdata_i;
  input tuser_i;
  input tlast_i;
  input tvalid_i;
  output [23:0]tdata_o;
  output hsync_o;
  output vsync_o;
  output data_en_o;
  output tready_o;
endmodule
