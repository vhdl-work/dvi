-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Fri Jul 17 19:49:39 2020
-- Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /mnt/Data1/FPGA_Projects/DVI/test_pattern_gen.srcs/sources_1/bd/design_1/ip/design_1_time_gen_0_0/design_1_time_gen_0_0_sim_netlist.vhdl
-- Design      : design_1_time_gen_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_time_gen_0_0_time_gen is
  port (
    tdata_o : out STD_LOGIC_VECTOR ( 23 downto 0 );
    vsync_o : out STD_LOGIC;
    data_en_o : out STD_LOGIC;
    tready_o : out STD_LOGIC;
    hsync_o : out STD_LOGIC;
    tdata_i : in STD_LOGIC_VECTOR ( 23 downto 0 );
    tvalid_i : in STD_LOGIC;
    pixel_clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_time_gen_0_0_time_gen : entity is "time_gen";
end design_1_time_gen_0_0_time_gen;

architecture STRUCTURE of design_1_time_gen_0_0_time_gen is
  signal data_en_o_reg_i_1_n_0 : STD_LOGIC;
  signal data_en_o_reg_i_2_n_0 : STD_LOGIC;
  signal data_en_o_reg_i_3_n_0 : STD_LOGIC;
  signal data_en_o_reg_i_4_n_0 : STD_LOGIC;
  signal data_en_o_reg_i_5_n_0 : STD_LOGIC;
  signal data_en_o_reg_i_6_n_0 : STD_LOGIC;
  signal data_en_o_reg_i_7_n_0 : STD_LOGIC;
  signal data_en_o_reg_i_8_n_0 : STD_LOGIC;
  signal hsync_cyc_s : STD_LOGIC_VECTOR ( 10 downto 0 );
  attribute MARK_DEBUG : boolean;
  attribute MARK_DEBUG of hsync_cyc_s : signal is std.standard.true;
  signal \hsync_cyc_s[10]_i_2_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[10]_i_3_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[10]_i_4_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[10]_i_5_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[10]_i_6_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[4]_i_2_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[5]_i_2_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[6]_i_2_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s[9]_i_2_n_0\ : STD_LOGIC;
  signal \hsync_cyc_s__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal hsync_o_INST_0_i_1_n_0 : STD_LOGIC;
  signal hsync_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal hsync_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal hsync_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal hsync_o_INST_0_i_5_n_0 : STD_LOGIC;
  signal hsync_o_INST_0_i_6_n_0 : STD_LOGIC;
  signal tready_o_reg_i_1_n_0 : STD_LOGIC;
  signal tready_o_reg_i_2_n_0 : STD_LOGIC;
  signal tready_o_reg_i_3_n_0 : STD_LOGIC;
  signal vsync_cyc_s : STD_LOGIC_VECTOR ( 9 downto 0 );
  attribute MARK_DEBUG of vsync_cyc_s : signal is std.standard.true;
  signal \vsync_cyc_s[0]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[1]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[2]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[3]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[4]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[5]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[5]_i_2_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[6]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[7]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[7]_i_2_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[8]_i_1_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[8]_i_2_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[9]_i_2_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[9]_i_3_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[9]_i_4_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s[9]_i_5_n_0\ : STD_LOGIC;
  signal \vsync_cyc_s__0\ : STD_LOGIC;
  signal vsync_o_reg_i_1_n_0 : STD_LOGIC;
  signal vsync_o_reg_i_2_n_0 : STD_LOGIC;
  signal vsync_o_reg_i_3_n_0 : STD_LOGIC;
  signal vsync_o_reg_i_4_n_0 : STD_LOGIC;
  signal vsync_o_reg_i_5_n_0 : STD_LOGIC;
  signal vsync_o_reg_i_6_n_0 : STD_LOGIC;
  signal vsync_o_reg_i_7_n_0 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of data_en_o_reg : label is "LDC";
  attribute KEEP : string;
  attribute KEEP of \hsync_cyc_s_reg[0]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[10]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[1]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[2]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[3]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[4]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[5]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[6]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[7]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[8]\ : label is "yes";
  attribute KEEP of \hsync_cyc_s_reg[9]\ : label is "yes";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[10]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[11]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[12]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[13]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[14]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[15]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[16]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[17]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[18]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[19]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[20]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[21]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[22]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[23]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[2]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[3]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[5]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[6]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[7]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[8]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \tdata_o_reg[9]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of tready_o_reg : label is "LDC";
  attribute KEEP of \vsync_cyc_s_reg[0]\ : label is "yes";
  attribute mark_debug_string : string;
  attribute mark_debug_string of \vsync_cyc_s_reg[0]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[1]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[1]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[2]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[2]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[3]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[3]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[4]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[4]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[5]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[5]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[6]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[6]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[7]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[7]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[8]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[8]\ : label is "true";
  attribute KEEP of \vsync_cyc_s_reg[9]\ : label is "yes";
  attribute mark_debug_string of \vsync_cyc_s_reg[9]\ : label is "true";
  attribute XILINX_LEGACY_PRIM of vsync_o_reg : label is "LDP";
begin
data_en_o_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => data_en_o_reg_i_3_n_0,
      D => data_en_o_reg_i_1_n_0,
      G => data_en_o_reg_i_2_n_0,
      GE => '1',
      Q => data_en_o
    );
data_en_o_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF8000"
    )
        port map (
      I0 => vsync_cyc_s(3),
      I1 => vsync_cyc_s(4),
      I2 => vsync_cyc_s(1),
      I3 => vsync_cyc_s(2),
      I4 => vsync_o_reg_i_5_n_0,
      O => data_en_o_reg_i_1_n_0
    );
data_en_o_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => data_en_o_reg_i_1_n_0,
      I1 => data_en_o_reg_i_4_n_0,
      I2 => hsync_cyc_s(1),
      I3 => hsync_cyc_s(3),
      I4 => data_en_o_reg_i_5_n_0,
      I5 => data_en_o_reg_i_6_n_0,
      O => data_en_o_reg_i_2_n_0
    );
data_en_o_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => data_en_o_reg_i_7_n_0,
      I1 => hsync_cyc_s(1),
      I2 => hsync_cyc_s(2),
      I3 => hsync_cyc_s(4),
      I4 => data_en_o_reg_i_8_n_0,
      I5 => data_en_o_reg_i_5_n_0,
      O => data_en_o_reg_i_3_n_0
    );
data_en_o_reg_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => hsync_cyc_s(8),
      I1 => hsync_cyc_s(7),
      O => data_en_o_reg_i_4_n_0
    );
data_en_o_reg_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => hsync_cyc_s(10),
      I1 => hsync_cyc_s(9),
      I2 => hsync_cyc_s(0),
      O => data_en_o_reg_i_5_n_0
    );
data_en_o_reg_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => hsync_cyc_s(4),
      I1 => hsync_cyc_s(5),
      I2 => hsync_cyc_s(6),
      I3 => hsync_cyc_s(2),
      O => data_en_o_reg_i_6_n_0
    );
data_en_o_reg_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => hsync_cyc_s(8),
      I1 => hsync_cyc_s(7),
      I2 => hsync_cyc_s(3),
      O => data_en_o_reg_i_7_n_0
    );
data_en_o_reg_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => hsync_cyc_s(5),
      I1 => hsync_cyc_s(6),
      O => data_en_o_reg_i_8_n_0
    );
\hsync_cyc_s[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => hsync_cyc_s(0),
      O => \hsync_cyc_s__0\(0)
    );
\hsync_cyc_s[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF8CFF8CFF8CFFBC"
    )
        port map (
      I0 => \hsync_cyc_s[10]_i_3_n_0\,
      I1 => hsync_cyc_s(10),
      I2 => hsync_cyc_s(9),
      I3 => \hsync_cyc_s[10]_i_4_n_0\,
      I4 => \hsync_cyc_s[10]_i_5_n_0\,
      I5 => \hsync_cyc_s[10]_i_6_n_0\,
      O => \hsync_cyc_s__0\(10)
    );
\hsync_cyc_s[10]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rst_i,
      O => \hsync_cyc_s[10]_i_2_n_0\
    );
\hsync_cyc_s[10]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF7CFCFCFC"
    )
        port map (
      I0 => hsync_cyc_s(3),
      I1 => hsync_cyc_s(2),
      I2 => hsync_cyc_s(1),
      I3 => hsync_cyc_s(8),
      I4 => hsync_cyc_s(7),
      I5 => \vsync_cyc_s[9]_i_3_n_0\,
      O => \hsync_cyc_s[10]_i_3_n_0\
    );
\hsync_cyc_s[10]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44444440"
    )
        port map (
      I0 => hsync_cyc_s(2),
      I1 => hsync_cyc_s(10),
      I2 => hsync_cyc_s(3),
      I3 => hsync_cyc_s(7),
      I4 => hsync_cyc_s(8),
      O => \hsync_cyc_s[10]_i_4_n_0\
    );
\hsync_cyc_s[10]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => hsync_cyc_s(0),
      I1 => hsync_cyc_s(6),
      I2 => hsync_cyc_s(5),
      I3 => hsync_cyc_s(4),
      I4 => hsync_o_INST_0_i_4_n_0,
      O => \hsync_cyc_s[10]_i_5_n_0\
    );
\hsync_cyc_s[10]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => hsync_cyc_s(7),
      I1 => hsync_cyc_s(8),
      O => \hsync_cyc_s[10]_i_6_n_0\
    );
\hsync_cyc_s[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0FE0"
    )
        port map (
      I0 => data_en_o_reg_i_6_n_0,
      I1 => tready_o_reg_i_3_n_0,
      I2 => hsync_cyc_s(0),
      I3 => hsync_cyc_s(1),
      O => \hsync_cyc_s__0\(1)
    );
\hsync_cyc_s[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => hsync_cyc_s(1),
      I1 => hsync_cyc_s(0),
      I2 => hsync_cyc_s(2),
      O => \hsync_cyc_s__0\(2)
    );
\hsync_cyc_s[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => hsync_cyc_s(0),
      I1 => hsync_cyc_s(1),
      I2 => hsync_cyc_s(2),
      I3 => hsync_cyc_s(3),
      O => \hsync_cyc_s__0\(3)
    );
\hsync_cyc_s[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF00F0444400F0"
    )
        port map (
      I0 => hsync_cyc_s(2),
      I1 => hsync_o_INST_0_i_3_n_0,
      I2 => hsync_cyc_s(0),
      I3 => hsync_o_INST_0_i_4_n_0,
      I4 => hsync_cyc_s(4),
      I5 => \hsync_cyc_s[4]_i_2_n_0\,
      O => \hsync_cyc_s__0\(4)
    );
\hsync_cyc_s[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5FFDFFFD"
    )
        port map (
      I0 => hsync_cyc_s(0),
      I1 => tready_o_reg_i_3_n_0,
      I2 => hsync_cyc_s(1),
      I3 => hsync_cyc_s(2),
      I4 => hsync_cyc_s(3),
      O => \hsync_cyc_s[4]_i_2_n_0\
    );
\hsync_cyc_s[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88B888B888B8CCFC"
    )
        port map (
      I0 => \hsync_cyc_s[6]_i_2_n_0\,
      I1 => hsync_cyc_s(5),
      I2 => \hsync_cyc_s[5]_i_2_n_0\,
      I3 => hsync_o_INST_0_i_4_n_0,
      I4 => hsync_cyc_s(6),
      I5 => hsync_cyc_s(2),
      O => \hsync_cyc_s__0\(5)
    );
\hsync_cyc_s[5]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hsync_cyc_s(0),
      I1 => hsync_cyc_s(4),
      O => \hsync_cyc_s[5]_i_2_n_0\
    );
\hsync_cyc_s[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF00800F0F0080"
    )
        port map (
      I0 => hsync_cyc_s(0),
      I1 => hsync_cyc_s(4),
      I2 => hsync_cyc_s(5),
      I3 => hsync_o_INST_0_i_4_n_0,
      I4 => hsync_cyc_s(6),
      I5 => \hsync_cyc_s[6]_i_2_n_0\,
      O => \hsync_cyc_s__0\(6)
    );
\hsync_cyc_s[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7CFFFFFFFFFFFF"
    )
        port map (
      I0 => hsync_cyc_s(3),
      I1 => hsync_cyc_s(2),
      I2 => hsync_cyc_s(1),
      I3 => tready_o_reg_i_3_n_0,
      I4 => hsync_cyc_s(0),
      I5 => hsync_cyc_s(4),
      O => \hsync_cyc_s[6]_i_2_n_0\
    );
\hsync_cyc_s[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF40000000"
    )
        port map (
      I0 => hsync_o_INST_0_i_4_n_0,
      I1 => hsync_cyc_s(4),
      I2 => hsync_cyc_s(5),
      I3 => hsync_cyc_s(6),
      I4 => hsync_cyc_s(0),
      I5 => hsync_cyc_s(7),
      O => \hsync_cyc_s__0\(7)
    );
\hsync_cyc_s[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => hsync_cyc_s(7),
      I1 => \hsync_cyc_s[10]_i_5_n_0\,
      I2 => hsync_cyc_s(8),
      O => \hsync_cyc_s__0\(8)
    );
\hsync_cyc_s[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF000F4444000F"
    )
        port map (
      I0 => hsync_cyc_s(2),
      I1 => \hsync_cyc_s[9]_i_2_n_0\,
      I2 => \hsync_cyc_s[10]_i_6_n_0\,
      I3 => \hsync_cyc_s[10]_i_5_n_0\,
      I4 => hsync_cyc_s(9),
      I5 => \hsync_cyc_s[10]_i_3_n_0\,
      O => \hsync_cyc_s__0\(9)
    );
\hsync_cyc_s[9]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => hsync_cyc_s(3),
      I1 => hsync_cyc_s(7),
      I2 => hsync_cyc_s(8),
      I3 => hsync_cyc_s(10),
      O => \hsync_cyc_s[9]_i_2_n_0\
    );
\hsync_cyc_s_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => '1',
      D => \hsync_cyc_s__0\(0),
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => hsync_cyc_s(0)
    );
\hsync_cyc_s_reg[10]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => '1',
      D => \hsync_cyc_s__0\(10),
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => hsync_cyc_s(10)
    );
\hsync_cyc_s_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \hsync_cyc_s__0\(1),
      Q => hsync_cyc_s(1)
    );
\hsync_cyc_s_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \hsync_cyc_s__0\(2),
      Q => hsync_cyc_s(2)
    );
\hsync_cyc_s_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \hsync_cyc_s__0\(3),
      Q => hsync_cyc_s(3)
    );
\hsync_cyc_s_reg[4]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => '1',
      D => \hsync_cyc_s__0\(4),
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => hsync_cyc_s(4)
    );
\hsync_cyc_s_reg[5]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => '1',
      D => \hsync_cyc_s__0\(5),
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => hsync_cyc_s(5)
    );
\hsync_cyc_s_reg[6]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => '1',
      D => \hsync_cyc_s__0\(6),
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => hsync_cyc_s(6)
    );
\hsync_cyc_s_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \hsync_cyc_s__0\(7),
      Q => hsync_cyc_s(7)
    );
\hsync_cyc_s_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \hsync_cyc_s__0\(8),
      Q => hsync_cyc_s(8)
    );
\hsync_cyc_s_reg[9]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => '1',
      D => \hsync_cyc_s__0\(9),
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => hsync_cyc_s(9)
    );
hsync_o_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAEAAAF"
    )
        port map (
      I0 => hsync_o_INST_0_i_1_n_0,
      I1 => hsync_cyc_s(4),
      I2 => hsync_o_INST_0_i_2_n_0,
      I3 => hsync_o_INST_0_i_3_n_0,
      I4 => hsync_o_INST_0_i_4_n_0,
      I5 => hsync_o_INST_0_i_5_n_0,
      O => hsync_o
    );
hsync_o_INST_0_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"22222AAA"
    )
        port map (
      I0 => hsync_o_INST_0_i_6_n_0,
      I1 => hsync_cyc_s(4),
      I2 => hsync_cyc_s(2),
      I3 => hsync_cyc_s(1),
      I4 => hsync_cyc_s(3),
      O => hsync_o_INST_0_i_1_n_0
    );
hsync_o_INST_0_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => hsync_cyc_s(7),
      I1 => hsync_cyc_s(8),
      O => hsync_o_INST_0_i_2_n_0
    );
hsync_o_INST_0_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => hsync_cyc_s(5),
      I1 => hsync_cyc_s(6),
      O => hsync_o_INST_0_i_3_n_0
    );
hsync_o_INST_0_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => hsync_cyc_s(3),
      I1 => hsync_cyc_s(1),
      I2 => hsync_cyc_s(2),
      O => hsync_o_INST_0_i_4_n_0
    );
hsync_o_INST_0_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => hsync_cyc_s(9),
      I1 => hsync_cyc_s(10),
      O => hsync_o_INST_0_i_5_n_0
    );
hsync_o_INST_0_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => hsync_cyc_s(7),
      I1 => hsync_cyc_s(8),
      I2 => hsync_cyc_s(5),
      I3 => hsync_cyc_s(6),
      I4 => hsync_cyc_s(10),
      I5 => hsync_cyc_s(9),
      O => hsync_o_INST_0_i_6_n_0
    );
\tdata_o_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(0),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(0)
    );
\tdata_o_reg[10]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(10),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(10)
    );
\tdata_o_reg[11]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(11),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(11)
    );
\tdata_o_reg[12]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(12),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(12)
    );
\tdata_o_reg[13]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(13),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(13)
    );
\tdata_o_reg[14]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(14),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(14)
    );
\tdata_o_reg[15]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(15),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(15)
    );
\tdata_o_reg[16]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(16),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(16)
    );
\tdata_o_reg[17]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(17),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(17)
    );
\tdata_o_reg[18]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(18),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(18)
    );
\tdata_o_reg[19]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(19),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(19)
    );
\tdata_o_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(1),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(1)
    );
\tdata_o_reg[20]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(20),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(20)
    );
\tdata_o_reg[21]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(21),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(21)
    );
\tdata_o_reg[22]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(22),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(22)
    );
\tdata_o_reg[23]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(23),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(23)
    );
\tdata_o_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(2),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(2)
    );
\tdata_o_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(3),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(3)
    );
\tdata_o_reg[4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(4),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(4)
    );
\tdata_o_reg[5]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(5),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(5)
    );
\tdata_o_reg[6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(6),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(6)
    );
\tdata_o_reg[7]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(7),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(7)
    );
\tdata_o_reg[8]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(8),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(8)
    );
\tdata_o_reg[9]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tdata_i(9),
      G => tvalid_i,
      GE => '1',
      Q => tdata_o(9)
    );
tready_o_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => tready_o_reg_i_2_n_0,
      D => data_en_o_reg_i_1_n_0,
      G => tready_o_reg_i_1_n_0,
      GE => '1',
      Q => tready_o
    );
tready_o_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => data_en_o_reg_i_1_n_0,
      I1 => data_en_o_reg_i_4_n_0,
      I2 => hsync_cyc_s(1),
      I3 => hsync_cyc_s(3),
      I4 => data_en_o_reg_i_5_n_0,
      I5 => data_en_o_reg_i_6_n_0,
      O => tready_o_reg_i_1_n_0
    );
tready_o_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => hsync_cyc_s(1),
      I1 => hsync_cyc_s(0),
      I2 => tready_o_reg_i_3_n_0,
      I3 => data_en_o_reg_i_6_n_0,
      O => tready_o_reg_i_2_n_0
    );
tready_o_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFDFFFF"
    )
        port map (
      I0 => hsync_cyc_s(10),
      I1 => hsync_cyc_s(8),
      I2 => hsync_cyc_s(7),
      I3 => hsync_cyc_s(3),
      I4 => hsync_cyc_s(9),
      O => tready_o_reg_i_3_n_0
    );
\vsync_cyc_s[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => vsync_cyc_s(0),
      O => \vsync_cyc_s[0]_i_1_n_0\
    );
\vsync_cyc_s[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFFF7FF0000"
    )
        port map (
      I0 => vsync_cyc_s(3),
      I1 => vsync_cyc_s(5),
      I2 => \vsync_cyc_s[5]_i_2_n_0\,
      I3 => vsync_cyc_s(2),
      I4 => vsync_cyc_s(0),
      I5 => vsync_cyc_s(1),
      O => \vsync_cyc_s[1]_i_1_n_0\
    );
\vsync_cyc_s[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5515FFFFAAAA0000"
    )
        port map (
      I0 => vsync_cyc_s(1),
      I1 => vsync_cyc_s(3),
      I2 => vsync_cyc_s(5),
      I3 => \vsync_cyc_s[5]_i_2_n_0\,
      I4 => vsync_cyc_s(0),
      I5 => vsync_cyc_s(2),
      O => \vsync_cyc_s[2]_i_1_n_0\
    );
\vsync_cyc_s[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"51AAFF00FF00FF00"
    )
        port map (
      I0 => vsync_cyc_s(1),
      I1 => vsync_cyc_s(5),
      I2 => \vsync_cyc_s[5]_i_2_n_0\,
      I3 => vsync_cyc_s(3),
      I4 => vsync_cyc_s(0),
      I5 => vsync_cyc_s(2),
      O => \vsync_cyc_s[3]_i_1_n_0\
    );
\vsync_cyc_s[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => vsync_cyc_s(1),
      I1 => vsync_cyc_s(2),
      I2 => vsync_cyc_s(0),
      I3 => vsync_cyc_s(3),
      I4 => vsync_cyc_s(4),
      O => \vsync_cyc_s[4]_i_1_n_0\
    );
\vsync_cyc_s[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCCC62C8CCCC"
    )
        port map (
      I0 => vsync_cyc_s(1),
      I1 => vsync_cyc_s(5),
      I2 => \vsync_cyc_s[5]_i_2_n_0\,
      I3 => vsync_cyc_s(4),
      I4 => vsync_cyc_s(3),
      I5 => vsync_o_reg_i_6_n_0,
      O => \vsync_cyc_s[5]_i_1_n_0\
    );
\vsync_cyc_s[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FDFFFFFF"
    )
        port map (
      I0 => vsync_cyc_s(9),
      I1 => vsync_cyc_s(4),
      I2 => vsync_cyc_s(8),
      I3 => vsync_cyc_s(7),
      I4 => vsync_cyc_s(6),
      O => \vsync_cyc_s[5]_i_2_n_0\
    );
\vsync_cyc_s[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0F0F00FF0B0B0"
    )
        port map (
      I0 => \vsync_cyc_s[7]_i_2_n_0\,
      I1 => vsync_cyc_s(7),
      I2 => vsync_cyc_s(6),
      I3 => vsync_cyc_s(4),
      I4 => vsync_cyc_s(1),
      I5 => \vsync_cyc_s[9]_i_4_n_0\,
      O => \vsync_cyc_s[6]_i_1_n_0\
    );
\vsync_cyc_s[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7F4FFFF08080000"
    )
        port map (
      I0 => vsync_cyc_s(4),
      I1 => vsync_cyc_s(1),
      I2 => \vsync_cyc_s[9]_i_4_n_0\,
      I3 => \vsync_cyc_s[7]_i_2_n_0\,
      I4 => vsync_cyc_s(6),
      I5 => vsync_cyc_s(7),
      O => \vsync_cyc_s[7]_i_1_n_0\
    );
\vsync_cyc_s[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => vsync_cyc_s(8),
      I1 => vsync_cyc_s(4),
      I2 => vsync_cyc_s(9),
      O => \vsync_cyc_s[7]_i_2_n_0\
    );
\vsync_cyc_s[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F708"
    )
        port map (
      I0 => vsync_cyc_s(6),
      I1 => vsync_cyc_s(7),
      I2 => \vsync_cyc_s[8]_i_2_n_0\,
      I3 => vsync_cyc_s(8),
      O => \vsync_cyc_s[8]_i_1_n_0\
    );
\vsync_cyc_s[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => vsync_cyc_s(4),
      I1 => vsync_cyc_s(1),
      I2 => vsync_cyc_s(0),
      I3 => vsync_cyc_s(2),
      I4 => vsync_cyc_s(3),
      I5 => vsync_cyc_s(5),
      O => \vsync_cyc_s[8]_i_2_n_0\
    );
\vsync_cyc_s[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \vsync_cyc_s[9]_i_3_n_0\,
      I1 => hsync_cyc_s(1),
      I2 => hsync_cyc_s(2),
      I3 => tready_o_reg_i_3_n_0,
      O => \vsync_cyc_s__0\
    );
\vsync_cyc_s[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBFFFFE00400000"
    )
        port map (
      I0 => \vsync_cyc_s[9]_i_4_n_0\,
      I1 => vsync_cyc_s(1),
      I2 => vsync_cyc_s(4),
      I3 => \vsync_cyc_s[9]_i_5_n_0\,
      I4 => vsync_cyc_s(8),
      I5 => vsync_cyc_s(9),
      O => \vsync_cyc_s[9]_i_2_n_0\
    );
\vsync_cyc_s[9]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => hsync_cyc_s(4),
      I1 => hsync_cyc_s(5),
      I2 => hsync_cyc_s(6),
      I3 => hsync_cyc_s(0),
      O => \vsync_cyc_s[9]_i_3_n_0\
    );
\vsync_cyc_s[9]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => vsync_cyc_s(5),
      I1 => vsync_cyc_s(3),
      I2 => vsync_cyc_s(2),
      I3 => vsync_cyc_s(0),
      O => \vsync_cyc_s[9]_i_4_n_0\
    );
\vsync_cyc_s[9]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => vsync_cyc_s(6),
      I1 => vsync_cyc_s(7),
      O => \vsync_cyc_s[9]_i_5_n_0\
    );
\vsync_cyc_s_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      D => \vsync_cyc_s[0]_i_1_n_0\,
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => vsync_cyc_s(0)
    );
\vsync_cyc_s_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \vsync_cyc_s[1]_i_1_n_0\,
      Q => vsync_cyc_s(1)
    );
\vsync_cyc_s_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      D => \vsync_cyc_s[2]_i_1_n_0\,
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => vsync_cyc_s(2)
    );
\vsync_cyc_s_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      D => \vsync_cyc_s[3]_i_1_n_0\,
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => vsync_cyc_s(3)
    );
\vsync_cyc_s_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \vsync_cyc_s[4]_i_1_n_0\,
      Q => vsync_cyc_s(4)
    );
\vsync_cyc_s_reg[5]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      D => \vsync_cyc_s[5]_i_1_n_0\,
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => vsync_cyc_s(5)
    );
\vsync_cyc_s_reg[6]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      D => \vsync_cyc_s[6]_i_1_n_0\,
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => vsync_cyc_s(6)
    );
\vsync_cyc_s_reg[7]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      D => \vsync_cyc_s[7]_i_1_n_0\,
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => vsync_cyc_s(7)
    );
\vsync_cyc_s_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      CLR => \hsync_cyc_s[10]_i_2_n_0\,
      D => \vsync_cyc_s[8]_i_1_n_0\,
      Q => vsync_cyc_s(8)
    );
\vsync_cyc_s_reg[9]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \vsync_cyc_s__0\,
      D => \vsync_cyc_s[9]_i_2_n_0\,
      PRE => \hsync_cyc_s[10]_i_2_n_0\,
      Q => vsync_cyc_s(9)
    );
vsync_o_reg: unisim.vcomponents.LDPE
    generic map(
      INIT => '1'
    )
        port map (
      D => '0',
      G => vsync_o_reg_i_1_n_0,
      GE => '1',
      PRE => vsync_o_reg_i_2_n_0,
      Q => vsync_o
    );
vsync_o_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0280"
    )
        port map (
      I0 => vsync_o_reg_i_3_n_0,
      I1 => vsync_cyc_s(3),
      I2 => vsync_cyc_s(1),
      I3 => vsync_o_reg_i_4_n_0,
      O => vsync_o_reg_i_1_n_0
    );
vsync_o_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => vsync_o_reg_i_5_n_0,
      I1 => vsync_cyc_s(1),
      I2 => vsync_cyc_s(3),
      I3 => vsync_cyc_s(4),
      I4 => vsync_o_reg_i_6_n_0,
      I5 => vsync_o_reg_i_4_n_0,
      O => vsync_o_reg_i_2_n_0
    );
vsync_o_reg_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => vsync_cyc_s(0),
      I1 => vsync_cyc_s(2),
      I2 => vsync_cyc_s(4),
      I3 => vsync_o_reg_i_5_n_0,
      O => vsync_o_reg_i_3_n_0
    );
vsync_o_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => hsync_cyc_s(10),
      I1 => hsync_cyc_s(9),
      I2 => hsync_cyc_s(0),
      I3 => vsync_o_reg_i_7_n_0,
      I4 => hsync_cyc_s(4),
      I5 => hsync_o_INST_0_i_4_n_0,
      O => vsync_o_reg_i_4_n_0
    );
vsync_o_reg_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => vsync_cyc_s(5),
      I1 => vsync_cyc_s(8),
      I2 => vsync_cyc_s(9),
      I3 => vsync_cyc_s(7),
      I4 => vsync_cyc_s(6),
      O => vsync_o_reg_i_5_n_0
    );
vsync_o_reg_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => vsync_cyc_s(0),
      I1 => vsync_cyc_s(2),
      O => vsync_o_reg_i_6_n_0
    );
vsync_o_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFFF"
    )
        port map (
      I0 => hsync_cyc_s(8),
      I1 => hsync_cyc_s(7),
      I2 => hsync_cyc_s(6),
      I3 => hsync_cyc_s(5),
      O => vsync_o_reg_i_7_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_time_gen_0_0 is
  port (
    pixel_clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    tdata_i : in STD_LOGIC_VECTOR ( 23 downto 0 );
    tuser_i : in STD_LOGIC;
    tlast_i : in STD_LOGIC;
    tvalid_i : in STD_LOGIC;
    tdata_o : out STD_LOGIC_VECTOR ( 23 downto 0 );
    hsync_o : out STD_LOGIC;
    vsync_o : out STD_LOGIC;
    data_en_o : out STD_LOGIC;
    tready_o : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_time_gen_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_time_gen_0_0 : entity is "design_1_time_gen_0_0,time_gen,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_time_gen_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_time_gen_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of design_1_time_gen_0_0 : entity is "time_gen,Vivado 2019.2";
end design_1_time_gen_0_0;

architecture STRUCTURE of design_1_time_gen_0_0 is
begin
U0: entity work.design_1_time_gen_0_0_time_gen
     port map (
      data_en_o => data_en_o,
      hsync_o => hsync_o,
      pixel_clk_i => pixel_clk_i,
      rst_i => rst_i,
      tdata_i(23 downto 0) => tdata_i(23 downto 0),
      tdata_o(23 downto 0) => tdata_o(23 downto 0),
      tready_o => tready_o,
      tvalid_i => tvalid_i,
      vsync_o => vsync_o
    );
end STRUCTURE;
