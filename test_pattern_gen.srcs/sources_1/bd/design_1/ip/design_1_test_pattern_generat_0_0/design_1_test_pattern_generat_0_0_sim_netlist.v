// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Fri Jul 17 19:49:39 2020
// Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
// Command     : write_verilog -force -mode funcsim
//               /mnt/Data1/FPGA_Projects/DVI/test_pattern_gen.srcs/sources_1/bd/design_1/ip/design_1_test_pattern_generat_0_0/design_1_test_pattern_generat_0_0_sim_netlist.v
// Design      : design_1_test_pattern_generat_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_test_pattern_generat_0_0,test_pattern_generator,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "test_pattern_generator,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module design_1_test_pattern_generat_0_0
   (pixel_clk_i,
    rst_i,
    tready_i,
    tdata_o,
    tuser_o,
    tlast_o,
    tvalid_o);
  input pixel_clk_i;
  input rst_i;
  input tready_i;
  output [23:0]tdata_o;
  output tuser_o;
  output tlast_o;
  output tvalid_o;

  wire pixel_clk_i;
  wire rst_i;
  wire [23:7]\^tdata_o ;
  wire tlast_o;
  wire tready_i;
  wire tuser_o;
  wire tvalid_o;

  assign tdata_o[23] = \^tdata_o [23];
  assign tdata_o[22] = \^tdata_o [23];
  assign tdata_o[21] = \^tdata_o [23];
  assign tdata_o[20] = \^tdata_o [23];
  assign tdata_o[19] = \^tdata_o [23];
  assign tdata_o[18] = \^tdata_o [23];
  assign tdata_o[17] = \^tdata_o [23];
  assign tdata_o[16] = \^tdata_o [23];
  assign tdata_o[15] = \^tdata_o [15];
  assign tdata_o[14] = \^tdata_o [15];
  assign tdata_o[13] = \^tdata_o [15];
  assign tdata_o[12] = \^tdata_o [15];
  assign tdata_o[11] = \^tdata_o [15];
  assign tdata_o[10] = \^tdata_o [15];
  assign tdata_o[9] = \^tdata_o [15];
  assign tdata_o[8] = \^tdata_o [15];
  assign tdata_o[7] = \^tdata_o [7];
  assign tdata_o[6] = \^tdata_o [7];
  assign tdata_o[5] = \^tdata_o [7];
  assign tdata_o[4] = \^tdata_o [7];
  assign tdata_o[3] = \^tdata_o [7];
  assign tdata_o[2] = \^tdata_o [7];
  assign tdata_o[1] = \^tdata_o [7];
  assign tdata_o[0] = \^tdata_o [7];
  design_1_test_pattern_generat_0_0_test_pattern_generator U0
       (.pixel_clk_i(pixel_clk_i),
        .rst_i(rst_i),
        .tdata_o({\^tdata_o [23],\^tdata_o [15],\^tdata_o [7]}),
        .tlast_o(tlast_o),
        .tready_i(tready_i),
        .tuser_o(tuser_o),
        .tvalid_o(tvalid_o));
endmodule

(* ORIG_REF_NAME = "test_pattern_generator" *) 
module design_1_test_pattern_generat_0_0_test_pattern_generator
   (tuser_o,
    tlast_o,
    tvalid_o,
    tdata_o,
    tready_i,
    pixel_clk_i,
    rst_i);
  output tuser_o;
  output tlast_o;
  output tvalid_o;
  output [2:0]tdata_o;
  input tready_i;
  input pixel_clk_i;
  input rst_i;

  wire __2;
  wire [7:0]fr_cnt_s;
  wire \fr_cnt_s[1]_i_2_n_0 ;
  wire \fr_cnt_s[7]_i_1_n_0 ;
  wire \fr_cnt_s[7]_i_3_n_0 ;
  wire [7:0]fr_cnt_s_1;
  wire [9:0]height_s;
  wire \height_s[7]_i_2_n_0 ;
  wire \height_s[9]_i_1_n_0 ;
  wire \height_s[9]_i_3_n_0 ;
  wire [9:0]height_s_0;
  wire i___1_carry__0_i_10_n_0;
  wire i___1_carry__0_i_11_n_0;
  wire i___1_carry__0_i_12_n_0;
  wire i___1_carry__0_i_13_n_0;
  wire i___1_carry__0_i_1_n_0;
  wire i___1_carry__0_i_2_n_0;
  wire i___1_carry__0_i_3_n_0;
  wire i___1_carry__0_i_4_n_0;
  wire i___1_carry__0_i_5_n_0;
  wire i___1_carry__0_i_6_n_0;
  wire i___1_carry__0_i_7_n_0;
  wire i___1_carry__0_i_8_n_0;
  wire i___1_carry__0_i_8_n_1;
  wire i___1_carry__0_i_8_n_2;
  wire i___1_carry__0_i_8_n_3;
  wire i___1_carry__0_i_8_n_4;
  wire i___1_carry__0_i_8_n_5;
  wire i___1_carry__0_i_8_n_6;
  wire i___1_carry__0_i_8_n_7;
  wire i___1_carry__0_i_9_n_0;
  wire i___1_carry__1_i_10_n_0;
  wire i___1_carry__1_i_11_n_0;
  wire i___1_carry__1_i_12_n_0;
  wire i___1_carry__1_i_13_n_0;
  wire i___1_carry__1_i_1_n_0;
  wire i___1_carry__1_i_2_n_0;
  wire i___1_carry__1_i_3_n_0;
  wire i___1_carry__1_i_4_n_0;
  wire i___1_carry__1_i_5_n_0;
  wire i___1_carry__1_i_6_n_0;
  wire i___1_carry__1_i_7_n_0;
  wire i___1_carry__1_i_8_n_0;
  wire i___1_carry__1_i_9_n_0;
  wire i___1_carry__2_i_10_n_0;
  wire i___1_carry__2_i_1_n_0;
  wire i___1_carry__2_i_2_n_0;
  wire i___1_carry__2_i_3_n_0;
  wire i___1_carry__2_i_4_n_0;
  wire i___1_carry__2_i_5_n_0;
  wire i___1_carry__2_i_6_n_0;
  wire i___1_carry__2_i_7_n_0;
  wire i___1_carry__2_i_8_n_0;
  wire i___1_carry__2_i_9_n_0;
  wire i___1_carry__3_i_1_n_0;
  wire i___1_carry_i_1_n_0;
  wire i___1_carry_i_1_n_1;
  wire i___1_carry_i_1_n_2;
  wire i___1_carry_i_1_n_3;
  wire i___1_carry_i_1_n_4;
  wire i___1_carry_i_1_n_5;
  wire i___1_carry_i_1_n_6;
  wire i___1_carry_i_1_n_7;
  wire i___1_carry_i_2_n_0;
  wire i___1_carry_i_3_n_0;
  wire i___1_carry_i_4_n_0;
  wire i___1_carry_i_5_n_0;
  wire i___1_carry_i_6_n_0;
  wire i___1_carry_i_7_n_0;
  wire i___1_carry_i_8_n_0;
  wire i___37_carry__0_i_1_n_0;
  wire i___37_carry_i_1_n_0;
  wire i___37_carry_i_2_n_0;
  wire i___37_carry_i_3_n_0;
  wire i___55_carry__0_i_1_n_0;
  wire i___55_carry__0_i_2_n_0;
  wire i___55_carry__0_i_3_n_0;
  wire i___55_carry__0_i_4_n_0;
  wire i___55_carry__0_i_5_n_0;
  wire i___55_carry__0_i_6_n_0;
  wire i___55_carry__0_i_7_n_0;
  wire i___55_carry__0_i_8_n_0;
  wire i___55_carry_i_1_n_0;
  wire i___55_carry_i_2_n_0;
  wire i___55_carry_i_3_n_0;
  wire i___55_carry_i_4_n_0;
  wire i___55_carry_i_5_n_0;
  wire i___55_carry_i_6_n_0;
  wire i___55_carry_i_7_n_0;
  wire pixel_clk_i;
  wire rst_i;
  wire [2:0]tdata_o;
  wire tdata_o2__0_carry__0_i_1_n_0;
  wire tdata_o2__0_carry__0_i_2_n_0;
  wire tdata_o2__0_carry__0_i_3_n_0;
  wire tdata_o2__0_carry__0_i_4_n_0;
  wire tdata_o2__0_carry__0_i_5_n_0;
  wire tdata_o2__0_carry__0_i_6_n_0;
  wire tdata_o2__0_carry__0_i_7_n_0;
  wire tdata_o2__0_carry__0_i_8_n_0;
  wire tdata_o2__0_carry__0_n_0;
  wire tdata_o2__0_carry__0_n_1;
  wire tdata_o2__0_carry__0_n_2;
  wire tdata_o2__0_carry__0_n_3;
  wire tdata_o2__0_carry__0_n_4;
  wire tdata_o2__0_carry__1_i_1_n_0;
  wire tdata_o2__0_carry__1_i_2_n_0;
  wire tdata_o2__0_carry__1_i_3_n_0;
  wire tdata_o2__0_carry__1_i_4_n_0;
  wire tdata_o2__0_carry__1_i_5_n_0;
  wire tdata_o2__0_carry__1_i_6_n_0;
  wire tdata_o2__0_carry__1_i_7_n_0;
  wire tdata_o2__0_carry__1_n_1;
  wire tdata_o2__0_carry__1_n_2;
  wire tdata_o2__0_carry__1_n_3;
  wire tdata_o2__0_carry__1_n_4;
  wire tdata_o2__0_carry__1_n_5;
  wire tdata_o2__0_carry__1_n_6;
  wire tdata_o2__0_carry__1_n_7;
  wire tdata_o2__0_carry_i_10_n_0;
  wire tdata_o2__0_carry_i_11_n_0;
  wire tdata_o2__0_carry_i_12_n_0;
  wire tdata_o2__0_carry_i_13_n_0;
  wire tdata_o2__0_carry_i_14_n_0;
  wire tdata_o2__0_carry_i_15_n_0;
  wire tdata_o2__0_carry_i_16_n_0;
  wire tdata_o2__0_carry_i_17_n_0;
  wire tdata_o2__0_carry_i_1_n_0;
  wire tdata_o2__0_carry_i_2_n_0;
  wire tdata_o2__0_carry_i_3_n_0;
  wire tdata_o2__0_carry_i_4_n_0;
  wire tdata_o2__0_carry_i_5_n_0;
  wire tdata_o2__0_carry_i_6_n_0;
  wire tdata_o2__0_carry_i_7_n_0;
  wire tdata_o2__0_carry_i_8_n_0;
  wire tdata_o2__0_carry_i_8_n_1;
  wire tdata_o2__0_carry_i_8_n_2;
  wire tdata_o2__0_carry_i_8_n_3;
  wire tdata_o2__0_carry_i_9_n_0;
  wire tdata_o2__0_carry_i_9_n_1;
  wire tdata_o2__0_carry_i_9_n_2;
  wire tdata_o2__0_carry_i_9_n_3;
  wire tdata_o2__0_carry_n_0;
  wire tdata_o2__0_carry_n_1;
  wire tdata_o2__0_carry_n_2;
  wire tdata_o2__0_carry_n_3;
  wire tdata_o2__27_carry__0_i_1_n_0;
  wire tdata_o2__27_carry__0_i_2_n_0;
  wire tdata_o2__27_carry__0_i_3_n_0;
  wire tdata_o2__27_carry__0_i_4_n_0;
  wire tdata_o2__27_carry__0_i_5_n_0;
  wire tdata_o2__27_carry__0_i_6_n_0;
  wire tdata_o2__27_carry__0_i_7_n_0;
  wire tdata_o2__27_carry__0_i_8_n_0;
  wire tdata_o2__27_carry__0_i_9_n_0;
  wire tdata_o2__27_carry__0_n_1;
  wire tdata_o2__27_carry__0_n_2;
  wire tdata_o2__27_carry__0_n_3;
  wire tdata_o2__27_carry_i_1_n_0;
  wire tdata_o2__27_carry_i_2_n_0;
  wire tdata_o2__27_carry_i_3_n_0;
  wire tdata_o2__27_carry_i_4_n_0;
  wire tdata_o2__27_carry_i_5_n_0;
  wire tdata_o2__27_carry_i_6_n_0;
  wire tdata_o2__27_carry_i_7_n_0;
  wire tdata_o2__27_carry_n_0;
  wire tdata_o2__27_carry_n_1;
  wire tdata_o2__27_carry_n_2;
  wire tdata_o2__27_carry_n_3;
  wire \tdata_o2_inferred__0/i___1_carry__0_n_0 ;
  wire \tdata_o2_inferred__0/i___1_carry__0_n_1 ;
  wire \tdata_o2_inferred__0/i___1_carry__0_n_2 ;
  wire \tdata_o2_inferred__0/i___1_carry__0_n_3 ;
  wire \tdata_o2_inferred__0/i___1_carry__1_n_0 ;
  wire \tdata_o2_inferred__0/i___1_carry__1_n_1 ;
  wire \tdata_o2_inferred__0/i___1_carry__1_n_2 ;
  wire \tdata_o2_inferred__0/i___1_carry__1_n_3 ;
  wire \tdata_o2_inferred__0/i___1_carry__1_n_4 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_0 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_1 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_2 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_3 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_4 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_5 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_6 ;
  wire \tdata_o2_inferred__0/i___1_carry__2_n_7 ;
  wire \tdata_o2_inferred__0/i___1_carry__3_n_7 ;
  wire \tdata_o2_inferred__0/i___1_carry_n_0 ;
  wire \tdata_o2_inferred__0/i___1_carry_n_1 ;
  wire \tdata_o2_inferred__0/i___1_carry_n_2 ;
  wire \tdata_o2_inferred__0/i___1_carry_n_3 ;
  wire \tdata_o2_inferred__0/i___37_carry__0_n_2 ;
  wire \tdata_o2_inferred__0/i___37_carry__0_n_3 ;
  wire \tdata_o2_inferred__0/i___37_carry__0_n_5 ;
  wire \tdata_o2_inferred__0/i___37_carry__0_n_6 ;
  wire \tdata_o2_inferred__0/i___37_carry__0_n_7 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_0 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_1 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_2 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_3 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_4 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_5 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_6 ;
  wire \tdata_o2_inferred__0/i___37_carry_n_7 ;
  wire \tdata_o2_inferred__0/i___55_carry__0_n_0 ;
  wire \tdata_o2_inferred__0/i___55_carry__0_n_1 ;
  wire \tdata_o2_inferred__0/i___55_carry__0_n_2 ;
  wire \tdata_o2_inferred__0/i___55_carry__0_n_3 ;
  wire \tdata_o2_inferred__0/i___55_carry_n_0 ;
  wire \tdata_o2_inferred__0/i___55_carry_n_1 ;
  wire \tdata_o2_inferred__0/i___55_carry_n_2 ;
  wire \tdata_o2_inferred__0/i___55_carry_n_3 ;
  wire [9:0]tdata_o3;
  wire \tdata_o[0]_INST_0_i_1_n_0 ;
  wire \tdata_o[0]_INST_0_i_2_n_0 ;
  wire \tdata_o[0]_INST_0_i_3_n_0 ;
  wire \tdata_o[0]_INST_0_i_4_n_0 ;
  wire \tdata_o[16]_INST_0_i_1_n_0 ;
  wire \tdata_o[16]_INST_0_i_2_n_0 ;
  wire \tdata_o[16]_INST_0_i_3_n_0 ;
  wire \tdata_o[16]_INST_0_i_4_n_0 ;
  wire \tdata_o[16]_INST_0_i_5_n_3 ;
  wire \tdata_o[16]_INST_0_i_6_n_2 ;
  wire \tdata_o[16]_INST_0_i_6_n_3 ;
  wire \tdata_o[16]_INST_0_i_6_n_5 ;
  wire \tdata_o[16]_INST_0_i_6_n_6 ;
  wire \tdata_o[16]_INST_0_i_6_n_7 ;
  wire \tdata_o[8]_INST_0_i_1_n_0 ;
  wire \tdata_o[8]_INST_0_i_2_n_0 ;
  wire \tdata_o[8]_INST_0_i_3_n_0 ;
  wire tlast_o;
  wire tlast_o_i_2_n_0;
  wire tready_i;
  wire tuser_o;
  wire tuser_o_i_1_n_0;
  wire tuser_o_i_2_n_0;
  wire tuser_o_i_3_n_0;
  wire tuser_o_i_4_n_0;
  wire tuser_o_i_5_n_0;
  wire tuser_o_i_6_n_0;
  wire tvalid_o;
  wire [10:0]width_s;
  wire \width_s[1]_i_2_n_0 ;
  wire \width_s[1]_i_3_n_0 ;
  wire \width_s[7]_i_2_n_0 ;
  wire \width_s_reg_n_0_[0] ;
  wire \width_s_reg_n_0_[10] ;
  wire \width_s_reg_n_0_[1] ;
  wire \width_s_reg_n_0_[2] ;
  wire \width_s_reg_n_0_[3] ;
  wire \width_s_reg_n_0_[4] ;
  wire \width_s_reg_n_0_[5] ;
  wire \width_s_reg_n_0_[6] ;
  wire \width_s_reg_n_0_[7] ;
  wire \width_s_reg_n_0_[8] ;
  wire \width_s_reg_n_0_[9] ;
  wire [3:0]NLW_tdata_o2__0_carry_O_UNCONNECTED;
  wire [2:0]NLW_tdata_o2__0_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_tdata_o2__0_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_tdata_o2__27_carry_O_UNCONNECTED;
  wire [3:3]NLW_tdata_o2__27_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_tdata_o2__27_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_tdata_o2_inferred__0/i___1_carry_O_UNCONNECTED ;
  wire [3:0]\NLW_tdata_o2_inferred__0/i___1_carry__0_O_UNCONNECTED ;
  wire [2:0]\NLW_tdata_o2_inferred__0/i___1_carry__1_O_UNCONNECTED ;
  wire [3:0]\NLW_tdata_o2_inferred__0/i___1_carry__3_CO_UNCONNECTED ;
  wire [3:1]\NLW_tdata_o2_inferred__0/i___1_carry__3_O_UNCONNECTED ;
  wire [3:2]\NLW_tdata_o2_inferred__0/i___37_carry__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_tdata_o2_inferred__0/i___37_carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_tdata_o2_inferred__0/i___55_carry_O_UNCONNECTED ;
  wire [3:0]\NLW_tdata_o2_inferred__0/i___55_carry__0_O_UNCONNECTED ;
  wire [3:1]\NLW_tdata_o[16]_INST_0_i_5_CO_UNCONNECTED ;
  wire [3:2]\NLW_tdata_o[16]_INST_0_i_5_O_UNCONNECTED ;
  wire [3:2]\NLW_tdata_o[16]_INST_0_i_6_CO_UNCONNECTED ;
  wire [3:3]\NLW_tdata_o[16]_INST_0_i_6_O_UNCONNECTED ;

  LUT1 #(
    .INIT(2'h1)) 
    \fr_cnt_s[0]_i_1 
       (.I0(fr_cnt_s[0]),
        .O(fr_cnt_s_1[0]));
  LUT6 #(
    .INIT(64'h4555AAAA5555AAAA)) 
    \fr_cnt_s[1]_i_1 
       (.I0(fr_cnt_s[1]),
        .I1(\fr_cnt_s[1]_i_2_n_0 ),
        .I2(fr_cnt_s[7]),
        .I3(fr_cnt_s[6]),
        .I4(fr_cnt_s[0]),
        .I5(fr_cnt_s[5]),
        .O(fr_cnt_s_1[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \fr_cnt_s[1]_i_2 
       (.I0(fr_cnt_s[2]),
        .I1(fr_cnt_s[3]),
        .I2(fr_cnt_s[1]),
        .I3(fr_cnt_s[4]),
        .O(\fr_cnt_s[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fr_cnt_s[2]_i_1 
       (.I0(fr_cnt_s[1]),
        .I1(fr_cnt_s[0]),
        .I2(fr_cnt_s[2]),
        .O(fr_cnt_s_1[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \fr_cnt_s[3]_i_1 
       (.I0(fr_cnt_s[2]),
        .I1(fr_cnt_s[0]),
        .I2(fr_cnt_s[1]),
        .I3(fr_cnt_s[3]),
        .O(fr_cnt_s_1[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hAAAA1555)) 
    \fr_cnt_s[4]_i_1 
       (.I0(\fr_cnt_s[7]_i_3_n_0 ),
        .I1(fr_cnt_s[5]),
        .I2(fr_cnt_s[6]),
        .I3(fr_cnt_s[7]),
        .I4(fr_cnt_s[4]),
        .O(fr_cnt_s_1[4]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFF0015AA)) 
    \fr_cnt_s[5]_i_1 
       (.I0(fr_cnt_s[4]),
        .I1(fr_cnt_s[7]),
        .I2(fr_cnt_s[6]),
        .I3(fr_cnt_s[5]),
        .I4(\fr_cnt_s[7]_i_3_n_0 ),
        .O(fr_cnt_s_1[5]));
  LUT5 #(
    .INIT(32'hF01CF0F0)) 
    \fr_cnt_s[6]_i_1 
       (.I0(fr_cnt_s[7]),
        .I1(fr_cnt_s[4]),
        .I2(fr_cnt_s[6]),
        .I3(\fr_cnt_s[7]_i_3_n_0 ),
        .I4(fr_cnt_s[5]),
        .O(fr_cnt_s_1[6]));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \fr_cnt_s[7]_i_1 
       (.I0(tuser_o_i_3_n_0),
        .I1(tuser_o_i_4_n_0),
        .I2(tready_i),
        .I3(height_s[4]),
        .I4(tuser_o_i_5_n_0),
        .I5(tuser_o_i_6_n_0),
        .O(\fr_cnt_s[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hDFDF2000)) 
    \fr_cnt_s[7]_i_2 
       (.I0(fr_cnt_s[5]),
        .I1(\fr_cnt_s[7]_i_3_n_0 ),
        .I2(fr_cnt_s[6]),
        .I3(fr_cnt_s[4]),
        .I4(fr_cnt_s[7]),
        .O(fr_cnt_s_1[7]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \fr_cnt_s[7]_i_3 
       (.I0(fr_cnt_s[2]),
        .I1(fr_cnt_s[0]),
        .I2(fr_cnt_s[1]),
        .I3(fr_cnt_s[3]),
        .O(\fr_cnt_s[7]_i_3_n_0 ));
  FDPE \fr_cnt_s_reg[0] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .D(fr_cnt_s_1[0]),
        .PRE(tuser_o_i_2_n_0),
        .Q(fr_cnt_s[0]));
  FDPE \fr_cnt_s_reg[1] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .D(fr_cnt_s_1[1]),
        .PRE(tuser_o_i_2_n_0),
        .Q(fr_cnt_s[1]));
  FDPE \fr_cnt_s_reg[2] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .D(fr_cnt_s_1[2]),
        .PRE(tuser_o_i_2_n_0),
        .Q(fr_cnt_s[2]));
  FDCE \fr_cnt_s_reg[3] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .CLR(tuser_o_i_2_n_0),
        .D(fr_cnt_s_1[3]),
        .Q(fr_cnt_s[3]));
  FDPE \fr_cnt_s_reg[4] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .D(fr_cnt_s_1[4]),
        .PRE(tuser_o_i_2_n_0),
        .Q(fr_cnt_s[4]));
  FDPE \fr_cnt_s_reg[5] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .D(fr_cnt_s_1[5]),
        .PRE(tuser_o_i_2_n_0),
        .Q(fr_cnt_s[5]));
  FDPE \fr_cnt_s_reg[6] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .D(fr_cnt_s_1[6]),
        .PRE(tuser_o_i_2_n_0),
        .Q(fr_cnt_s[6]));
  FDCE \fr_cnt_s_reg[7] 
       (.C(pixel_clk_i),
        .CE(\fr_cnt_s[7]_i_1_n_0 ),
        .CLR(tuser_o_i_2_n_0),
        .D(fr_cnt_s_1[7]),
        .Q(fr_cnt_s[7]));
  LUT1 #(
    .INIT(2'h1)) 
    \height_s[0]_i_1 
       (.I0(height_s[0]),
        .O(height_s_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \height_s[1]_i_1 
       (.I0(height_s[0]),
        .I1(height_s[1]),
        .O(height_s_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \height_s[2]_i_1 
       (.I0(height_s[1]),
        .I1(height_s[0]),
        .I2(height_s[2]),
        .O(height_s_0[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \height_s[3]_i_1 
       (.I0(height_s[2]),
        .I1(height_s[0]),
        .I2(height_s[1]),
        .I3(height_s[3]),
        .O(height_s_0[3]));
  LUT6 #(
    .INIT(64'h7FFF7FFF80000000)) 
    \height_s[4]_i_1 
       (.I0(height_s[2]),
        .I1(height_s[0]),
        .I2(height_s[1]),
        .I3(height_s[3]),
        .I4(tuser_o_i_3_n_0),
        .I5(height_s[4]),
        .O(height_s_0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \height_s[5]_i_1 
       (.I0(height_s[4]),
        .I1(height_s[2]),
        .I2(height_s[0]),
        .I3(height_s[1]),
        .I4(height_s[3]),
        .I5(height_s[5]),
        .O(height_s_0[5]));
  LUT6 #(
    .INIT(64'hF0F00FB0F0F0F0B0)) 
    \height_s[6]_i_1 
       (.I0(\height_s[7]_i_2_n_0 ),
        .I1(height_s[7]),
        .I2(height_s[6]),
        .I3(height_s[4]),
        .I4(tuser_o_i_5_n_0),
        .I5(height_s[5]),
        .O(height_s_0[6]));
  LUT6 #(
    .INIT(64'hF7F4FFFF08080000)) 
    \height_s[7]_i_1 
       (.I0(height_s[5]),
        .I1(height_s[4]),
        .I2(tuser_o_i_5_n_0),
        .I3(\height_s[7]_i_2_n_0 ),
        .I4(height_s[6]),
        .I5(height_s[7]),
        .O(height_s_0[7]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \height_s[7]_i_2 
       (.I0(height_s[8]),
        .I1(height_s[5]),
        .I2(height_s[9]),
        .O(\height_s[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF7FFFFF00800000)) 
    \height_s[8]_i_1 
       (.I0(height_s[6]),
        .I1(height_s[7]),
        .I2(height_s[4]),
        .I3(tuser_o_i_5_n_0),
        .I4(height_s[5]),
        .I5(height_s[8]),
        .O(height_s_0[8]));
  LUT5 #(
    .INIT(32'h00000008)) 
    \height_s[9]_i_1 
       (.I0(tready_i),
        .I1(\width_s_reg_n_0_[10] ),
        .I2(\width_s_reg_n_0_[9] ),
        .I3(\width_s_reg_n_0_[8] ),
        .I4(tuser_o_i_6_n_0),
        .O(\height_s[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFDFFFFE00200000)) 
    \height_s[9]_i_2 
       (.I0(height_s[5]),
        .I1(tuser_o_i_5_n_0),
        .I2(height_s[4]),
        .I3(\height_s[9]_i_3_n_0 ),
        .I4(height_s[8]),
        .I5(height_s[9]),
        .O(height_s_0[9]));
  LUT2 #(
    .INIT(4'h7)) 
    \height_s[9]_i_3 
       (.I0(height_s[6]),
        .I1(height_s[7]),
        .O(\height_s[9]_i_3_n_0 ));
  FDPE \height_s_reg[0] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .D(height_s_0[0]),
        .PRE(tuser_o_i_2_n_0),
        .Q(height_s[0]));
  FDPE \height_s_reg[1] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .D(height_s_0[1]),
        .PRE(tuser_o_i_2_n_0),
        .Q(height_s[1]));
  FDPE \height_s_reg[2] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .D(height_s_0[2]),
        .PRE(tuser_o_i_2_n_0),
        .Q(height_s[2]));
  FDPE \height_s_reg[3] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .D(height_s_0[3]),
        .PRE(tuser_o_i_2_n_0),
        .Q(height_s[3]));
  FDCE \height_s_reg[4] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .CLR(tuser_o_i_2_n_0),
        .D(height_s_0[4]),
        .Q(height_s[4]));
  FDCE \height_s_reg[5] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .CLR(tuser_o_i_2_n_0),
        .D(height_s_0[5]),
        .Q(height_s[5]));
  FDPE \height_s_reg[6] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .D(height_s_0[6]),
        .PRE(tuser_o_i_2_n_0),
        .Q(height_s[6]));
  FDPE \height_s_reg[7] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .D(height_s_0[7]),
        .PRE(tuser_o_i_2_n_0),
        .Q(height_s[7]));
  FDCE \height_s_reg[8] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .CLR(tuser_o_i_2_n_0),
        .D(height_s_0[8]),
        .Q(height_s[8]));
  FDPE \height_s_reg[9] 
       (.C(pixel_clk_i),
        .CE(\height_s[9]_i_1_n_0 ),
        .D(height_s_0[9]),
        .PRE(tuser_o_i_2_n_0),
        .Q(height_s[9]));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    i___1_carry__0_i_1
       (.I0(i___1_carry_i_1_n_5),
        .I1(i___1_carry__0_i_8_n_5),
        .I2(i___1_carry_i_1_n_7),
        .I3(i___1_carry_i_1_n_6),
        .I4(i___1_carry_i_1_n_4),
        .I5(i___1_carry__0_i_9_n_0),
        .O(i___1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry__0_i_10
       (.I0(\width_s_reg_n_0_[7] ),
        .I1(fr_cnt_s[7]),
        .O(i___1_carry__0_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry__0_i_11
       (.I0(\width_s_reg_n_0_[6] ),
        .I1(fr_cnt_s[6]),
        .O(i___1_carry__0_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry__0_i_12
       (.I0(\width_s_reg_n_0_[5] ),
        .I1(fr_cnt_s[5]),
        .O(i___1_carry__0_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry__0_i_13
       (.I0(\width_s_reg_n_0_[4] ),
        .I1(fr_cnt_s[4]),
        .O(i___1_carry__0_i_13_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    i___1_carry__0_i_2
       (.I0(i___1_carry__0_i_8_n_5),
        .I1(i___1_carry_i_1_n_5),
        .I2(i___1_carry_i_1_n_7),
        .I3(i___1_carry__0_i_8_n_7),
        .O(i___1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    i___1_carry__0_i_3
       (.I0(i___1_carry__0_i_8_n_7),
        .I1(i___1_carry_i_1_n_7),
        .O(i___1_carry__0_i_3_n_0));
  LUT5 #(
    .INIT(32'h6AA6A66A)) 
    i___1_carry__0_i_4
       (.I0(i___1_carry__0_i_1_n_0),
        .I1(i___1_carry__0_i_8_n_7),
        .I2(i___1_carry_i_1_n_7),
        .I3(i___1_carry_i_1_n_5),
        .I4(i___1_carry__0_i_8_n_5),
        .O(i___1_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9A59)) 
    i___1_carry__0_i_5
       (.I0(i___1_carry__0_i_2_n_0),
        .I1(i___1_carry_i_1_n_6),
        .I2(i___1_carry_i_1_n_4),
        .I3(i___1_carry__0_i_8_n_6),
        .O(i___1_carry__0_i_5_n_0));
  LUT5 #(
    .INIT(32'h87787887)) 
    i___1_carry__0_i_6
       (.I0(i___1_carry_i_1_n_7),
        .I1(i___1_carry__0_i_8_n_7),
        .I2(i___1_carry_i_1_n_6),
        .I3(i___1_carry_i_1_n_4),
        .I4(i___1_carry__0_i_8_n_6),
        .O(i___1_carry__0_i_6_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    i___1_carry__0_i_7
       (.I0(i___1_carry__0_i_8_n_7),
        .I1(i___1_carry_i_1_n_7),
        .I2(i___1_carry_i_1_n_5),
        .O(i___1_carry__0_i_7_n_0));
  CARRY4 i___1_carry__0_i_8
       (.CI(i___1_carry_i_1_n_0),
        .CO({i___1_carry__0_i_8_n_0,i___1_carry__0_i_8_n_1,i___1_carry__0_i_8_n_2,i___1_carry__0_i_8_n_3}),
        .CYINIT(1'b0),
        .DI({\width_s_reg_n_0_[7] ,\width_s_reg_n_0_[6] ,\width_s_reg_n_0_[5] ,\width_s_reg_n_0_[4] }),
        .O({i___1_carry__0_i_8_n_4,i___1_carry__0_i_8_n_5,i___1_carry__0_i_8_n_6,i___1_carry__0_i_8_n_7}),
        .S({i___1_carry__0_i_10_n_0,i___1_carry__0_i_11_n_0,i___1_carry__0_i_12_n_0,i___1_carry__0_i_13_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry__0_i_9
       (.I0(i___1_carry__0_i_8_n_6),
        .I1(i___1_carry__0_i_8_n_4),
        .O(i___1_carry__0_i_9_n_0));
  LUT5 #(
    .INIT(32'hE88E8EE8)) 
    i___1_carry__1_i_1
       (.I0(i___1_carry__1_i_9_n_0),
        .I1(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I2(i___1_carry__0_i_8_n_5),
        .I3(i___1_carry__0_i_8_n_7),
        .I4(\tdata_o[16]_INST_0_i_6_n_5 ),
        .O(i___1_carry__1_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry__1_i_10
       (.I0(i___1_carry_i_1_n_4),
        .I1(\tdata_o[16]_INST_0_i_6_n_6 ),
        .O(i___1_carry__1_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h4D)) 
    i___1_carry__1_i_11
       (.I0(i___1_carry__0_i_8_n_4),
        .I1(i___1_carry_i_1_n_6),
        .I2(i___1_carry_i_1_n_4),
        .O(i___1_carry__1_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    i___1_carry__1_i_12
       (.I0(i___1_carry_i_1_n_7),
        .I1(i___1_carry__0_i_8_n_5),
        .I2(i___1_carry_i_1_n_5),
        .O(i___1_carry__1_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h96)) 
    i___1_carry__1_i_13
       (.I0(i___1_carry__0_i_8_n_7),
        .I1(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I2(i___1_carry__0_i_8_n_5),
        .O(i___1_carry__1_i_13_n_0));
  LUT6 #(
    .INIT(64'h4848DE48DE48DEDE)) 
    i___1_carry__1_i_2
       (.I0(i___1_carry__1_i_10_n_0),
        .I1(i___1_carry__0_i_8_n_4),
        .I2(i___1_carry__0_i_8_n_6),
        .I3(i___1_carry_i_1_n_5),
        .I4(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I5(i___1_carry__0_i_8_n_7),
        .O(i___1_carry__1_i_2_n_0));
  LUT5 #(
    .INIT(32'hFF969600)) 
    i___1_carry__1_i_3
       (.I0(i___1_carry_i_1_n_5),
        .I1(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I2(i___1_carry__0_i_8_n_7),
        .I3(i___1_carry__0_i_8_n_5),
        .I4(i___1_carry__1_i_11_n_0),
        .O(i___1_carry__1_i_3_n_0));
  LUT5 #(
    .INIT(32'hE88E8EE8)) 
    i___1_carry__1_i_4
       (.I0(i___1_carry__0_i_8_n_6),
        .I1(i___1_carry__1_i_12_n_0),
        .I2(i___1_carry_i_1_n_4),
        .I3(i___1_carry_i_1_n_6),
        .I4(i___1_carry__0_i_8_n_4),
        .O(i___1_carry__1_i_4_n_0));
  LUT6 #(
    .INIT(64'h9669969669699669)) 
    i___1_carry__1_i_5
       (.I0(i___1_carry__1_i_1_n_0),
        .I1(i___1_carry__0_i_9_n_0),
        .I2(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I3(i___1_carry__0_i_8_n_5),
        .I4(i___1_carry__0_i_8_n_7),
        .I5(\tdata_o[16]_INST_0_i_6_n_5 ),
        .O(i___1_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'h9669969669699669)) 
    i___1_carry__1_i_6
       (.I0(i___1_carry__1_i_2_n_0),
        .I1(i___1_carry__1_i_13_n_0),
        .I2(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I3(i___1_carry__0_i_8_n_6),
        .I4(i___1_carry_i_1_n_4),
        .I5(\tdata_o[16]_INST_0_i_6_n_6 ),
        .O(i___1_carry__1_i_6_n_0));
  LUT6 #(
    .INIT(64'h9669696996969669)) 
    i___1_carry__1_i_7
       (.I0(i___1_carry__1_i_3_n_0),
        .I1(i___1_carry__0_i_9_n_0),
        .I2(i___1_carry__1_i_10_n_0),
        .I3(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I4(i___1_carry__0_i_8_n_7),
        .I5(i___1_carry_i_1_n_5),
        .O(i___1_carry__1_i_7_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    i___1_carry__1_i_8
       (.I0(i___1_carry__1_i_4_n_0),
        .I1(i___1_carry__0_i_8_n_4),
        .I2(i___1_carry_i_1_n_6),
        .I3(i___1_carry_i_1_n_4),
        .I4(i___1_carry__1_i_13_n_0),
        .I5(i___1_carry_i_1_n_5),
        .O(i___1_carry__1_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h4D)) 
    i___1_carry__1_i_9
       (.I0(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I1(i___1_carry_i_1_n_4),
        .I2(i___1_carry__0_i_8_n_6),
        .O(i___1_carry__1_i_9_n_0));
  LUT4 #(
    .INIT(16'h6606)) 
    i___1_carry__2_i_1
       (.I0(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I1(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I3(i___1_carry__0_i_8_n_4),
        .O(i___1_carry__2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry__2_i_10
       (.I0(i___1_carry__0_i_8_n_5),
        .I1(\tdata_o[16]_INST_0_i_6_n_7 ),
        .O(i___1_carry__2_i_10_n_0));
  LUT4 #(
    .INIT(16'h6606)) 
    i___1_carry__2_i_2
       (.I0(i___1_carry__0_i_8_n_4),
        .I1(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I3(i___1_carry__0_i_8_n_5),
        .O(i___1_carry__2_i_2_n_0));
  LUT5 #(
    .INIT(32'hFF6F6606)) 
    i___1_carry__2_i_3
       (.I0(i___1_carry__0_i_8_n_5),
        .I1(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I2(i___1_carry__0_i_8_n_4),
        .I3(i___1_carry__0_i_8_n_6),
        .I4(\tdata_o[16]_INST_0_i_6_n_5 ),
        .O(i___1_carry__2_i_3_n_0));
  LUT6 #(
    .INIT(64'h28BE2828BEBE28BE)) 
    i___1_carry__2_i_4
       (.I0(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I1(i___1_carry__0_i_8_n_6),
        .I2(i___1_carry__0_i_8_n_4),
        .I3(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I4(i___1_carry__0_i_8_n_7),
        .I5(i___1_carry__0_i_8_n_5),
        .O(i___1_carry__2_i_4_n_0));
  LUT4 #(
    .INIT(16'h3783)) 
    i___1_carry__2_i_5
       (.I0(i___1_carry__0_i_8_n_4),
        .I1(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I3(\tdata_o[16]_INST_0_i_6_n_5 ),
        .O(i___1_carry__2_i_5_n_0));
  LUT5 #(
    .INIT(32'hC36C93C3)) 
    i___1_carry__2_i_6
       (.I0(i___1_carry__0_i_8_n_5),
        .I1(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I3(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I4(i___1_carry__0_i_8_n_4),
        .O(i___1_carry__2_i_6_n_0));
  LUT6 #(
    .INIT(64'h7A851FE0E01F7A85)) 
    i___1_carry__2_i_7
       (.I0(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I1(i___1_carry__0_i_8_n_6),
        .I2(i___1_carry__0_i_8_n_4),
        .I3(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I4(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I5(i___1_carry__0_i_8_n_5),
        .O(i___1_carry__2_i_7_n_0));
  LUT6 #(
    .INIT(64'h71E78E188E1871E7)) 
    i___1_carry__2_i_8
       (.I0(i___1_carry__2_i_9_n_0),
        .I1(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I2(i___1_carry__0_i_8_n_4),
        .I3(i___1_carry__0_i_8_n_6),
        .I4(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I5(i___1_carry__2_i_10_n_0),
        .O(i___1_carry__2_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h4D)) 
    i___1_carry__2_i_9
       (.I0(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I1(i___1_carry__0_i_8_n_7),
        .I2(i___1_carry__0_i_8_n_5),
        .O(i___1_carry__2_i_9_n_0));
  LUT3 #(
    .INIT(8'h83)) 
    i___1_carry__3_i_1
       (.I0(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I1(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_6 ),
        .O(i___1_carry__3_i_1_n_0));
  CARRY4 i___1_carry_i_1
       (.CI(1'b0),
        .CO({i___1_carry_i_1_n_0,i___1_carry_i_1_n_1,i___1_carry_i_1_n_2,i___1_carry_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({\width_s_reg_n_0_[3] ,\width_s_reg_n_0_[2] ,\width_s_reg_n_0_[1] ,\width_s_reg_n_0_[0] }),
        .O({i___1_carry_i_1_n_4,i___1_carry_i_1_n_5,i___1_carry_i_1_n_6,i___1_carry_i_1_n_7}),
        .S({i___1_carry_i_5_n_0,i___1_carry_i_6_n_0,i___1_carry_i_7_n_0,i___1_carry_i_8_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    i___1_carry_i_2
       (.I0(i___1_carry_i_1_n_6),
        .I1(i___1_carry_i_1_n_4),
        .O(i___1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___1_carry_i_3
       (.I0(i___1_carry_i_1_n_7),
        .I1(i___1_carry_i_1_n_5),
        .O(i___1_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___1_carry_i_4
       (.I0(i___1_carry_i_1_n_6),
        .O(i___1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry_i_5
       (.I0(\width_s_reg_n_0_[3] ),
        .I1(fr_cnt_s[3]),
        .O(i___1_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry_i_6
       (.I0(\width_s_reg_n_0_[2] ),
        .I1(fr_cnt_s[2]),
        .O(i___1_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry_i_7
       (.I0(\width_s_reg_n_0_[1] ),
        .I1(fr_cnt_s[1]),
        .O(i___1_carry_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___1_carry_i_8
       (.I0(\width_s_reg_n_0_[0] ),
        .I1(fr_cnt_s[0]),
        .O(i___1_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___37_carry__0_i_1
       (.I0(\tdata_o2_inferred__0/i___1_carry__3_n_7 ),
        .I1(\tdata_o2_inferred__0/i___1_carry__2_n_5 ),
        .O(i___37_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___37_carry_i_1
       (.I0(\tdata_o2_inferred__0/i___1_carry__2_n_4 ),
        .I1(\tdata_o2_inferred__0/i___1_carry__2_n_6 ),
        .O(i___37_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___37_carry_i_2
       (.I0(\tdata_o2_inferred__0/i___1_carry__2_n_5 ),
        .I1(\tdata_o2_inferred__0/i___1_carry__2_n_7 ),
        .O(i___37_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___37_carry_i_3
       (.I0(\tdata_o2_inferred__0/i___1_carry__2_n_6 ),
        .I1(\tdata_o2_inferred__0/i___1_carry__1_n_4 ),
        .O(i___37_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i___55_carry__0_i_1
       (.I0(\tdata_o2_inferred__0/i___37_carry__0_n_6 ),
        .I1(\tdata_o[16]_INST_0_i_6_n_6 ),
        .O(i___55_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i___55_carry__0_i_2
       (.I0(\tdata_o2_inferred__0/i___37_carry__0_n_7 ),
        .I1(\tdata_o[16]_INST_0_i_6_n_7 ),
        .O(i___55_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i___55_carry__0_i_3
       (.I0(\tdata_o2_inferred__0/i___37_carry_n_4 ),
        .I1(i___1_carry__0_i_8_n_4),
        .O(i___55_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i___55_carry__0_i_4
       (.I0(\tdata_o2_inferred__0/i___37_carry_n_5 ),
        .I1(i___1_carry__0_i_8_n_5),
        .O(i___55_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    i___55_carry__0_i_5
       (.I0(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I1(\tdata_o2_inferred__0/i___37_carry__0_n_6 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I3(\tdata_o2_inferred__0/i___37_carry__0_n_5 ),
        .O(i___55_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    i___55_carry__0_i_6
       (.I0(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I1(\tdata_o2_inferred__0/i___37_carry__0_n_7 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_6 ),
        .I3(\tdata_o2_inferred__0/i___37_carry__0_n_6 ),
        .O(i___55_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    i___55_carry__0_i_7
       (.I0(i___1_carry__0_i_8_n_4),
        .I1(\tdata_o2_inferred__0/i___37_carry_n_4 ),
        .I2(\tdata_o[16]_INST_0_i_6_n_7 ),
        .I3(\tdata_o2_inferred__0/i___37_carry__0_n_7 ),
        .O(i___55_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    i___55_carry__0_i_8
       (.I0(i___1_carry__0_i_8_n_5),
        .I1(\tdata_o2_inferred__0/i___37_carry_n_5 ),
        .I2(i___1_carry__0_i_8_n_4),
        .I3(\tdata_o2_inferred__0/i___37_carry_n_4 ),
        .O(i___55_carry__0_i_8_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    i___55_carry_i_1
       (.I0(\tdata_o2_inferred__0/i___37_carry_n_6 ),
        .I1(i___1_carry__0_i_8_n_6),
        .O(i___55_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i___55_carry_i_2
       (.I0(\tdata_o2_inferred__0/i___37_carry_n_7 ),
        .I1(i___1_carry__0_i_8_n_7),
        .O(i___55_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    i___55_carry_i_3
       (.I0(\tdata_o2_inferred__0/i___1_carry__1_n_4 ),
        .I1(i___1_carry_i_1_n_4),
        .O(i___55_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    i___55_carry_i_4
       (.I0(i___1_carry__0_i_8_n_6),
        .I1(\tdata_o2_inferred__0/i___37_carry_n_6 ),
        .I2(i___1_carry__0_i_8_n_5),
        .I3(\tdata_o2_inferred__0/i___37_carry_n_5 ),
        .O(i___55_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h4BB4)) 
    i___55_carry_i_5
       (.I0(i___1_carry__0_i_8_n_7),
        .I1(\tdata_o2_inferred__0/i___37_carry_n_7 ),
        .I2(\tdata_o2_inferred__0/i___37_carry_n_6 ),
        .I3(i___1_carry__0_i_8_n_6),
        .O(i___55_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    i___55_carry_i_6
       (.I0(i___1_carry_i_1_n_4),
        .I1(\tdata_o2_inferred__0/i___1_carry__1_n_4 ),
        .I2(i___1_carry__0_i_8_n_7),
        .I3(\tdata_o2_inferred__0/i___37_carry_n_7 ),
        .O(i___55_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___55_carry_i_7
       (.I0(i___1_carry_i_1_n_4),
        .I1(\tdata_o2_inferred__0/i___1_carry__1_n_4 ),
        .O(i___55_carry_i_7_n_0));
  CARRY4 tdata_o2__0_carry
       (.CI(1'b0),
        .CO({tdata_o2__0_carry_n_0,tdata_o2__0_carry_n_1,tdata_o2__0_carry_n_2,tdata_o2__0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({tdata_o2__0_carry_i_1_n_0,tdata_o2__0_carry_i_2_n_0,tdata_o2__0_carry_i_3_n_0,1'b0}),
        .O(NLW_tdata_o2__0_carry_O_UNCONNECTED[3:0]),
        .S({tdata_o2__0_carry_i_4_n_0,tdata_o2__0_carry_i_5_n_0,tdata_o2__0_carry_i_6_n_0,tdata_o2__0_carry_i_7_n_0}));
  CARRY4 tdata_o2__0_carry__0
       (.CI(tdata_o2__0_carry_n_0),
        .CO({tdata_o2__0_carry__0_n_0,tdata_o2__0_carry__0_n_1,tdata_o2__0_carry__0_n_2,tdata_o2__0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({tdata_o2__0_carry__0_i_1_n_0,tdata_o2__0_carry__0_i_2_n_0,tdata_o2__0_carry__0_i_3_n_0,tdata_o2__0_carry__0_i_4_n_0}),
        .O({tdata_o2__0_carry__0_n_4,NLW_tdata_o2__0_carry__0_O_UNCONNECTED[2:0]}),
        .S({tdata_o2__0_carry__0_i_5_n_0,tdata_o2__0_carry__0_i_6_n_0,tdata_o2__0_carry__0_i_7_n_0,tdata_o2__0_carry__0_i_8_n_0}));
  LUT3 #(
    .INIT(8'hB2)) 
    tdata_o2__0_carry__0_i_1
       (.I0(tdata_o3[4]),
        .I1(tdata_o3[6]),
        .I2(tdata_o3[9]),
        .O(tdata_o2__0_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    tdata_o2__0_carry__0_i_2
       (.I0(tdata_o3[3]),
        .I1(tdata_o3[5]),
        .I2(tdata_o3[8]),
        .O(tdata_o2__0_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    tdata_o2__0_carry__0_i_3
       (.I0(tdata_o3[2]),
        .I1(tdata_o3[4]),
        .I2(tdata_o3[7]),
        .O(tdata_o2__0_carry__0_i_3_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    tdata_o2__0_carry__0_i_4
       (.I0(tdata_o3[1]),
        .I1(tdata_o3[3]),
        .I2(tdata_o3[6]),
        .O(tdata_o2__0_carry__0_i_4_n_0));
  LUT5 #(
    .INIT(32'h4DB2B24D)) 
    tdata_o2__0_carry__0_i_5
       (.I0(tdata_o3[9]),
        .I1(tdata_o3[6]),
        .I2(tdata_o3[4]),
        .I3(tdata_o3[7]),
        .I4(tdata_o3[5]),
        .O(tdata_o2__0_carry__0_i_5_n_0));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    tdata_o2__0_carry__0_i_6
       (.I0(tdata_o3[8]),
        .I1(tdata_o3[5]),
        .I2(tdata_o3[3]),
        .I3(tdata_o3[4]),
        .I4(tdata_o3[6]),
        .I5(tdata_o3[9]),
        .O(tdata_o2__0_carry__0_i_6_n_0));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    tdata_o2__0_carry__0_i_7
       (.I0(tdata_o3[7]),
        .I1(tdata_o3[4]),
        .I2(tdata_o3[2]),
        .I3(tdata_o3[3]),
        .I4(tdata_o3[5]),
        .I5(tdata_o3[8]),
        .O(tdata_o2__0_carry__0_i_7_n_0));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    tdata_o2__0_carry__0_i_8
       (.I0(tdata_o3[6]),
        .I1(tdata_o3[3]),
        .I2(tdata_o3[1]),
        .I3(tdata_o3[2]),
        .I4(tdata_o3[4]),
        .I5(tdata_o3[7]),
        .O(tdata_o2__0_carry__0_i_8_n_0));
  CARRY4 tdata_o2__0_carry__1
       (.CI(tdata_o2__0_carry__0_n_0),
        .CO({NLW_tdata_o2__0_carry__1_CO_UNCONNECTED[3],tdata_o2__0_carry__1_n_1,tdata_o2__0_carry__1_n_2,tdata_o2__0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,tdata_o2__0_carry__1_i_1_n_0,tdata_o2__0_carry__1_i_2_n_0,tdata_o2__0_carry__1_i_3_n_0}),
        .O({tdata_o2__0_carry__1_n_4,tdata_o2__0_carry__1_n_5,tdata_o2__0_carry__1_n_6,tdata_o2__0_carry__1_n_7}),
        .S({tdata_o2__0_carry__1_i_4_n_0,tdata_o2__0_carry__1_i_5_n_0,tdata_o2__0_carry__1_i_6_n_0,tdata_o2__0_carry__1_i_7_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    tdata_o2__0_carry__1_i_1
       (.I0(tdata_o3[7]),
        .I1(tdata_o3[9]),
        .O(tdata_o2__0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    tdata_o2__0_carry__1_i_2
       (.I0(tdata_o3[6]),
        .I1(tdata_o3[8]),
        .O(tdata_o2__0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    tdata_o2__0_carry__1_i_3
       (.I0(tdata_o3[5]),
        .I1(tdata_o3[7]),
        .O(tdata_o2__0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    tdata_o2__0_carry__1_i_4
       (.I0(tdata_o3[8]),
        .I1(tdata_o3[9]),
        .O(tdata_o2__0_carry__1_i_4_n_0));
  LUT3 #(
    .INIT(8'h4B)) 
    tdata_o2__0_carry__1_i_5
       (.I0(tdata_o3[9]),
        .I1(tdata_o3[7]),
        .I2(tdata_o3[8]),
        .O(tdata_o2__0_carry__1_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    tdata_o2__0_carry__1_i_6
       (.I0(tdata_o3[8]),
        .I1(tdata_o3[6]),
        .I2(tdata_o3[9]),
        .I3(tdata_o3[7]),
        .O(tdata_o2__0_carry__1_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    tdata_o2__0_carry__1_i_7
       (.I0(tdata_o3[7]),
        .I1(tdata_o3[5]),
        .I2(tdata_o3[8]),
        .I3(tdata_o3[6]),
        .O(tdata_o2__0_carry__1_i_7_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    tdata_o2__0_carry_i_1
       (.I0(tdata_o3[0]),
        .I1(tdata_o3[2]),
        .I2(tdata_o3[5]),
        .O(tdata_o2__0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_10
       (.I0(height_s[3]),
        .I1(fr_cnt_s[3]),
        .O(tdata_o2__0_carry_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_11
       (.I0(height_s[2]),
        .I1(fr_cnt_s[2]),
        .O(tdata_o2__0_carry_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_12
       (.I0(height_s[1]),
        .I1(fr_cnt_s[1]),
        .O(tdata_o2__0_carry_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_13
       (.I0(height_s[0]),
        .I1(fr_cnt_s[0]),
        .O(tdata_o2__0_carry_i_13_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_14
       (.I0(height_s[7]),
        .I1(fr_cnt_s[7]),
        .O(tdata_o2__0_carry_i_14_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_15
       (.I0(height_s[6]),
        .I1(fr_cnt_s[6]),
        .O(tdata_o2__0_carry_i_15_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_16
       (.I0(height_s[5]),
        .I1(fr_cnt_s[5]),
        .O(tdata_o2__0_carry_i_16_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_17
       (.I0(height_s[4]),
        .I1(fr_cnt_s[4]),
        .O(tdata_o2__0_carry_i_17_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    tdata_o2__0_carry_i_2
       (.I0(tdata_o3[5]),
        .I1(tdata_o3[2]),
        .I2(tdata_o3[0]),
        .O(tdata_o2__0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    tdata_o2__0_carry_i_3
       (.I0(tdata_o3[3]),
        .I1(tdata_o3[0]),
        .O(tdata_o2__0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    tdata_o2__0_carry_i_4
       (.I0(tdata_o3[5]),
        .I1(tdata_o3[2]),
        .I2(tdata_o3[0]),
        .I3(tdata_o3[1]),
        .I4(tdata_o3[3]),
        .I5(tdata_o3[6]),
        .O(tdata_o2__0_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'h69966969)) 
    tdata_o2__0_carry_i_5
       (.I0(tdata_o3[0]),
        .I1(tdata_o3[2]),
        .I2(tdata_o3[5]),
        .I3(tdata_o3[1]),
        .I4(tdata_o3[4]),
        .O(tdata_o2__0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    tdata_o2__0_carry_i_6
       (.I0(tdata_o3[0]),
        .I1(tdata_o3[3]),
        .I2(tdata_o3[4]),
        .I3(tdata_o3[1]),
        .O(tdata_o2__0_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__0_carry_i_7
       (.I0(tdata_o3[3]),
        .I1(tdata_o3[0]),
        .O(tdata_o2__0_carry_i_7_n_0));
  CARRY4 tdata_o2__0_carry_i_8
       (.CI(1'b0),
        .CO({tdata_o2__0_carry_i_8_n_0,tdata_o2__0_carry_i_8_n_1,tdata_o2__0_carry_i_8_n_2,tdata_o2__0_carry_i_8_n_3}),
        .CYINIT(1'b0),
        .DI(height_s[3:0]),
        .O(tdata_o3[3:0]),
        .S({tdata_o2__0_carry_i_10_n_0,tdata_o2__0_carry_i_11_n_0,tdata_o2__0_carry_i_12_n_0,tdata_o2__0_carry_i_13_n_0}));
  CARRY4 tdata_o2__0_carry_i_9
       (.CI(tdata_o2__0_carry_i_8_n_0),
        .CO({tdata_o2__0_carry_i_9_n_0,tdata_o2__0_carry_i_9_n_1,tdata_o2__0_carry_i_9_n_2,tdata_o2__0_carry_i_9_n_3}),
        .CYINIT(1'b0),
        .DI(height_s[7:4]),
        .O(tdata_o3[7:4]),
        .S({tdata_o2__0_carry_i_14_n_0,tdata_o2__0_carry_i_15_n_0,tdata_o2__0_carry_i_16_n_0,tdata_o2__0_carry_i_17_n_0}));
  CARRY4 tdata_o2__27_carry
       (.CI(1'b0),
        .CO({tdata_o2__27_carry_n_0,tdata_o2__27_carry_n_1,tdata_o2__27_carry_n_2,tdata_o2__27_carry_n_3}),
        .CYINIT(1'b0),
        .DI({tdata_o2__27_carry_i_1_n_0,tdata_o2__27_carry_i_2_n_0,tdata_o2__27_carry_i_3_n_0,1'b0}),
        .O(NLW_tdata_o2__27_carry_O_UNCONNECTED[3:0]),
        .S({tdata_o2__27_carry_i_4_n_0,tdata_o2__27_carry_i_5_n_0,tdata_o2__27_carry_i_6_n_0,tdata_o2__27_carry_i_7_n_0}));
  CARRY4 tdata_o2__27_carry__0
       (.CI(tdata_o2__27_carry_n_0),
        .CO({NLW_tdata_o2__27_carry__0_CO_UNCONNECTED[3],tdata_o2__27_carry__0_n_1,tdata_o2__27_carry__0_n_2,tdata_o2__27_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,tdata_o2__27_carry__0_i_1_n_0,tdata_o2__27_carry__0_i_2_n_0,tdata_o2__27_carry__0_i_3_n_0}),
        .O(NLW_tdata_o2__27_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,tdata_o2__27_carry__0_i_4_n_0,tdata_o2__27_carry__0_i_5_n_0,tdata_o2__27_carry__0_i_6_n_0}));
  LUT6 #(
    .INIT(64'h00000000015FF800)) 
    tdata_o2__27_carry__0_i_1
       (.I0(tdata_o2__0_carry__1_n_7),
        .I1(tdata_o2__0_carry__0_n_4),
        .I2(tdata_o2__0_carry__1_n_4),
        .I3(tdata_o2__0_carry__1_n_6),
        .I4(tdata_o2__0_carry__1_n_5),
        .I5(tdata_o3[8]),
        .O(tdata_o2__27_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h00000000E7871878)) 
    tdata_o2__27_carry__0_i_2
       (.I0(tdata_o2__0_carry__1_n_7),
        .I1(tdata_o2__0_carry__1_n_5),
        .I2(tdata_o2__0_carry__1_n_6),
        .I3(tdata_o2__0_carry__0_n_4),
        .I4(tdata_o2__0_carry__1_n_4),
        .I5(tdata_o3[7]),
        .O(tdata_o2__27_carry__0_i_2_n_0));
  LUT5 #(
    .INIT(32'h00009666)) 
    tdata_o2__27_carry__0_i_3
       (.I0(tdata_o2__0_carry__1_n_5),
        .I1(tdata_o2__0_carry__1_n_7),
        .I2(tdata_o2__0_carry__0_n_4),
        .I3(tdata_o2__0_carry__1_n_6),
        .I4(tdata_o3[6]),
        .O(tdata_o2__27_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__27_carry__0_i_4
       (.I0(tdata_o2__27_carry__0_i_1_n_0),
        .I1(tdata_o2__27_carry__0_i_7_n_0),
        .O(tdata_o2__27_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__27_carry__0_i_5
       (.I0(tdata_o2__27_carry__0_i_2_n_0),
        .I1(tdata_o2__27_carry__0_i_8_n_0),
        .O(tdata_o2__27_carry__0_i_5_n_0));
  LUT6 #(
    .INIT(64'hBFEAEABF40151540)) 
    tdata_o2__27_carry__0_i_6
       (.I0(tdata_o3[6]),
        .I1(tdata_o2__0_carry__1_n_6),
        .I2(tdata_o2__0_carry__0_n_4),
        .I3(tdata_o2__0_carry__1_n_7),
        .I4(tdata_o2__0_carry__1_n_5),
        .I5(tdata_o2__27_carry__0_i_9_n_0),
        .O(tdata_o2__27_carry__0_i_6_n_0));
  LUT6 #(
    .INIT(64'h11C8FF00EE3700FF)) 
    tdata_o2__27_carry__0_i_7
       (.I0(tdata_o2__0_carry__1_n_7),
        .I1(tdata_o2__0_carry__1_n_6),
        .I2(tdata_o2__0_carry__0_n_4),
        .I3(tdata_o2__0_carry__1_n_4),
        .I4(tdata_o2__0_carry__1_n_5),
        .I5(tdata_o3[9]),
        .O(tdata_o2__27_carry__0_i_7_n_0));
  LUT6 #(
    .INIT(64'h6569655969596999)) 
    tdata_o2__27_carry__0_i_8
       (.I0(tdata_o3[8]),
        .I1(tdata_o2__0_carry__1_n_5),
        .I2(tdata_o2__0_carry__1_n_6),
        .I3(tdata_o2__0_carry__1_n_4),
        .I4(tdata_o2__0_carry__0_n_4),
        .I5(tdata_o2__0_carry__1_n_7),
        .O(tdata_o2__27_carry__0_i_8_n_0));
  LUT6 #(
    .INIT(64'h9996699996666999)) 
    tdata_o2__27_carry__0_i_9
       (.I0(tdata_o2__0_carry__1_n_4),
        .I1(tdata_o3[7]),
        .I2(tdata_o2__0_carry__1_n_5),
        .I3(tdata_o2__0_carry__1_n_7),
        .I4(tdata_o2__0_carry__1_n_6),
        .I5(tdata_o2__0_carry__0_n_4),
        .O(tdata_o2__27_carry__0_i_9_n_0));
  LUT3 #(
    .INIT(8'h6F)) 
    tdata_o2__27_carry_i_1
       (.I0(tdata_o2__0_carry__0_n_4),
        .I1(tdata_o2__0_carry__1_n_6),
        .I2(tdata_o3[5]),
        .O(tdata_o2__27_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    tdata_o2__27_carry_i_2
       (.I0(tdata_o2__0_carry__1_n_7),
        .I1(tdata_o3[4]),
        .O(tdata_o2__27_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    tdata_o2__27_carry_i_3
       (.I0(tdata_o2__0_carry__0_n_4),
        .I1(tdata_o3[3]),
        .O(tdata_o2__27_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h96C3C369693C3C96)) 
    tdata_o2__27_carry_i_4
       (.I0(tdata_o3[5]),
        .I1(tdata_o2__0_carry__1_n_5),
        .I2(tdata_o2__0_carry__1_n_7),
        .I3(tdata_o2__0_carry__0_n_4),
        .I4(tdata_o2__0_carry__1_n_6),
        .I5(tdata_o3[6]),
        .O(tdata_o2__27_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'hB44B4BB4)) 
    tdata_o2__27_carry_i_5
       (.I0(tdata_o3[4]),
        .I1(tdata_o2__0_carry__1_n_7),
        .I2(tdata_o3[5]),
        .I3(tdata_o2__0_carry__0_n_4),
        .I4(tdata_o2__0_carry__1_n_6),
        .O(tdata_o2__27_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    tdata_o2__27_carry_i_6
       (.I0(tdata_o3[3]),
        .I1(tdata_o2__0_carry__0_n_4),
        .I2(tdata_o3[4]),
        .I3(tdata_o2__0_carry__1_n_7),
        .O(tdata_o2__27_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tdata_o2__27_carry_i_7
       (.I0(tdata_o3[3]),
        .I1(tdata_o2__0_carry__0_n_4),
        .O(tdata_o2__27_carry_i_7_n_0));
  CARRY4 \tdata_o2_inferred__0/i___1_carry 
       (.CI(1'b0),
        .CO({\tdata_o2_inferred__0/i___1_carry_n_0 ,\tdata_o2_inferred__0/i___1_carry_n_1 ,\tdata_o2_inferred__0/i___1_carry_n_2 ,\tdata_o2_inferred__0/i___1_carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i___1_carry_i_1_n_6,i___1_carry_i_1_n_7,1'b0,1'b1}),
        .O(\NLW_tdata_o2_inferred__0/i___1_carry_O_UNCONNECTED [3:0]),
        .S({i___1_carry_i_2_n_0,i___1_carry_i_3_n_0,i___1_carry_i_4_n_0,i___1_carry_i_1_n_7}));
  CARRY4 \tdata_o2_inferred__0/i___1_carry__0 
       (.CI(\tdata_o2_inferred__0/i___1_carry_n_0 ),
        .CO({\tdata_o2_inferred__0/i___1_carry__0_n_0 ,\tdata_o2_inferred__0/i___1_carry__0_n_1 ,\tdata_o2_inferred__0/i___1_carry__0_n_2 ,\tdata_o2_inferred__0/i___1_carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i___1_carry__0_i_1_n_0,i___1_carry__0_i_2_n_0,i___1_carry__0_i_3_n_0,i___1_carry_i_1_n_5}),
        .O(\NLW_tdata_o2_inferred__0/i___1_carry__0_O_UNCONNECTED [3:0]),
        .S({i___1_carry__0_i_4_n_0,i___1_carry__0_i_5_n_0,i___1_carry__0_i_6_n_0,i___1_carry__0_i_7_n_0}));
  CARRY4 \tdata_o2_inferred__0/i___1_carry__1 
       (.CI(\tdata_o2_inferred__0/i___1_carry__0_n_0 ),
        .CO({\tdata_o2_inferred__0/i___1_carry__1_n_0 ,\tdata_o2_inferred__0/i___1_carry__1_n_1 ,\tdata_o2_inferred__0/i___1_carry__1_n_2 ,\tdata_o2_inferred__0/i___1_carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({i___1_carry__1_i_1_n_0,i___1_carry__1_i_2_n_0,i___1_carry__1_i_3_n_0,i___1_carry__1_i_4_n_0}),
        .O({\tdata_o2_inferred__0/i___1_carry__1_n_4 ,\NLW_tdata_o2_inferred__0/i___1_carry__1_O_UNCONNECTED [2:0]}),
        .S({i___1_carry__1_i_5_n_0,i___1_carry__1_i_6_n_0,i___1_carry__1_i_7_n_0,i___1_carry__1_i_8_n_0}));
  CARRY4 \tdata_o2_inferred__0/i___1_carry__2 
       (.CI(\tdata_o2_inferred__0/i___1_carry__1_n_0 ),
        .CO({\tdata_o2_inferred__0/i___1_carry__2_n_0 ,\tdata_o2_inferred__0/i___1_carry__2_n_1 ,\tdata_o2_inferred__0/i___1_carry__2_n_2 ,\tdata_o2_inferred__0/i___1_carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({i___1_carry__2_i_1_n_0,i___1_carry__2_i_2_n_0,i___1_carry__2_i_3_n_0,i___1_carry__2_i_4_n_0}),
        .O({\tdata_o2_inferred__0/i___1_carry__2_n_4 ,\tdata_o2_inferred__0/i___1_carry__2_n_5 ,\tdata_o2_inferred__0/i___1_carry__2_n_6 ,\tdata_o2_inferred__0/i___1_carry__2_n_7 }),
        .S({i___1_carry__2_i_5_n_0,i___1_carry__2_i_6_n_0,i___1_carry__2_i_7_n_0,i___1_carry__2_i_8_n_0}));
  CARRY4 \tdata_o2_inferred__0/i___1_carry__3 
       (.CI(\tdata_o2_inferred__0/i___1_carry__2_n_0 ),
        .CO(\NLW_tdata_o2_inferred__0/i___1_carry__3_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_tdata_o2_inferred__0/i___1_carry__3_O_UNCONNECTED [3:1],\tdata_o2_inferred__0/i___1_carry__3_n_7 }),
        .S({1'b0,1'b0,1'b0,i___1_carry__3_i_1_n_0}));
  CARRY4 \tdata_o2_inferred__0/i___37_carry 
       (.CI(1'b0),
        .CO({\tdata_o2_inferred__0/i___37_carry_n_0 ,\tdata_o2_inferred__0/i___37_carry_n_1 ,\tdata_o2_inferred__0/i___37_carry_n_2 ,\tdata_o2_inferred__0/i___37_carry_n_3 }),
        .CYINIT(1'b0),
        .DI({\tdata_o2_inferred__0/i___1_carry__2_n_4 ,\tdata_o2_inferred__0/i___1_carry__2_n_5 ,\tdata_o2_inferred__0/i___1_carry__2_n_6 ,1'b0}),
        .O({\tdata_o2_inferred__0/i___37_carry_n_4 ,\tdata_o2_inferred__0/i___37_carry_n_5 ,\tdata_o2_inferred__0/i___37_carry_n_6 ,\tdata_o2_inferred__0/i___37_carry_n_7 }),
        .S({i___37_carry_i_1_n_0,i___37_carry_i_2_n_0,i___37_carry_i_3_n_0,\tdata_o2_inferred__0/i___1_carry__2_n_7 }));
  CARRY4 \tdata_o2_inferred__0/i___37_carry__0 
       (.CI(\tdata_o2_inferred__0/i___37_carry_n_0 ),
        .CO({\NLW_tdata_o2_inferred__0/i___37_carry__0_CO_UNCONNECTED [3:2],\tdata_o2_inferred__0/i___37_carry__0_n_2 ,\tdata_o2_inferred__0/i___37_carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\tdata_o2_inferred__0/i___1_carry__3_n_7 }),
        .O({\NLW_tdata_o2_inferred__0/i___37_carry__0_O_UNCONNECTED [3],\tdata_o2_inferred__0/i___37_carry__0_n_5 ,\tdata_o2_inferred__0/i___37_carry__0_n_6 ,\tdata_o2_inferred__0/i___37_carry__0_n_7 }),
        .S({1'b0,\tdata_o2_inferred__0/i___1_carry__3_n_7 ,\tdata_o2_inferred__0/i___1_carry__2_n_4 ,i___37_carry__0_i_1_n_0}));
  CARRY4 \tdata_o2_inferred__0/i___55_carry 
       (.CI(1'b0),
        .CO({\tdata_o2_inferred__0/i___55_carry_n_0 ,\tdata_o2_inferred__0/i___55_carry_n_1 ,\tdata_o2_inferred__0/i___55_carry_n_2 ,\tdata_o2_inferred__0/i___55_carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i___55_carry_i_1_n_0,i___55_carry_i_2_n_0,i___55_carry_i_3_n_0,1'b0}),
        .O(\NLW_tdata_o2_inferred__0/i___55_carry_O_UNCONNECTED [3:0]),
        .S({i___55_carry_i_4_n_0,i___55_carry_i_5_n_0,i___55_carry_i_6_n_0,i___55_carry_i_7_n_0}));
  CARRY4 \tdata_o2_inferred__0/i___55_carry__0 
       (.CI(\tdata_o2_inferred__0/i___55_carry_n_0 ),
        .CO({\tdata_o2_inferred__0/i___55_carry__0_n_0 ,\tdata_o2_inferred__0/i___55_carry__0_n_1 ,\tdata_o2_inferred__0/i___55_carry__0_n_2 ,\tdata_o2_inferred__0/i___55_carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i___55_carry__0_i_1_n_0,i___55_carry__0_i_2_n_0,i___55_carry__0_i_3_n_0,i___55_carry__0_i_4_n_0}),
        .O(\NLW_tdata_o2_inferred__0/i___55_carry__0_O_UNCONNECTED [3:0]),
        .S({i___55_carry__0_i_5_n_0,i___55_carry__0_i_6_n_0,i___55_carry__0_i_7_n_0,i___55_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h4440)) 
    \tdata_o[0]_INST_0 
       (.I0(\tdata_o[0]_INST_0_i_1_n_0 ),
        .I1(rst_i),
        .I2(\tdata_o[0]_INST_0_i_2_n_0 ),
        .I3(\tdata_o[16]_INST_0_i_3_n_0 ),
        .O(tdata_o[0]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0BF4)) 
    \tdata_o[0]_INST_0_i_1 
       (.I0(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I1(\tdata_o2_inferred__0/i___37_carry__0_n_5 ),
        .I2(\tdata_o2_inferred__0/i___55_carry__0_n_0 ),
        .I3(\tdata_o2_inferred__0/i___1_carry__1_n_4 ),
        .O(\tdata_o[0]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0F0F00000101FF00)) 
    \tdata_o[0]_INST_0_i_2 
       (.I0(tdata_o2__0_carry__1_n_6),
        .I1(\tdata_o[0]_INST_0_i_3_n_0 ),
        .I2(tdata_o2__27_carry__0_n_1),
        .I3(\tdata_o[0]_INST_0_i_4_n_0 ),
        .I4(tdata_o2__0_carry__0_n_4),
        .I5(tdata_o3[9]),
        .O(\tdata_o[0]_INST_0_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \tdata_o[0]_INST_0_i_3 
       (.I0(tdata_o2__0_carry__1_n_5),
        .I1(tdata_o2__0_carry__1_n_7),
        .O(\tdata_o[0]_INST_0_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h42AA)) 
    \tdata_o[0]_INST_0_i_4 
       (.I0(tdata_o2__0_carry__1_n_4),
        .I1(tdata_o2__0_carry__1_n_6),
        .I2(tdata_o2__0_carry__1_n_7),
        .I3(tdata_o2__0_carry__1_n_5),
        .O(\tdata_o[0]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF0800000000)) 
    \tdata_o[16]_INST_0 
       (.I0(\tdata_o[16]_INST_0_i_1_n_0 ),
        .I1(tdata_o2__0_carry__0_n_4),
        .I2(tdata_o2__27_carry__0_n_1),
        .I3(\tdata_o[16]_INST_0_i_2_n_0 ),
        .I4(\tdata_o[16]_INST_0_i_3_n_0 ),
        .I5(\tdata_o[16]_INST_0_i_4_n_0 ),
        .O(tdata_o[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFF40)) 
    \tdata_o[16]_INST_0_i_1 
       (.I0(tdata_o2__0_carry__1_n_6),
        .I1(tdata_o2__0_carry__1_n_5),
        .I2(tdata_o2__0_carry__1_n_7),
        .I3(tdata_o3[9]),
        .O(\tdata_o[16]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0101011110000000)) 
    \tdata_o[16]_INST_0_i_2 
       (.I0(tdata_o3[9]),
        .I1(tdata_o2__0_carry__0_n_4),
        .I2(tdata_o2__0_carry__1_n_5),
        .I3(tdata_o2__0_carry__1_n_7),
        .I4(tdata_o2__0_carry__1_n_6),
        .I5(tdata_o2__0_carry__1_n_4),
        .O(\tdata_o[16]_INST_0_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40AA15AA)) 
    \tdata_o[16]_INST_0_i_3 
       (.I0(tdata_o2__27_carry__0_n_1),
        .I1(tdata_o2__0_carry__1_n_5),
        .I2(tdata_o2__0_carry__1_n_6),
        .I3(tdata_o2__0_carry__0_n_4),
        .I4(tdata_o2__0_carry__1_n_4),
        .O(\tdata_o[16]_INST_0_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h66560000)) 
    \tdata_o[16]_INST_0_i_4 
       (.I0(\tdata_o2_inferred__0/i___1_carry__1_n_4 ),
        .I1(\tdata_o2_inferred__0/i___55_carry__0_n_0 ),
        .I2(\tdata_o2_inferred__0/i___37_carry__0_n_5 ),
        .I3(\tdata_o[16]_INST_0_i_6_n_5 ),
        .I4(rst_i),
        .O(\tdata_o[16]_INST_0_i_4_n_0 ));
  CARRY4 \tdata_o[16]_INST_0_i_5 
       (.CI(tdata_o2__0_carry_i_9_n_0),
        .CO({\NLW_tdata_o[16]_INST_0_i_5_CO_UNCONNECTED [3:1],\tdata_o[16]_INST_0_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_tdata_o[16]_INST_0_i_5_O_UNCONNECTED [3:2],tdata_o3[9:8]}),
        .S({1'b0,1'b0,height_s[9:8]}));
  CARRY4 \tdata_o[16]_INST_0_i_6 
       (.CI(i___1_carry__0_i_8_n_0),
        .CO({\NLW_tdata_o[16]_INST_0_i_6_CO_UNCONNECTED [3:2],\tdata_o[16]_INST_0_i_6_n_2 ,\tdata_o[16]_INST_0_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_tdata_o[16]_INST_0_i_6_O_UNCONNECTED [3],\tdata_o[16]_INST_0_i_6_n_5 ,\tdata_o[16]_INST_0_i_6_n_6 ,\tdata_o[16]_INST_0_i_6_n_7 }),
        .S({1'b0,\width_s_reg_n_0_[10] ,\width_s_reg_n_0_[9] ,\width_s_reg_n_0_[8] }));
  LUT6 #(
    .INIT(64'hFF00AA00AE00AE00)) 
    \tdata_o[8]_INST_0 
       (.I0(\tdata_o[8]_INST_0_i_1_n_0 ),
        .I1(\tdata_o[8]_INST_0_i_2_n_0 ),
        .I2(tdata_o2__27_carry__0_n_1),
        .I3(\tdata_o[16]_INST_0_i_4_n_0 ),
        .I4(\tdata_o[8]_INST_0_i_3_n_0 ),
        .I5(tdata_o2__0_carry__0_n_4),
        .O(tdata_o[1]));
  LUT5 #(
    .INIT(32'h04000000)) 
    \tdata_o[8]_INST_0_i_1 
       (.I0(tdata_o2__0_carry__1_n_4),
        .I1(tdata_o2__0_carry__1_n_5),
        .I2(tdata_o3[9]),
        .I3(tdata_o2__0_carry__0_n_4),
        .I4(tdata_o2__0_carry__1_n_6),
        .O(\tdata_o[8]_INST_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFFC8FF7F)) 
    \tdata_o[8]_INST_0_i_2 
       (.I0(tdata_o2__0_carry__1_n_6),
        .I1(tdata_o2__0_carry__1_n_5),
        .I2(tdata_o2__0_carry__1_n_7),
        .I3(tdata_o3[9]),
        .I4(tdata_o2__0_carry__1_n_4),
        .O(\tdata_o[8]_INST_0_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00001F00)) 
    \tdata_o[8]_INST_0_i_3 
       (.I0(tdata_o2__0_carry__1_n_6),
        .I1(tdata_o2__0_carry__1_n_7),
        .I2(tdata_o2__0_carry__1_n_5),
        .I3(tdata_o2__0_carry__1_n_4),
        .I4(tdata_o3[9]),
        .I5(tdata_o2__27_carry__0_n_1),
        .O(\tdata_o[8]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000020000000000)) 
    tlast_o_i_1
       (.I0(\width_s_reg_n_0_[10] ),
        .I1(\width_s_reg_n_0_[9] ),
        .I2(\width_s_reg_n_0_[8] ),
        .I3(\width_s_reg_n_0_[1] ),
        .I4(\width_s_reg_n_0_[0] ),
        .I5(tlast_o_i_2_n_0),
        .O(__2));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    tlast_o_i_2
       (.I0(\width_s_reg_n_0_[4] ),
        .I1(\width_s_reg_n_0_[5] ),
        .I2(\width_s_reg_n_0_[2] ),
        .I3(\width_s_reg_n_0_[3] ),
        .I4(\width_s_reg_n_0_[7] ),
        .I5(\width_s_reg_n_0_[6] ),
        .O(tlast_o_i_2_n_0));
  FDCE tlast_o_reg
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(tuser_o_i_2_n_0),
        .D(__2),
        .Q(tlast_o));
  LUT5 #(
    .INIT(32'h00000010)) 
    tuser_o_i_1
       (.I0(tuser_o_i_3_n_0),
        .I1(height_s[4]),
        .I2(tuser_o_i_4_n_0),
        .I3(tuser_o_i_5_n_0),
        .I4(tuser_o_i_6_n_0),
        .O(tuser_o_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    tuser_o_i_2
       (.I0(rst_i),
        .O(tuser_o_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFDFFFFFF)) 
    tuser_o_i_3
       (.I0(height_s[9]),
        .I1(height_s[5]),
        .I2(height_s[8]),
        .I3(height_s[7]),
        .I4(height_s[6]),
        .O(tuser_o_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h02)) 
    tuser_o_i_4
       (.I0(\width_s_reg_n_0_[10] ),
        .I1(\width_s_reg_n_0_[9] ),
        .I2(\width_s_reg_n_0_[8] ),
        .O(tuser_o_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    tuser_o_i_5
       (.I0(height_s[2]),
        .I1(height_s[0]),
        .I2(height_s[1]),
        .I3(height_s[3]),
        .O(tuser_o_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    tuser_o_i_6
       (.I0(\width_s_reg_n_0_[6] ),
        .I1(\width_s[7]_i_2_n_0 ),
        .I2(\width_s_reg_n_0_[7] ),
        .O(tuser_o_i_6_n_0));
  FDCE tuser_o_reg
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(tuser_o_i_2_n_0),
        .D(tuser_o_i_1_n_0),
        .Q(tuser_o));
  FDCE tvalid_o_reg
       (.C(pixel_clk_i),
        .CE(1'b1),
        .CLR(tuser_o_i_2_n_0),
        .D(1'b1),
        .Q(tvalid_o));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \width_s[0]_i_1 
       (.I0(\width_s_reg_n_0_[0] ),
        .O(width_s[0]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hAA68)) 
    \width_s[10]_i_1 
       (.I0(\width_s_reg_n_0_[10] ),
        .I1(\width_s_reg_n_0_[9] ),
        .I2(\width_s_reg_n_0_[8] ),
        .I3(tuser_o_i_6_n_0),
        .O(width_s[10]));
  LUT6 #(
    .INIT(64'h55545555AAAAAAAA)) 
    \width_s[1]_i_1 
       (.I0(\width_s_reg_n_0_[0] ),
        .I1(\width_s[1]_i_2_n_0 ),
        .I2(\width_s[1]_i_3_n_0 ),
        .I3(\width_s_reg_n_0_[8] ),
        .I4(\width_s_reg_n_0_[10] ),
        .I5(\width_s_reg_n_0_[1] ),
        .O(width_s[1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \width_s[1]_i_2 
       (.I0(\width_s_reg_n_0_[7] ),
        .I1(\width_s_reg_n_0_[0] ),
        .I2(\width_s_reg_n_0_[5] ),
        .I3(\width_s_reg_n_0_[6] ),
        .O(\width_s[1]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \width_s[1]_i_3 
       (.I0(\width_s_reg_n_0_[3] ),
        .I1(\width_s_reg_n_0_[4] ),
        .I2(\width_s_reg_n_0_[2] ),
        .I3(\width_s_reg_n_0_[9] ),
        .O(\width_s[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \width_s[2]_i_1 
       (.I0(\width_s_reg_n_0_[1] ),
        .I1(\width_s_reg_n_0_[0] ),
        .I2(\width_s_reg_n_0_[2] ),
        .O(width_s[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \width_s[3]_i_1 
       (.I0(\width_s_reg_n_0_[2] ),
        .I1(\width_s_reg_n_0_[0] ),
        .I2(\width_s_reg_n_0_[1] ),
        .I3(\width_s_reg_n_0_[3] ),
        .O(width_s[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \width_s[4]_i_1 
       (.I0(\width_s_reg_n_0_[3] ),
        .I1(\width_s_reg_n_0_[1] ),
        .I2(\width_s_reg_n_0_[0] ),
        .I3(\width_s_reg_n_0_[2] ),
        .I4(\width_s_reg_n_0_[4] ),
        .O(width_s[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \width_s[5]_i_1 
       (.I0(\width_s_reg_n_0_[4] ),
        .I1(\width_s_reg_n_0_[2] ),
        .I2(\width_s_reg_n_0_[0] ),
        .I3(\width_s_reg_n_0_[1] ),
        .I4(\width_s_reg_n_0_[3] ),
        .I5(\width_s_reg_n_0_[5] ),
        .O(width_s[5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \width_s[6]_i_1 
       (.I0(\width_s[7]_i_2_n_0 ),
        .I1(\width_s_reg_n_0_[6] ),
        .O(width_s[6]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \width_s[7]_i_1 
       (.I0(\width_s_reg_n_0_[6] ),
        .I1(\width_s[7]_i_2_n_0 ),
        .I2(\width_s_reg_n_0_[7] ),
        .O(width_s[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \width_s[7]_i_2 
       (.I0(\width_s_reg_n_0_[4] ),
        .I1(\width_s_reg_n_0_[2] ),
        .I2(\width_s_reg_n_0_[0] ),
        .I3(\width_s_reg_n_0_[1] ),
        .I4(\width_s_reg_n_0_[3] ),
        .I5(\width_s_reg_n_0_[5] ),
        .O(\width_s[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hAA51)) 
    \width_s[8]_i_1 
       (.I0(tuser_o_i_6_n_0),
        .I1(\width_s_reg_n_0_[10] ),
        .I2(\width_s_reg_n_0_[9] ),
        .I3(\width_s_reg_n_0_[8] ),
        .O(width_s[8]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \width_s[9]_i_1 
       (.I0(\width_s_reg_n_0_[8] ),
        .I1(tuser_o_i_6_n_0),
        .I2(\width_s_reg_n_0_[9] ),
        .O(width_s[9]));
  FDPE \width_s_reg[0] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[0]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[0] ));
  FDPE \width_s_reg[10] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[10]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[10] ));
  FDPE \width_s_reg[1] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[1]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[1] ));
  FDPE \width_s_reg[2] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[2]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[2] ));
  FDPE \width_s_reg[3] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[3]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[3] ));
  FDPE \width_s_reg[4] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[4]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[4] ));
  FDPE \width_s_reg[5] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[5]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[5] ));
  FDPE \width_s_reg[6] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[6]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[6] ));
  FDPE \width_s_reg[7] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .D(width_s[7]),
        .PRE(tuser_o_i_2_n_0),
        .Q(\width_s_reg_n_0_[7] ));
  FDCE \width_s_reg[8] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .CLR(tuser_o_i_2_n_0),
        .D(width_s[8]),
        .Q(\width_s_reg_n_0_[8] ));
  FDCE \width_s_reg[9] 
       (.C(pixel_clk_i),
        .CE(tready_i),
        .CLR(tuser_o_i_2_n_0),
        .D(width_s[9]),
        .Q(\width_s_reg_n_0_[9] ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
