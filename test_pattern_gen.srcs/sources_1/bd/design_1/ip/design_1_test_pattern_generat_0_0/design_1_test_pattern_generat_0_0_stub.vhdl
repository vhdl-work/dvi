-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Fri Jul 17 19:49:39 2020
-- Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /mnt/Data1/FPGA_Projects/DVI/test_pattern_gen.srcs/sources_1/bd/design_1/ip/design_1_test_pattern_generat_0_0/design_1_test_pattern_generat_0_0_stub.vhdl
-- Design      : design_1_test_pattern_generat_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_test_pattern_generat_0_0 is
  Port ( 
    pixel_clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    tready_i : in STD_LOGIC;
    tdata_o : out STD_LOGIC_VECTOR ( 23 downto 0 );
    tuser_o : out STD_LOGIC;
    tlast_o : out STD_LOGIC;
    tvalid_o : out STD_LOGIC
  );

end design_1_test_pattern_generat_0_0;

architecture stub of design_1_test_pattern_generat_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "pixel_clk_i,rst_i,tready_i,tdata_o[23:0],tuser_o,tlast_o,tvalid_o";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "test_pattern_generator,Vivado 2019.2";
begin
end;
