-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Fri Jul 17 19:49:40 2020
-- Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /mnt/Data1/FPGA_Projects/DVI/test_pattern_gen.srcs/sources_1/bd/design_1/ip/design_1_test_pattern_generat_0_0/design_1_test_pattern_generat_0_0_sim_netlist.vhdl
-- Design      : design_1_test_pattern_generat_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_test_pattern_generat_0_0_test_pattern_generator is
  port (
    tuser_o : out STD_LOGIC;
    tlast_o : out STD_LOGIC;
    tvalid_o : out STD_LOGIC;
    tdata_o : out STD_LOGIC_VECTOR ( 2 downto 0 );
    tready_i : in STD_LOGIC;
    pixel_clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_test_pattern_generat_0_0_test_pattern_generator : entity is "test_pattern_generator";
end design_1_test_pattern_generat_0_0_test_pattern_generator;

architecture STRUCTURE of design_1_test_pattern_generat_0_0_test_pattern_generator is
  signal \__2\ : STD_LOGIC;
  signal fr_cnt_s : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \fr_cnt_s[1]_i_2_n_0\ : STD_LOGIC;
  signal \fr_cnt_s[7]_i_1_n_0\ : STD_LOGIC;
  signal \fr_cnt_s[7]_i_3_n_0\ : STD_LOGIC;
  signal fr_cnt_s_1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal height_s : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \height_s[7]_i_2_n_0\ : STD_LOGIC;
  signal \height_s[9]_i_1_n_0\ : STD_LOGIC;
  signal \height_s[9]_i_3_n_0\ : STD_LOGIC;
  signal height_s_0 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \i___1_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_12_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_13_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_1\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_2\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_3\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_4\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_5\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_6\ : STD_LOGIC;
  signal \i___1_carry__0_i_8_n_7\ : STD_LOGIC;
  signal \i___1_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_13_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \i___1_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \i___1_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \i___1_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_1\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_2\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_3\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_4\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_5\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_6\ : STD_LOGIC;
  signal \i___1_carry_i_1_n_7\ : STD_LOGIC;
  signal \i___1_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_7_n_0\ : STD_LOGIC;
  signal \i___1_carry_i_8_n_0\ : STD_LOGIC;
  signal \i___37_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___37_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___37_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___37_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___55_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___55_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___55_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___55_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___55_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___55_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___55_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___55_carry_i_7_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_n_1\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_n_2\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_n_3\ : STD_LOGIC;
  signal \tdata_o2__0_carry__0_n_4\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_n_1\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_n_2\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_n_3\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_n_4\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_n_5\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_n_6\ : STD_LOGIC;
  signal \tdata_o2__0_carry__1_n_7\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_10_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_11_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_12_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_13_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_14_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_15_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_16_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_17_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_3_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_4_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_5_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_6_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_7_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_8_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_8_n_1\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_8_n_2\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_8_n_3\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_9_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_9_n_1\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_9_n_2\ : STD_LOGIC;
  signal \tdata_o2__0_carry_i_9_n_3\ : STD_LOGIC;
  signal \tdata_o2__0_carry_n_0\ : STD_LOGIC;
  signal \tdata_o2__0_carry_n_1\ : STD_LOGIC;
  signal \tdata_o2__0_carry_n_2\ : STD_LOGIC;
  signal \tdata_o2__0_carry_n_3\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_n_1\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_n_2\ : STD_LOGIC;
  signal \tdata_o2__27_carry__0_n_3\ : STD_LOGIC;
  signal \tdata_o2__27_carry_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_i_3_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_i_4_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_i_5_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_i_6_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_i_7_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_n_0\ : STD_LOGIC;
  signal \tdata_o2__27_carry_n_1\ : STD_LOGIC;
  signal \tdata_o2__27_carry_n_2\ : STD_LOGIC;
  signal \tdata_o2__27_carry_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__0_n_0\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__0_n_1\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__0_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__0_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__1_n_0\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__1_n_1\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__1_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__1_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__1_n_4\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_0\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_1\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_4\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_5\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_6\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__2_n_7\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry__3_n_7\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry_n_0\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry_n_1\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___1_carry_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry__0_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry__0_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry__0_n_5\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry__0_n_6\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry__0_n_7\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_0\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_1\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_4\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_5\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_6\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___37_carry_n_7\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry__0_n_0\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry__0_n_1\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry__0_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry__0_n_3\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry_n_0\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry_n_1\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry_n_2\ : STD_LOGIC;
  signal \tdata_o2_inferred__0/i___55_carry_n_3\ : STD_LOGIC;
  signal tdata_o3 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \tdata_o[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o[0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o[0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \tdata_o[0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_5_n_3\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_6_n_2\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_6_n_3\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_6_n_5\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_6_n_6\ : STD_LOGIC;
  signal \tdata_o[16]_INST_0_i_6_n_7\ : STD_LOGIC;
  signal \tdata_o[8]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \tdata_o[8]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \tdata_o[8]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal tlast_o_i_2_n_0 : STD_LOGIC;
  signal tuser_o_i_1_n_0 : STD_LOGIC;
  signal tuser_o_i_2_n_0 : STD_LOGIC;
  signal tuser_o_i_3_n_0 : STD_LOGIC;
  signal tuser_o_i_4_n_0 : STD_LOGIC;
  signal tuser_o_i_5_n_0 : STD_LOGIC;
  signal tuser_o_i_6_n_0 : STD_LOGIC;
  signal width_s : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \width_s[1]_i_2_n_0\ : STD_LOGIC;
  signal \width_s[1]_i_3_n_0\ : STD_LOGIC;
  signal \width_s[7]_i_2_n_0\ : STD_LOGIC;
  signal \width_s_reg_n_0_[0]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[10]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[1]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[2]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[3]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[4]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[5]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[6]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[7]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[8]\ : STD_LOGIC;
  signal \width_s_reg_n_0_[9]\ : STD_LOGIC;
  signal \NLW_tdata_o2__0_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o2__0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_tdata_o2__0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_tdata_o2__27_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o2__27_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_tdata_o2__27_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o2_inferred__0/i___1_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o2_inferred__0/i___1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o2_inferred__0/i___1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_tdata_o2_inferred__0/i___1_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o2_inferred__0/i___1_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_tdata_o2_inferred__0/i___37_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tdata_o2_inferred__0/i___37_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_tdata_o2_inferred__0/i___55_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o2_inferred__0/i___55_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdata_o[16]_INST_0_i_5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_tdata_o[16]_INST_0_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tdata_o[16]_INST_0_i_6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tdata_o[16]_INST_0_i_6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \fr_cnt_s[1]_i_2\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \fr_cnt_s[2]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \fr_cnt_s[3]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \fr_cnt_s[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \fr_cnt_s[5]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \fr_cnt_s[7]_i_3\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \height_s[1]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \height_s[2]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \height_s[3]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \height_s[7]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \i___1_carry__0_i_9\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \i___1_carry__1_i_10\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \i___1_carry__1_i_11\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \i___1_carry__1_i_12\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \i___1_carry__1_i_13\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \i___1_carry__1_i_9\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \i___1_carry__2_i_10\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \i___1_carry__2_i_9\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \tdata_o[0]_INST_0_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tdata_o[0]_INST_0_i_3\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \tdata_o[0]_INST_0_i_4\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \tdata_o[16]_INST_0_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \tdata_o[16]_INST_0_i_4\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tdata_o[8]_INST_0_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of tuser_o_i_3 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of tuser_o_i_4 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of tuser_o_i_5 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of tuser_o_i_6 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \width_s[0]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \width_s[10]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \width_s[1]_i_2\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \width_s[2]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \width_s[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \width_s[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \width_s[6]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \width_s[7]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \width_s[8]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \width_s[9]_i_1\ : label is "soft_lutpair7";
begin
\fr_cnt_s[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => fr_cnt_s(0),
      O => fr_cnt_s_1(0)
    );
\fr_cnt_s[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4555AAAA5555AAAA"
    )
        port map (
      I0 => fr_cnt_s(1),
      I1 => \fr_cnt_s[1]_i_2_n_0\,
      I2 => fr_cnt_s(7),
      I3 => fr_cnt_s(6),
      I4 => fr_cnt_s(0),
      I5 => fr_cnt_s(5),
      O => fr_cnt_s_1(1)
    );
\fr_cnt_s[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => fr_cnt_s(2),
      I1 => fr_cnt_s(3),
      I2 => fr_cnt_s(1),
      I3 => fr_cnt_s(4),
      O => \fr_cnt_s[1]_i_2_n_0\
    );
\fr_cnt_s[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => fr_cnt_s(1),
      I1 => fr_cnt_s(0),
      I2 => fr_cnt_s(2),
      O => fr_cnt_s_1(2)
    );
\fr_cnt_s[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => fr_cnt_s(2),
      I1 => fr_cnt_s(0),
      I2 => fr_cnt_s(1),
      I3 => fr_cnt_s(3),
      O => fr_cnt_s_1(3)
    );
\fr_cnt_s[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA1555"
    )
        port map (
      I0 => \fr_cnt_s[7]_i_3_n_0\,
      I1 => fr_cnt_s(5),
      I2 => fr_cnt_s(6),
      I3 => fr_cnt_s(7),
      I4 => fr_cnt_s(4),
      O => fr_cnt_s_1(4)
    );
\fr_cnt_s[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF0015AA"
    )
        port map (
      I0 => fr_cnt_s(4),
      I1 => fr_cnt_s(7),
      I2 => fr_cnt_s(6),
      I3 => fr_cnt_s(5),
      I4 => \fr_cnt_s[7]_i_3_n_0\,
      O => fr_cnt_s_1(5)
    );
\fr_cnt_s[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F01CF0F0"
    )
        port map (
      I0 => fr_cnt_s(7),
      I1 => fr_cnt_s(4),
      I2 => fr_cnt_s(6),
      I3 => \fr_cnt_s[7]_i_3_n_0\,
      I4 => fr_cnt_s(5),
      O => fr_cnt_s_1(6)
    );
\fr_cnt_s[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000040"
    )
        port map (
      I0 => tuser_o_i_3_n_0,
      I1 => tuser_o_i_4_n_0,
      I2 => tready_i,
      I3 => height_s(4),
      I4 => tuser_o_i_5_n_0,
      I5 => tuser_o_i_6_n_0,
      O => \fr_cnt_s[7]_i_1_n_0\
    );
\fr_cnt_s[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFDF2000"
    )
        port map (
      I0 => fr_cnt_s(5),
      I1 => \fr_cnt_s[7]_i_3_n_0\,
      I2 => fr_cnt_s(6),
      I3 => fr_cnt_s(4),
      I4 => fr_cnt_s(7),
      O => fr_cnt_s_1(7)
    );
\fr_cnt_s[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => fr_cnt_s(2),
      I1 => fr_cnt_s(0),
      I2 => fr_cnt_s(1),
      I3 => fr_cnt_s(3),
      O => \fr_cnt_s[7]_i_3_n_0\
    );
\fr_cnt_s_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      D => fr_cnt_s_1(0),
      PRE => tuser_o_i_2_n_0,
      Q => fr_cnt_s(0)
    );
\fr_cnt_s_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      D => fr_cnt_s_1(1),
      PRE => tuser_o_i_2_n_0,
      Q => fr_cnt_s(1)
    );
\fr_cnt_s_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      D => fr_cnt_s_1(2),
      PRE => tuser_o_i_2_n_0,
      Q => fr_cnt_s(2)
    );
\fr_cnt_s_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      CLR => tuser_o_i_2_n_0,
      D => fr_cnt_s_1(3),
      Q => fr_cnt_s(3)
    );
\fr_cnt_s_reg[4]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      D => fr_cnt_s_1(4),
      PRE => tuser_o_i_2_n_0,
      Q => fr_cnt_s(4)
    );
\fr_cnt_s_reg[5]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      D => fr_cnt_s_1(5),
      PRE => tuser_o_i_2_n_0,
      Q => fr_cnt_s(5)
    );
\fr_cnt_s_reg[6]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      D => fr_cnt_s_1(6),
      PRE => tuser_o_i_2_n_0,
      Q => fr_cnt_s(6)
    );
\fr_cnt_s_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \fr_cnt_s[7]_i_1_n_0\,
      CLR => tuser_o_i_2_n_0,
      D => fr_cnt_s_1(7),
      Q => fr_cnt_s(7)
    );
\height_s[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => height_s(0),
      O => height_s_0(0)
    );
\height_s[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(0),
      I1 => height_s(1),
      O => height_s_0(1)
    );
\height_s[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => height_s(1),
      I1 => height_s(0),
      I2 => height_s(2),
      O => height_s_0(2)
    );
\height_s[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => height_s(2),
      I1 => height_s(0),
      I2 => height_s(1),
      I3 => height_s(3),
      O => height_s_0(3)
    );
\height_s[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFF7FFF80000000"
    )
        port map (
      I0 => height_s(2),
      I1 => height_s(0),
      I2 => height_s(1),
      I3 => height_s(3),
      I4 => tuser_o_i_3_n_0,
      I5 => height_s(4),
      O => height_s_0(4)
    );
\height_s[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => height_s(4),
      I1 => height_s(2),
      I2 => height_s(0),
      I3 => height_s(1),
      I4 => height_s(3),
      I5 => height_s(5),
      O => height_s_0(5)
    );
\height_s[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F00FB0F0F0F0B0"
    )
        port map (
      I0 => \height_s[7]_i_2_n_0\,
      I1 => height_s(7),
      I2 => height_s(6),
      I3 => height_s(4),
      I4 => tuser_o_i_5_n_0,
      I5 => height_s(5),
      O => height_s_0(6)
    );
\height_s[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7F4FFFF08080000"
    )
        port map (
      I0 => height_s(5),
      I1 => height_s(4),
      I2 => tuser_o_i_5_n_0,
      I3 => \height_s[7]_i_2_n_0\,
      I4 => height_s(6),
      I5 => height_s(7),
      O => height_s_0(7)
    );
\height_s[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => height_s(8),
      I1 => height_s(5),
      I2 => height_s(9),
      O => \height_s[7]_i_2_n_0\
    );
\height_s[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF7FFFFF00800000"
    )
        port map (
      I0 => height_s(6),
      I1 => height_s(7),
      I2 => height_s(4),
      I3 => tuser_o_i_5_n_0,
      I4 => height_s(5),
      I5 => height_s(8),
      O => height_s_0(8)
    );
\height_s[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => tready_i,
      I1 => \width_s_reg_n_0_[10]\,
      I2 => \width_s_reg_n_0_[9]\,
      I3 => \width_s_reg_n_0_[8]\,
      I4 => tuser_o_i_6_n_0,
      O => \height_s[9]_i_1_n_0\
    );
\height_s[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFE00200000"
    )
        port map (
      I0 => height_s(5),
      I1 => tuser_o_i_5_n_0,
      I2 => height_s(4),
      I3 => \height_s[9]_i_3_n_0\,
      I4 => height_s(8),
      I5 => height_s(9),
      O => height_s_0(9)
    );
\height_s[9]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => height_s(6),
      I1 => height_s(7),
      O => \height_s[9]_i_3_n_0\
    );
\height_s_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      D => height_s_0(0),
      PRE => tuser_o_i_2_n_0,
      Q => height_s(0)
    );
\height_s_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      D => height_s_0(1),
      PRE => tuser_o_i_2_n_0,
      Q => height_s(1)
    );
\height_s_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      D => height_s_0(2),
      PRE => tuser_o_i_2_n_0,
      Q => height_s(2)
    );
\height_s_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      D => height_s_0(3),
      PRE => tuser_o_i_2_n_0,
      Q => height_s(3)
    );
\height_s_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      CLR => tuser_o_i_2_n_0,
      D => height_s_0(4),
      Q => height_s(4)
    );
\height_s_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      CLR => tuser_o_i_2_n_0,
      D => height_s_0(5),
      Q => height_s(5)
    );
\height_s_reg[6]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      D => height_s_0(6),
      PRE => tuser_o_i_2_n_0,
      Q => height_s(6)
    );
\height_s_reg[7]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      D => height_s_0(7),
      PRE => tuser_o_i_2_n_0,
      Q => height_s(7)
    );
\height_s_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      CLR => tuser_o_i_2_n_0,
      D => height_s_0(8),
      Q => height_s(8)
    );
\height_s_reg[9]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => \height_s[9]_i_1_n_0\,
      D => height_s_0(9),
      PRE => tuser_o_i_2_n_0,
      Q => height_s(9)
    );
\i___1_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8E71718E718E8E71"
    )
        port map (
      I0 => \i___1_carry_i_1_n_5\,
      I1 => \i___1_carry__0_i_8_n_5\,
      I2 => \i___1_carry_i_1_n_7\,
      I3 => \i___1_carry_i_1_n_6\,
      I4 => \i___1_carry_i_1_n_4\,
      I5 => \i___1_carry__0_i_9_n_0\,
      O => \i___1_carry__0_i_1_n_0\
    );
\i___1_carry__0_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[7]\,
      I1 => fr_cnt_s(7),
      O => \i___1_carry__0_i_10_n_0\
    );
\i___1_carry__0_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[6]\,
      I1 => fr_cnt_s(6),
      O => \i___1_carry__0_i_11_n_0\
    );
\i___1_carry__0_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[5]\,
      I1 => fr_cnt_s(5),
      O => \i___1_carry__0_i_12_n_0\
    );
\i___1_carry__0_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[4]\,
      I1 => fr_cnt_s(4),
      O => \i___1_carry__0_i_13_n_0\
    );
\i___1_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_5\,
      I1 => \i___1_carry_i_1_n_5\,
      I2 => \i___1_carry_i_1_n_7\,
      I3 => \i___1_carry__0_i_8_n_7\,
      O => \i___1_carry__0_i_2_n_0\
    );
\i___1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_7\,
      I1 => \i___1_carry_i_1_n_7\,
      O => \i___1_carry__0_i_3_n_0\
    );
\i___1_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AA6A66A"
    )
        port map (
      I0 => \i___1_carry__0_i_1_n_0\,
      I1 => \i___1_carry__0_i_8_n_7\,
      I2 => \i___1_carry_i_1_n_7\,
      I3 => \i___1_carry_i_1_n_5\,
      I4 => \i___1_carry__0_i_8_n_5\,
      O => \i___1_carry__0_i_4_n_0\
    );
\i___1_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A59"
    )
        port map (
      I0 => \i___1_carry__0_i_2_n_0\,
      I1 => \i___1_carry_i_1_n_6\,
      I2 => \i___1_carry_i_1_n_4\,
      I3 => \i___1_carry__0_i_8_n_6\,
      O => \i___1_carry__0_i_5_n_0\
    );
\i___1_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"87787887"
    )
        port map (
      I0 => \i___1_carry_i_1_n_7\,
      I1 => \i___1_carry__0_i_8_n_7\,
      I2 => \i___1_carry_i_1_n_6\,
      I3 => \i___1_carry_i_1_n_4\,
      I4 => \i___1_carry__0_i_8_n_6\,
      O => \i___1_carry__0_i_6_n_0\
    );
\i___1_carry__0_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_7\,
      I1 => \i___1_carry_i_1_n_7\,
      I2 => \i___1_carry_i_1_n_5\,
      O => \i___1_carry__0_i_7_n_0\
    );
\i___1_carry__0_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \i___1_carry_i_1_n_0\,
      CO(3) => \i___1_carry__0_i_8_n_0\,
      CO(2) => \i___1_carry__0_i_8_n_1\,
      CO(1) => \i___1_carry__0_i_8_n_2\,
      CO(0) => \i___1_carry__0_i_8_n_3\,
      CYINIT => '0',
      DI(3) => \width_s_reg_n_0_[7]\,
      DI(2) => \width_s_reg_n_0_[6]\,
      DI(1) => \width_s_reg_n_0_[5]\,
      DI(0) => \width_s_reg_n_0_[4]\,
      O(3) => \i___1_carry__0_i_8_n_4\,
      O(2) => \i___1_carry__0_i_8_n_5\,
      O(1) => \i___1_carry__0_i_8_n_6\,
      O(0) => \i___1_carry__0_i_8_n_7\,
      S(3) => \i___1_carry__0_i_10_n_0\,
      S(2) => \i___1_carry__0_i_11_n_0\,
      S(1) => \i___1_carry__0_i_12_n_0\,
      S(0) => \i___1_carry__0_i_13_n_0\
    );
\i___1_carry__0_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_6\,
      I1 => \i___1_carry__0_i_8_n_4\,
      O => \i___1_carry__0_i_9_n_0\
    );
\i___1_carry__1_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E88E8EE8"
    )
        port map (
      I0 => \i___1_carry__1_i_9_n_0\,
      I1 => \tdata_o[16]_INST_0_i_6_n_7\,
      I2 => \i___1_carry__0_i_8_n_5\,
      I3 => \i___1_carry__0_i_8_n_7\,
      I4 => \tdata_o[16]_INST_0_i_6_n_5\,
      O => \i___1_carry__1_i_1_n_0\
    );
\i___1_carry__1_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___1_carry_i_1_n_4\,
      I1 => \tdata_o[16]_INST_0_i_6_n_6\,
      O => \i___1_carry__1_i_10_n_0\
    );
\i___1_carry__1_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_4\,
      I1 => \i___1_carry_i_1_n_6\,
      I2 => \i___1_carry_i_1_n_4\,
      O => \i___1_carry__1_i_11_n_0\
    );
\i___1_carry__1_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \i___1_carry_i_1_n_7\,
      I1 => \i___1_carry__0_i_8_n_5\,
      I2 => \i___1_carry_i_1_n_5\,
      O => \i___1_carry__1_i_12_n_0\
    );
\i___1_carry__1_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_7\,
      I1 => \tdata_o[16]_INST_0_i_6_n_7\,
      I2 => \i___1_carry__0_i_8_n_5\,
      O => \i___1_carry__1_i_13_n_0\
    );
\i___1_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4848DE48DE48DEDE"
    )
        port map (
      I0 => \i___1_carry__1_i_10_n_0\,
      I1 => \i___1_carry__0_i_8_n_4\,
      I2 => \i___1_carry__0_i_8_n_6\,
      I3 => \i___1_carry_i_1_n_5\,
      I4 => \tdata_o[16]_INST_0_i_6_n_7\,
      I5 => \i___1_carry__0_i_8_n_7\,
      O => \i___1_carry__1_i_2_n_0\
    );
\i___1_carry__1_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF969600"
    )
        port map (
      I0 => \i___1_carry_i_1_n_5\,
      I1 => \tdata_o[16]_INST_0_i_6_n_7\,
      I2 => \i___1_carry__0_i_8_n_7\,
      I3 => \i___1_carry__0_i_8_n_5\,
      I4 => \i___1_carry__1_i_11_n_0\,
      O => \i___1_carry__1_i_3_n_0\
    );
\i___1_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E88E8EE8"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_6\,
      I1 => \i___1_carry__1_i_12_n_0\,
      I2 => \i___1_carry_i_1_n_4\,
      I3 => \i___1_carry_i_1_n_6\,
      I4 => \i___1_carry__0_i_8_n_4\,
      O => \i___1_carry__1_i_4_n_0\
    );
\i___1_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669969669699669"
    )
        port map (
      I0 => \i___1_carry__1_i_1_n_0\,
      I1 => \i___1_carry__0_i_9_n_0\,
      I2 => \tdata_o[16]_INST_0_i_6_n_6\,
      I3 => \i___1_carry__0_i_8_n_5\,
      I4 => \i___1_carry__0_i_8_n_7\,
      I5 => \tdata_o[16]_INST_0_i_6_n_5\,
      O => \i___1_carry__1_i_5_n_0\
    );
\i___1_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669969669699669"
    )
        port map (
      I0 => \i___1_carry__1_i_2_n_0\,
      I1 => \i___1_carry__1_i_13_n_0\,
      I2 => \tdata_o[16]_INST_0_i_6_n_5\,
      I3 => \i___1_carry__0_i_8_n_6\,
      I4 => \i___1_carry_i_1_n_4\,
      I5 => \tdata_o[16]_INST_0_i_6_n_6\,
      O => \i___1_carry__1_i_6_n_0\
    );
\i___1_carry__1_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696996969669"
    )
        port map (
      I0 => \i___1_carry__1_i_3_n_0\,
      I1 => \i___1_carry__0_i_9_n_0\,
      I2 => \i___1_carry__1_i_10_n_0\,
      I3 => \tdata_o[16]_INST_0_i_6_n_7\,
      I4 => \i___1_carry__0_i_8_n_7\,
      I5 => \i___1_carry_i_1_n_5\,
      O => \i___1_carry__1_i_7_n_0\
    );
\i___1_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \i___1_carry__1_i_4_n_0\,
      I1 => \i___1_carry__0_i_8_n_4\,
      I2 => \i___1_carry_i_1_n_6\,
      I3 => \i___1_carry_i_1_n_4\,
      I4 => \i___1_carry__1_i_13_n_0\,
      I5 => \i___1_carry_i_1_n_5\,
      O => \i___1_carry__1_i_8_n_0\
    );
\i___1_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_6\,
      I1 => \i___1_carry_i_1_n_4\,
      I2 => \i___1_carry__0_i_8_n_6\,
      O => \i___1_carry__1_i_9_n_0\
    );
\i___1_carry__2_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6606"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_5\,
      I1 => \tdata_o[16]_INST_0_i_6_n_7\,
      I2 => \tdata_o[16]_INST_0_i_6_n_6\,
      I3 => \i___1_carry__0_i_8_n_4\,
      O => \i___1_carry__2_i_1_n_0\
    );
\i___1_carry__2_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_5\,
      I1 => \tdata_o[16]_INST_0_i_6_n_7\,
      O => \i___1_carry__2_i_10_n_0\
    );
\i___1_carry__2_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6606"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_4\,
      I1 => \tdata_o[16]_INST_0_i_6_n_6\,
      I2 => \tdata_o[16]_INST_0_i_6_n_7\,
      I3 => \i___1_carry__0_i_8_n_5\,
      O => \i___1_carry__2_i_2_n_0\
    );
\i___1_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF6F6606"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_5\,
      I1 => \tdata_o[16]_INST_0_i_6_n_7\,
      I2 => \i___1_carry__0_i_8_n_4\,
      I3 => \i___1_carry__0_i_8_n_6\,
      I4 => \tdata_o[16]_INST_0_i_6_n_5\,
      O => \i___1_carry__2_i_3_n_0\
    );
\i___1_carry__2_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"28BE2828BEBE28BE"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_6\,
      I1 => \i___1_carry__0_i_8_n_6\,
      I2 => \i___1_carry__0_i_8_n_4\,
      I3 => \tdata_o[16]_INST_0_i_6_n_5\,
      I4 => \i___1_carry__0_i_8_n_7\,
      I5 => \i___1_carry__0_i_8_n_5\,
      O => \i___1_carry__2_i_4_n_0\
    );
\i___1_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3783"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_4\,
      I1 => \tdata_o[16]_INST_0_i_6_n_6\,
      I2 => \tdata_o[16]_INST_0_i_6_n_7\,
      I3 => \tdata_o[16]_INST_0_i_6_n_5\,
      O => \i___1_carry__2_i_5_n_0\
    );
\i___1_carry__2_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C36C93C3"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_5\,
      I1 => \tdata_o[16]_INST_0_i_6_n_5\,
      I2 => \tdata_o[16]_INST_0_i_6_n_7\,
      I3 => \tdata_o[16]_INST_0_i_6_n_6\,
      I4 => \i___1_carry__0_i_8_n_4\,
      O => \i___1_carry__2_i_6_n_0\
    );
\i___1_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7A851FE0E01F7A85"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_5\,
      I1 => \i___1_carry__0_i_8_n_6\,
      I2 => \i___1_carry__0_i_8_n_4\,
      I3 => \tdata_o[16]_INST_0_i_6_n_6\,
      I4 => \tdata_o[16]_INST_0_i_6_n_7\,
      I5 => \i___1_carry__0_i_8_n_5\,
      O => \i___1_carry__2_i_7_n_0\
    );
\i___1_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"71E78E188E1871E7"
    )
        port map (
      I0 => \i___1_carry__2_i_9_n_0\,
      I1 => \tdata_o[16]_INST_0_i_6_n_6\,
      I2 => \i___1_carry__0_i_8_n_4\,
      I3 => \i___1_carry__0_i_8_n_6\,
      I4 => \tdata_o[16]_INST_0_i_6_n_5\,
      I5 => \i___1_carry__2_i_10_n_0\,
      O => \i___1_carry__2_i_8_n_0\
    );
\i___1_carry__2_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_5\,
      I1 => \i___1_carry__0_i_8_n_7\,
      I2 => \i___1_carry__0_i_8_n_5\,
      O => \i___1_carry__2_i_9_n_0\
    );
\i___1_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"83"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_7\,
      I1 => \tdata_o[16]_INST_0_i_6_n_5\,
      I2 => \tdata_o[16]_INST_0_i_6_n_6\,
      O => \i___1_carry__3_i_1_n_0\
    );
\i___1_carry_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \i___1_carry_i_1_n_0\,
      CO(2) => \i___1_carry_i_1_n_1\,
      CO(1) => \i___1_carry_i_1_n_2\,
      CO(0) => \i___1_carry_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \width_s_reg_n_0_[3]\,
      DI(2) => \width_s_reg_n_0_[2]\,
      DI(1) => \width_s_reg_n_0_[1]\,
      DI(0) => \width_s_reg_n_0_[0]\,
      O(3) => \i___1_carry_i_1_n_4\,
      O(2) => \i___1_carry_i_1_n_5\,
      O(1) => \i___1_carry_i_1_n_6\,
      O(0) => \i___1_carry_i_1_n_7\,
      S(3) => \i___1_carry_i_5_n_0\,
      S(2) => \i___1_carry_i_6_n_0\,
      S(1) => \i___1_carry_i_7_n_0\,
      S(0) => \i___1_carry_i_8_n_0\
    );
\i___1_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i___1_carry_i_1_n_6\,
      I1 => \i___1_carry_i_1_n_4\,
      O => \i___1_carry_i_2_n_0\
    );
\i___1_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i___1_carry_i_1_n_7\,
      I1 => \i___1_carry_i_1_n_5\,
      O => \i___1_carry_i_3_n_0\
    );
\i___1_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i___1_carry_i_1_n_6\,
      O => \i___1_carry_i_4_n_0\
    );
\i___1_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[3]\,
      I1 => fr_cnt_s(3),
      O => \i___1_carry_i_5_n_0\
    );
\i___1_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[2]\,
      I1 => fr_cnt_s(2),
      O => \i___1_carry_i_6_n_0\
    );
\i___1_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[1]\,
      I1 => fr_cnt_s(1),
      O => \i___1_carry_i_7_n_0\
    );
\i___1_carry_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \width_s_reg_n_0_[0]\,
      I1 => fr_cnt_s(0),
      O => \i___1_carry_i_8_n_0\
    );
\i___37_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___1_carry__3_n_7\,
      I1 => \tdata_o2_inferred__0/i___1_carry__2_n_5\,
      O => \i___37_carry__0_i_1_n_0\
    );
\i___37_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___1_carry__2_n_4\,
      I1 => \tdata_o2_inferred__0/i___1_carry__2_n_6\,
      O => \i___37_carry_i_1_n_0\
    );
\i___37_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___1_carry__2_n_5\,
      I1 => \tdata_o2_inferred__0/i___1_carry__2_n_7\,
      O => \i___37_carry_i_2_n_0\
    );
\i___37_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___1_carry__2_n_6\,
      I1 => \tdata_o2_inferred__0/i___1_carry__1_n_4\,
      O => \i___37_carry_i_3_n_0\
    );
\i___55_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___37_carry__0_n_6\,
      I1 => \tdata_o[16]_INST_0_i_6_n_6\,
      O => \i___55_carry__0_i_1_n_0\
    );
\i___55_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___37_carry__0_n_7\,
      I1 => \tdata_o[16]_INST_0_i_6_n_7\,
      O => \i___55_carry__0_i_2_n_0\
    );
\i___55_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___37_carry_n_4\,
      I1 => \i___1_carry__0_i_8_n_4\,
      O => \i___55_carry__0_i_3_n_0\
    );
\i___55_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___37_carry_n_5\,
      I1 => \i___1_carry__0_i_8_n_5\,
      O => \i___55_carry__0_i_4_n_0\
    );
\i___55_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_6\,
      I1 => \tdata_o2_inferred__0/i___37_carry__0_n_6\,
      I2 => \tdata_o[16]_INST_0_i_6_n_5\,
      I3 => \tdata_o2_inferred__0/i___37_carry__0_n_5\,
      O => \i___55_carry__0_i_5_n_0\
    );
\i___55_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_7\,
      I1 => \tdata_o2_inferred__0/i___37_carry__0_n_7\,
      I2 => \tdata_o[16]_INST_0_i_6_n_6\,
      I3 => \tdata_o2_inferred__0/i___37_carry__0_n_6\,
      O => \i___55_carry__0_i_6_n_0\
    );
\i___55_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_4\,
      I1 => \tdata_o2_inferred__0/i___37_carry_n_4\,
      I2 => \tdata_o[16]_INST_0_i_6_n_7\,
      I3 => \tdata_o2_inferred__0/i___37_carry__0_n_7\,
      O => \i___55_carry__0_i_7_n_0\
    );
\i___55_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_5\,
      I1 => \tdata_o2_inferred__0/i___37_carry_n_5\,
      I2 => \i___1_carry__0_i_8_n_4\,
      I3 => \tdata_o2_inferred__0/i___37_carry_n_4\,
      O => \i___55_carry__0_i_8_n_0\
    );
\i___55_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___37_carry_n_6\,
      I1 => \i___1_carry__0_i_8_n_6\,
      O => \i___55_carry_i_1_n_0\
    );
\i___55_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___37_carry_n_7\,
      I1 => \i___1_carry__0_i_8_n_7\,
      O => \i___55_carry_i_2_n_0\
    );
\i___55_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___1_carry__1_n_4\,
      I1 => \i___1_carry_i_1_n_4\,
      O => \i___55_carry_i_3_n_0\
    );
\i___55_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_6\,
      I1 => \tdata_o2_inferred__0/i___37_carry_n_6\,
      I2 => \i___1_carry__0_i_8_n_5\,
      I3 => \tdata_o2_inferred__0/i___37_carry_n_5\,
      O => \i___55_carry_i_4_n_0\
    );
\i___55_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \i___1_carry__0_i_8_n_7\,
      I1 => \tdata_o2_inferred__0/i___37_carry_n_7\,
      I2 => \tdata_o2_inferred__0/i___37_carry_n_6\,
      I3 => \i___1_carry__0_i_8_n_6\,
      O => \i___55_carry_i_5_n_0\
    );
\i___55_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \i___1_carry_i_1_n_4\,
      I1 => \tdata_o2_inferred__0/i___1_carry__1_n_4\,
      I2 => \i___1_carry__0_i_8_n_7\,
      I3 => \tdata_o2_inferred__0/i___37_carry_n_7\,
      O => \i___55_carry_i_6_n_0\
    );
\i___55_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___1_carry_i_1_n_4\,
      I1 => \tdata_o2_inferred__0/i___1_carry__1_n_4\,
      O => \i___55_carry_i_7_n_0\
    );
\tdata_o2__0_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tdata_o2__0_carry_n_0\,
      CO(2) => \tdata_o2__0_carry_n_1\,
      CO(1) => \tdata_o2__0_carry_n_2\,
      CO(0) => \tdata_o2__0_carry_n_3\,
      CYINIT => '0',
      DI(3) => \tdata_o2__0_carry_i_1_n_0\,
      DI(2) => \tdata_o2__0_carry_i_2_n_0\,
      DI(1) => \tdata_o2__0_carry_i_3_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_tdata_o2__0_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \tdata_o2__0_carry_i_4_n_0\,
      S(2) => \tdata_o2__0_carry_i_5_n_0\,
      S(1) => \tdata_o2__0_carry_i_6_n_0\,
      S(0) => \tdata_o2__0_carry_i_7_n_0\
    );
\tdata_o2__0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2__0_carry_n_0\,
      CO(3) => \tdata_o2__0_carry__0_n_0\,
      CO(2) => \tdata_o2__0_carry__0_n_1\,
      CO(1) => \tdata_o2__0_carry__0_n_2\,
      CO(0) => \tdata_o2__0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \tdata_o2__0_carry__0_i_1_n_0\,
      DI(2) => \tdata_o2__0_carry__0_i_2_n_0\,
      DI(1) => \tdata_o2__0_carry__0_i_3_n_0\,
      DI(0) => \tdata_o2__0_carry__0_i_4_n_0\,
      O(3) => \tdata_o2__0_carry__0_n_4\,
      O(2 downto 0) => \NLW_tdata_o2__0_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \tdata_o2__0_carry__0_i_5_n_0\,
      S(2) => \tdata_o2__0_carry__0_i_6_n_0\,
      S(1) => \tdata_o2__0_carry__0_i_7_n_0\,
      S(0) => \tdata_o2__0_carry__0_i_8_n_0\
    );
\tdata_o2__0_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => tdata_o3(4),
      I1 => tdata_o3(6),
      I2 => tdata_o3(9),
      O => \tdata_o2__0_carry__0_i_1_n_0\
    );
\tdata_o2__0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => tdata_o3(3),
      I1 => tdata_o3(5),
      I2 => tdata_o3(8),
      O => \tdata_o2__0_carry__0_i_2_n_0\
    );
\tdata_o2__0_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => tdata_o3(2),
      I1 => tdata_o3(4),
      I2 => tdata_o3(7),
      O => \tdata_o2__0_carry__0_i_3_n_0\
    );
\tdata_o2__0_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => tdata_o3(1),
      I1 => tdata_o3(3),
      I2 => tdata_o3(6),
      O => \tdata_o2__0_carry__0_i_4_n_0\
    );
\tdata_o2__0_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => tdata_o3(9),
      I1 => tdata_o3(6),
      I2 => tdata_o3(4),
      I3 => tdata_o3(7),
      I4 => tdata_o3(5),
      O => \tdata_o2__0_carry__0_i_5_n_0\
    );
\tdata_o2__0_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => tdata_o3(8),
      I1 => tdata_o3(5),
      I2 => tdata_o3(3),
      I3 => tdata_o3(4),
      I4 => tdata_o3(6),
      I5 => tdata_o3(9),
      O => \tdata_o2__0_carry__0_i_6_n_0\
    );
\tdata_o2__0_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => tdata_o3(7),
      I1 => tdata_o3(4),
      I2 => tdata_o3(2),
      I3 => tdata_o3(3),
      I4 => tdata_o3(5),
      I5 => tdata_o3(8),
      O => \tdata_o2__0_carry__0_i_7_n_0\
    );
\tdata_o2__0_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => tdata_o3(6),
      I1 => tdata_o3(3),
      I2 => tdata_o3(1),
      I3 => tdata_o3(2),
      I4 => tdata_o3(4),
      I5 => tdata_o3(7),
      O => \tdata_o2__0_carry__0_i_8_n_0\
    );
\tdata_o2__0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2__0_carry__0_n_0\,
      CO(3) => \NLW_tdata_o2__0_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \tdata_o2__0_carry__1_n_1\,
      CO(1) => \tdata_o2__0_carry__1_n_2\,
      CO(0) => \tdata_o2__0_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \tdata_o2__0_carry__1_i_1_n_0\,
      DI(1) => \tdata_o2__0_carry__1_i_2_n_0\,
      DI(0) => \tdata_o2__0_carry__1_i_3_n_0\,
      O(3) => \tdata_o2__0_carry__1_n_4\,
      O(2) => \tdata_o2__0_carry__1_n_5\,
      O(1) => \tdata_o2__0_carry__1_n_6\,
      O(0) => \tdata_o2__0_carry__1_n_7\,
      S(3) => \tdata_o2__0_carry__1_i_4_n_0\,
      S(2) => \tdata_o2__0_carry__1_i_5_n_0\,
      S(1) => \tdata_o2__0_carry__1_i_6_n_0\,
      S(0) => \tdata_o2__0_carry__1_i_7_n_0\
    );
\tdata_o2__0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tdata_o3(7),
      I1 => tdata_o3(9),
      O => \tdata_o2__0_carry__1_i_1_n_0\
    );
\tdata_o2__0_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tdata_o3(6),
      I1 => tdata_o3(8),
      O => \tdata_o2__0_carry__1_i_2_n_0\
    );
\tdata_o2__0_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tdata_o3(5),
      I1 => tdata_o3(7),
      O => \tdata_o2__0_carry__1_i_3_n_0\
    );
\tdata_o2__0_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => tdata_o3(8),
      I1 => tdata_o3(9),
      O => \tdata_o2__0_carry__1_i_4_n_0\
    );
\tdata_o2__0_carry__1_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4B"
    )
        port map (
      I0 => tdata_o3(9),
      I1 => tdata_o3(7),
      I2 => tdata_o3(8),
      O => \tdata_o2__0_carry__1_i_5_n_0\
    );
\tdata_o2__0_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => tdata_o3(8),
      I1 => tdata_o3(6),
      I2 => tdata_o3(9),
      I3 => tdata_o3(7),
      O => \tdata_o2__0_carry__1_i_6_n_0\
    );
\tdata_o2__0_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => tdata_o3(7),
      I1 => tdata_o3(5),
      I2 => tdata_o3(8),
      I3 => tdata_o3(6),
      O => \tdata_o2__0_carry__1_i_7_n_0\
    );
\tdata_o2__0_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => tdata_o3(0),
      I1 => tdata_o3(2),
      I2 => tdata_o3(5),
      O => \tdata_o2__0_carry_i_1_n_0\
    );
\tdata_o2__0_carry_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(3),
      I1 => fr_cnt_s(3),
      O => \tdata_o2__0_carry_i_10_n_0\
    );
\tdata_o2__0_carry_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(2),
      I1 => fr_cnt_s(2),
      O => \tdata_o2__0_carry_i_11_n_0\
    );
\tdata_o2__0_carry_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(1),
      I1 => fr_cnt_s(1),
      O => \tdata_o2__0_carry_i_12_n_0\
    );
\tdata_o2__0_carry_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(0),
      I1 => fr_cnt_s(0),
      O => \tdata_o2__0_carry_i_13_n_0\
    );
\tdata_o2__0_carry_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(7),
      I1 => fr_cnt_s(7),
      O => \tdata_o2__0_carry_i_14_n_0\
    );
\tdata_o2__0_carry_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(6),
      I1 => fr_cnt_s(6),
      O => \tdata_o2__0_carry_i_15_n_0\
    );
\tdata_o2__0_carry_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(5),
      I1 => fr_cnt_s(5),
      O => \tdata_o2__0_carry_i_16_n_0\
    );
\tdata_o2__0_carry_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => height_s(4),
      I1 => fr_cnt_s(4),
      O => \tdata_o2__0_carry_i_17_n_0\
    );
\tdata_o2__0_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => tdata_o3(5),
      I1 => tdata_o3(2),
      I2 => tdata_o3(0),
      O => \tdata_o2__0_carry_i_2_n_0\
    );
\tdata_o2__0_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => tdata_o3(3),
      I1 => tdata_o3(0),
      O => \tdata_o2__0_carry_i_3_n_0\
    );
\tdata_o2__0_carry_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => tdata_o3(5),
      I1 => tdata_o3(2),
      I2 => tdata_o3(0),
      I3 => tdata_o3(1),
      I4 => tdata_o3(3),
      I5 => tdata_o3(6),
      O => \tdata_o2__0_carry_i_4_n_0\
    );
\tdata_o2__0_carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => tdata_o3(0),
      I1 => tdata_o3(2),
      I2 => tdata_o3(5),
      I3 => tdata_o3(1),
      I4 => tdata_o3(4),
      O => \tdata_o2__0_carry_i_5_n_0\
    );
\tdata_o2__0_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => tdata_o3(0),
      I1 => tdata_o3(3),
      I2 => tdata_o3(4),
      I3 => tdata_o3(1),
      O => \tdata_o2__0_carry_i_6_n_0\
    );
\tdata_o2__0_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => tdata_o3(3),
      I1 => tdata_o3(0),
      O => \tdata_o2__0_carry_i_7_n_0\
    );
\tdata_o2__0_carry_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tdata_o2__0_carry_i_8_n_0\,
      CO(2) => \tdata_o2__0_carry_i_8_n_1\,
      CO(1) => \tdata_o2__0_carry_i_8_n_2\,
      CO(0) => \tdata_o2__0_carry_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => height_s(3 downto 0),
      O(3 downto 0) => tdata_o3(3 downto 0),
      S(3) => \tdata_o2__0_carry_i_10_n_0\,
      S(2) => \tdata_o2__0_carry_i_11_n_0\,
      S(1) => \tdata_o2__0_carry_i_12_n_0\,
      S(0) => \tdata_o2__0_carry_i_13_n_0\
    );
\tdata_o2__0_carry_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2__0_carry_i_8_n_0\,
      CO(3) => \tdata_o2__0_carry_i_9_n_0\,
      CO(2) => \tdata_o2__0_carry_i_9_n_1\,
      CO(1) => \tdata_o2__0_carry_i_9_n_2\,
      CO(0) => \tdata_o2__0_carry_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => height_s(7 downto 4),
      O(3 downto 0) => tdata_o3(7 downto 4),
      S(3) => \tdata_o2__0_carry_i_14_n_0\,
      S(2) => \tdata_o2__0_carry_i_15_n_0\,
      S(1) => \tdata_o2__0_carry_i_16_n_0\,
      S(0) => \tdata_o2__0_carry_i_17_n_0\
    );
\tdata_o2__27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tdata_o2__27_carry_n_0\,
      CO(2) => \tdata_o2__27_carry_n_1\,
      CO(1) => \tdata_o2__27_carry_n_2\,
      CO(0) => \tdata_o2__27_carry_n_3\,
      CYINIT => '0',
      DI(3) => \tdata_o2__27_carry_i_1_n_0\,
      DI(2) => \tdata_o2__27_carry_i_2_n_0\,
      DI(1) => \tdata_o2__27_carry_i_3_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_tdata_o2__27_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \tdata_o2__27_carry_i_4_n_0\,
      S(2) => \tdata_o2__27_carry_i_5_n_0\,
      S(1) => \tdata_o2__27_carry_i_6_n_0\,
      S(0) => \tdata_o2__27_carry_i_7_n_0\
    );
\tdata_o2__27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2__27_carry_n_0\,
      CO(3) => \NLW_tdata_o2__27_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \tdata_o2__27_carry__0_n_1\,
      CO(1) => \tdata_o2__27_carry__0_n_2\,
      CO(0) => \tdata_o2__27_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \tdata_o2__27_carry__0_i_1_n_0\,
      DI(1) => \tdata_o2__27_carry__0_i_2_n_0\,
      DI(0) => \tdata_o2__27_carry__0_i_3_n_0\,
      O(3 downto 0) => \NLW_tdata_o2__27_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \tdata_o2__27_carry__0_i_4_n_0\,
      S(1) => \tdata_o2__27_carry__0_i_5_n_0\,
      S(0) => \tdata_o2__27_carry__0_i_6_n_0\
    );
\tdata_o2__27_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000015FF800"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_7\,
      I1 => \tdata_o2__0_carry__0_n_4\,
      I2 => \tdata_o2__0_carry__1_n_4\,
      I3 => \tdata_o2__0_carry__1_n_6\,
      I4 => \tdata_o2__0_carry__1_n_5\,
      I5 => tdata_o3(8),
      O => \tdata_o2__27_carry__0_i_1_n_0\
    );
\tdata_o2__27_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000E7871878"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_7\,
      I1 => \tdata_o2__0_carry__1_n_5\,
      I2 => \tdata_o2__0_carry__1_n_6\,
      I3 => \tdata_o2__0_carry__0_n_4\,
      I4 => \tdata_o2__0_carry__1_n_4\,
      I5 => tdata_o3(7),
      O => \tdata_o2__27_carry__0_i_2_n_0\
    );
\tdata_o2__27_carry__0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00009666"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_5\,
      I1 => \tdata_o2__0_carry__1_n_7\,
      I2 => \tdata_o2__0_carry__0_n_4\,
      I3 => \tdata_o2__0_carry__1_n_6\,
      I4 => tdata_o3(6),
      O => \tdata_o2__27_carry__0_i_3_n_0\
    );
\tdata_o2__27_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tdata_o2__27_carry__0_i_1_n_0\,
      I1 => \tdata_o2__27_carry__0_i_7_n_0\,
      O => \tdata_o2__27_carry__0_i_4_n_0\
    );
\tdata_o2__27_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tdata_o2__27_carry__0_i_2_n_0\,
      I1 => \tdata_o2__27_carry__0_i_8_n_0\,
      O => \tdata_o2__27_carry__0_i_5_n_0\
    );
\tdata_o2__27_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFEAEABF40151540"
    )
        port map (
      I0 => tdata_o3(6),
      I1 => \tdata_o2__0_carry__1_n_6\,
      I2 => \tdata_o2__0_carry__0_n_4\,
      I3 => \tdata_o2__0_carry__1_n_7\,
      I4 => \tdata_o2__0_carry__1_n_5\,
      I5 => \tdata_o2__27_carry__0_i_9_n_0\,
      O => \tdata_o2__27_carry__0_i_6_n_0\
    );
\tdata_o2__27_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"11C8FF00EE3700FF"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_7\,
      I1 => \tdata_o2__0_carry__1_n_6\,
      I2 => \tdata_o2__0_carry__0_n_4\,
      I3 => \tdata_o2__0_carry__1_n_4\,
      I4 => \tdata_o2__0_carry__1_n_5\,
      I5 => tdata_o3(9),
      O => \tdata_o2__27_carry__0_i_7_n_0\
    );
\tdata_o2__27_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6569655969596999"
    )
        port map (
      I0 => tdata_o3(8),
      I1 => \tdata_o2__0_carry__1_n_5\,
      I2 => \tdata_o2__0_carry__1_n_6\,
      I3 => \tdata_o2__0_carry__1_n_4\,
      I4 => \tdata_o2__0_carry__0_n_4\,
      I5 => \tdata_o2__0_carry__1_n_7\,
      O => \tdata_o2__27_carry__0_i_8_n_0\
    );
\tdata_o2__27_carry__0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996699996666999"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_4\,
      I1 => tdata_o3(7),
      I2 => \tdata_o2__0_carry__1_n_5\,
      I3 => \tdata_o2__0_carry__1_n_7\,
      I4 => \tdata_o2__0_carry__1_n_6\,
      I5 => \tdata_o2__0_carry__0_n_4\,
      O => \tdata_o2__27_carry__0_i_9_n_0\
    );
\tdata_o2__27_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6F"
    )
        port map (
      I0 => \tdata_o2__0_carry__0_n_4\,
      I1 => \tdata_o2__0_carry__1_n_6\,
      I2 => tdata_o3(5),
      O => \tdata_o2__27_carry_i_1_n_0\
    );
\tdata_o2__27_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_7\,
      I1 => tdata_o3(4),
      O => \tdata_o2__27_carry_i_2_n_0\
    );
\tdata_o2__27_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \tdata_o2__0_carry__0_n_4\,
      I1 => tdata_o3(3),
      O => \tdata_o2__27_carry_i_3_n_0\
    );
\tdata_o2__27_carry_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"96C3C369693C3C96"
    )
        port map (
      I0 => tdata_o3(5),
      I1 => \tdata_o2__0_carry__1_n_5\,
      I2 => \tdata_o2__0_carry__1_n_7\,
      I3 => \tdata_o2__0_carry__0_n_4\,
      I4 => \tdata_o2__0_carry__1_n_6\,
      I5 => tdata_o3(6),
      O => \tdata_o2__27_carry_i_4_n_0\
    );
\tdata_o2__27_carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B44B4BB4"
    )
        port map (
      I0 => tdata_o3(4),
      I1 => \tdata_o2__0_carry__1_n_7\,
      I2 => tdata_o3(5),
      I3 => \tdata_o2__0_carry__0_n_4\,
      I4 => \tdata_o2__0_carry__1_n_6\,
      O => \tdata_o2__27_carry_i_5_n_0\
    );
\tdata_o2__27_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => tdata_o3(3),
      I1 => \tdata_o2__0_carry__0_n_4\,
      I2 => tdata_o3(4),
      I3 => \tdata_o2__0_carry__1_n_7\,
      O => \tdata_o2__27_carry_i_6_n_0\
    );
\tdata_o2__27_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => tdata_o3(3),
      I1 => \tdata_o2__0_carry__0_n_4\,
      O => \tdata_o2__27_carry_i_7_n_0\
    );
\tdata_o2_inferred__0/i___1_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tdata_o2_inferred__0/i___1_carry_n_0\,
      CO(2) => \tdata_o2_inferred__0/i___1_carry_n_1\,
      CO(1) => \tdata_o2_inferred__0/i___1_carry_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___1_carry_n_3\,
      CYINIT => '0',
      DI(3) => \i___1_carry_i_1_n_6\,
      DI(2) => \i___1_carry_i_1_n_7\,
      DI(1 downto 0) => B"01",
      O(3 downto 0) => \NLW_tdata_o2_inferred__0/i___1_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___1_carry_i_2_n_0\,
      S(2) => \i___1_carry_i_3_n_0\,
      S(1) => \i___1_carry_i_4_n_0\,
      S(0) => \i___1_carry_i_1_n_7\
    );
\tdata_o2_inferred__0/i___1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2_inferred__0/i___1_carry_n_0\,
      CO(3) => \tdata_o2_inferred__0/i___1_carry__0_n_0\,
      CO(2) => \tdata_o2_inferred__0/i___1_carry__0_n_1\,
      CO(1) => \tdata_o2_inferred__0/i___1_carry__0_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___1_carry__0_i_1_n_0\,
      DI(2) => \i___1_carry__0_i_2_n_0\,
      DI(1) => \i___1_carry__0_i_3_n_0\,
      DI(0) => \i___1_carry_i_1_n_5\,
      O(3 downto 0) => \NLW_tdata_o2_inferred__0/i___1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___1_carry__0_i_4_n_0\,
      S(2) => \i___1_carry__0_i_5_n_0\,
      S(1) => \i___1_carry__0_i_6_n_0\,
      S(0) => \i___1_carry__0_i_7_n_0\
    );
\tdata_o2_inferred__0/i___1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2_inferred__0/i___1_carry__0_n_0\,
      CO(3) => \tdata_o2_inferred__0/i___1_carry__1_n_0\,
      CO(2) => \tdata_o2_inferred__0/i___1_carry__1_n_1\,
      CO(1) => \tdata_o2_inferred__0/i___1_carry__1_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___1_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \i___1_carry__1_i_1_n_0\,
      DI(2) => \i___1_carry__1_i_2_n_0\,
      DI(1) => \i___1_carry__1_i_3_n_0\,
      DI(0) => \i___1_carry__1_i_4_n_0\,
      O(3) => \tdata_o2_inferred__0/i___1_carry__1_n_4\,
      O(2 downto 0) => \NLW_tdata_o2_inferred__0/i___1_carry__1_O_UNCONNECTED\(2 downto 0),
      S(3) => \i___1_carry__1_i_5_n_0\,
      S(2) => \i___1_carry__1_i_6_n_0\,
      S(1) => \i___1_carry__1_i_7_n_0\,
      S(0) => \i___1_carry__1_i_8_n_0\
    );
\tdata_o2_inferred__0/i___1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2_inferred__0/i___1_carry__1_n_0\,
      CO(3) => \tdata_o2_inferred__0/i___1_carry__2_n_0\,
      CO(2) => \tdata_o2_inferred__0/i___1_carry__2_n_1\,
      CO(1) => \tdata_o2_inferred__0/i___1_carry__2_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___1_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \i___1_carry__2_i_1_n_0\,
      DI(2) => \i___1_carry__2_i_2_n_0\,
      DI(1) => \i___1_carry__2_i_3_n_0\,
      DI(0) => \i___1_carry__2_i_4_n_0\,
      O(3) => \tdata_o2_inferred__0/i___1_carry__2_n_4\,
      O(2) => \tdata_o2_inferred__0/i___1_carry__2_n_5\,
      O(1) => \tdata_o2_inferred__0/i___1_carry__2_n_6\,
      O(0) => \tdata_o2_inferred__0/i___1_carry__2_n_7\,
      S(3) => \i___1_carry__2_i_5_n_0\,
      S(2) => \i___1_carry__2_i_6_n_0\,
      S(1) => \i___1_carry__2_i_7_n_0\,
      S(0) => \i___1_carry__2_i_8_n_0\
    );
\tdata_o2_inferred__0/i___1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2_inferred__0/i___1_carry__2_n_0\,
      CO(3 downto 0) => \NLW_tdata_o2_inferred__0/i___1_carry__3_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_tdata_o2_inferred__0/i___1_carry__3_O_UNCONNECTED\(3 downto 1),
      O(0) => \tdata_o2_inferred__0/i___1_carry__3_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \i___1_carry__3_i_1_n_0\
    );
\tdata_o2_inferred__0/i___37_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tdata_o2_inferred__0/i___37_carry_n_0\,
      CO(2) => \tdata_o2_inferred__0/i___37_carry_n_1\,
      CO(1) => \tdata_o2_inferred__0/i___37_carry_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___37_carry_n_3\,
      CYINIT => '0',
      DI(3) => \tdata_o2_inferred__0/i___1_carry__2_n_4\,
      DI(2) => \tdata_o2_inferred__0/i___1_carry__2_n_5\,
      DI(1) => \tdata_o2_inferred__0/i___1_carry__2_n_6\,
      DI(0) => '0',
      O(3) => \tdata_o2_inferred__0/i___37_carry_n_4\,
      O(2) => \tdata_o2_inferred__0/i___37_carry_n_5\,
      O(1) => \tdata_o2_inferred__0/i___37_carry_n_6\,
      O(0) => \tdata_o2_inferred__0/i___37_carry_n_7\,
      S(3) => \i___37_carry_i_1_n_0\,
      S(2) => \i___37_carry_i_2_n_0\,
      S(1) => \i___37_carry_i_3_n_0\,
      S(0) => \tdata_o2_inferred__0/i___1_carry__2_n_7\
    );
\tdata_o2_inferred__0/i___37_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2_inferred__0/i___37_carry_n_0\,
      CO(3 downto 2) => \NLW_tdata_o2_inferred__0/i___37_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \tdata_o2_inferred__0/i___37_carry__0_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___37_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \tdata_o2_inferred__0/i___1_carry__3_n_7\,
      O(3) => \NLW_tdata_o2_inferred__0/i___37_carry__0_O_UNCONNECTED\(3),
      O(2) => \tdata_o2_inferred__0/i___37_carry__0_n_5\,
      O(1) => \tdata_o2_inferred__0/i___37_carry__0_n_6\,
      O(0) => \tdata_o2_inferred__0/i___37_carry__0_n_7\,
      S(3) => '0',
      S(2) => \tdata_o2_inferred__0/i___1_carry__3_n_7\,
      S(1) => \tdata_o2_inferred__0/i___1_carry__2_n_4\,
      S(0) => \i___37_carry__0_i_1_n_0\
    );
\tdata_o2_inferred__0/i___55_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tdata_o2_inferred__0/i___55_carry_n_0\,
      CO(2) => \tdata_o2_inferred__0/i___55_carry_n_1\,
      CO(1) => \tdata_o2_inferred__0/i___55_carry_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___55_carry_n_3\,
      CYINIT => '0',
      DI(3) => \i___55_carry_i_1_n_0\,
      DI(2) => \i___55_carry_i_2_n_0\,
      DI(1) => \i___55_carry_i_3_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_tdata_o2_inferred__0/i___55_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___55_carry_i_4_n_0\,
      S(2) => \i___55_carry_i_5_n_0\,
      S(1) => \i___55_carry_i_6_n_0\,
      S(0) => \i___55_carry_i_7_n_0\
    );
\tdata_o2_inferred__0/i___55_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2_inferred__0/i___55_carry_n_0\,
      CO(3) => \tdata_o2_inferred__0/i___55_carry__0_n_0\,
      CO(2) => \tdata_o2_inferred__0/i___55_carry__0_n_1\,
      CO(1) => \tdata_o2_inferred__0/i___55_carry__0_n_2\,
      CO(0) => \tdata_o2_inferred__0/i___55_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___55_carry__0_i_1_n_0\,
      DI(2) => \i___55_carry__0_i_2_n_0\,
      DI(1) => \i___55_carry__0_i_3_n_0\,
      DI(0) => \i___55_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_tdata_o2_inferred__0/i___55_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___55_carry__0_i_5_n_0\,
      S(2) => \i___55_carry__0_i_6_n_0\,
      S(1) => \i___55_carry__0_i_7_n_0\,
      S(0) => \i___55_carry__0_i_8_n_0\
    );
\tdata_o[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4440"
    )
        port map (
      I0 => \tdata_o[0]_INST_0_i_1_n_0\,
      I1 => rst_i,
      I2 => \tdata_o[0]_INST_0_i_2_n_0\,
      I3 => \tdata_o[16]_INST_0_i_3_n_0\,
      O => tdata_o(0)
    );
\tdata_o[0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0BF4"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_6_n_5\,
      I1 => \tdata_o2_inferred__0/i___37_carry__0_n_5\,
      I2 => \tdata_o2_inferred__0/i___55_carry__0_n_0\,
      I3 => \tdata_o2_inferred__0/i___1_carry__1_n_4\,
      O => \tdata_o[0]_INST_0_i_1_n_0\
    );
\tdata_o[0]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F00000101FF00"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_6\,
      I1 => \tdata_o[0]_INST_0_i_3_n_0\,
      I2 => \tdata_o2__27_carry__0_n_1\,
      I3 => \tdata_o[0]_INST_0_i_4_n_0\,
      I4 => \tdata_o2__0_carry__0_n_4\,
      I5 => tdata_o3(9),
      O => \tdata_o[0]_INST_0_i_2_n_0\
    );
\tdata_o[0]_INST_0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_5\,
      I1 => \tdata_o2__0_carry__1_n_7\,
      O => \tdata_o[0]_INST_0_i_3_n_0\
    );
\tdata_o[0]_INST_0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"42AA"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_4\,
      I1 => \tdata_o2__0_carry__1_n_6\,
      I2 => \tdata_o2__0_carry__1_n_7\,
      I3 => \tdata_o2__0_carry__1_n_5\,
      O => \tdata_o[0]_INST_0_i_4_n_0\
    );
\tdata_o[16]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF0800000000"
    )
        port map (
      I0 => \tdata_o[16]_INST_0_i_1_n_0\,
      I1 => \tdata_o2__0_carry__0_n_4\,
      I2 => \tdata_o2__27_carry__0_n_1\,
      I3 => \tdata_o[16]_INST_0_i_2_n_0\,
      I4 => \tdata_o[16]_INST_0_i_3_n_0\,
      I5 => \tdata_o[16]_INST_0_i_4_n_0\,
      O => tdata_o(2)
    );
\tdata_o[16]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF40"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_6\,
      I1 => \tdata_o2__0_carry__1_n_5\,
      I2 => \tdata_o2__0_carry__1_n_7\,
      I3 => tdata_o3(9),
      O => \tdata_o[16]_INST_0_i_1_n_0\
    );
\tdata_o[16]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0101011110000000"
    )
        port map (
      I0 => tdata_o3(9),
      I1 => \tdata_o2__0_carry__0_n_4\,
      I2 => \tdata_o2__0_carry__1_n_5\,
      I3 => \tdata_o2__0_carry__1_n_7\,
      I4 => \tdata_o2__0_carry__1_n_6\,
      I5 => \tdata_o2__0_carry__1_n_4\,
      O => \tdata_o[16]_INST_0_i_2_n_0\
    );
\tdata_o[16]_INST_0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40AA15AA"
    )
        port map (
      I0 => \tdata_o2__27_carry__0_n_1\,
      I1 => \tdata_o2__0_carry__1_n_5\,
      I2 => \tdata_o2__0_carry__1_n_6\,
      I3 => \tdata_o2__0_carry__0_n_4\,
      I4 => \tdata_o2__0_carry__1_n_4\,
      O => \tdata_o[16]_INST_0_i_3_n_0\
    );
\tdata_o[16]_INST_0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66560000"
    )
        port map (
      I0 => \tdata_o2_inferred__0/i___1_carry__1_n_4\,
      I1 => \tdata_o2_inferred__0/i___55_carry__0_n_0\,
      I2 => \tdata_o2_inferred__0/i___37_carry__0_n_5\,
      I3 => \tdata_o[16]_INST_0_i_6_n_5\,
      I4 => rst_i,
      O => \tdata_o[16]_INST_0_i_4_n_0\
    );
\tdata_o[16]_INST_0_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \tdata_o2__0_carry_i_9_n_0\,
      CO(3 downto 1) => \NLW_tdata_o[16]_INST_0_i_5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \tdata_o[16]_INST_0_i_5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_tdata_o[16]_INST_0_i_5_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => tdata_o3(9 downto 8),
      S(3 downto 2) => B"00",
      S(1 downto 0) => height_s(9 downto 8)
    );
\tdata_o[16]_INST_0_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \i___1_carry__0_i_8_n_0\,
      CO(3 downto 2) => \NLW_tdata_o[16]_INST_0_i_6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \tdata_o[16]_INST_0_i_6_n_2\,
      CO(0) => \tdata_o[16]_INST_0_i_6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_tdata_o[16]_INST_0_i_6_O_UNCONNECTED\(3),
      O(2) => \tdata_o[16]_INST_0_i_6_n_5\,
      O(1) => \tdata_o[16]_INST_0_i_6_n_6\,
      O(0) => \tdata_o[16]_INST_0_i_6_n_7\,
      S(3) => '0',
      S(2) => \width_s_reg_n_0_[10]\,
      S(1) => \width_s_reg_n_0_[9]\,
      S(0) => \width_s_reg_n_0_[8]\
    );
\tdata_o[8]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00AA00AE00AE00"
    )
        port map (
      I0 => \tdata_o[8]_INST_0_i_1_n_0\,
      I1 => \tdata_o[8]_INST_0_i_2_n_0\,
      I2 => \tdata_o2__27_carry__0_n_1\,
      I3 => \tdata_o[16]_INST_0_i_4_n_0\,
      I4 => \tdata_o[8]_INST_0_i_3_n_0\,
      I5 => \tdata_o2__0_carry__0_n_4\,
      O => tdata_o(1)
    );
\tdata_o[8]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04000000"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_4\,
      I1 => \tdata_o2__0_carry__1_n_5\,
      I2 => tdata_o3(9),
      I3 => \tdata_o2__0_carry__0_n_4\,
      I4 => \tdata_o2__0_carry__1_n_6\,
      O => \tdata_o[8]_INST_0_i_1_n_0\
    );
\tdata_o[8]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC8FF7F"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_6\,
      I1 => \tdata_o2__0_carry__1_n_5\,
      I2 => \tdata_o2__0_carry__1_n_7\,
      I3 => tdata_o3(9),
      I4 => \tdata_o2__0_carry__1_n_4\,
      O => \tdata_o[8]_INST_0_i_2_n_0\
    );
\tdata_o[8]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00001F00"
    )
        port map (
      I0 => \tdata_o2__0_carry__1_n_6\,
      I1 => \tdata_o2__0_carry__1_n_7\,
      I2 => \tdata_o2__0_carry__1_n_5\,
      I3 => \tdata_o2__0_carry__1_n_4\,
      I4 => tdata_o3(9),
      I5 => \tdata_o2__27_carry__0_n_1\,
      O => \tdata_o[8]_INST_0_i_3_n_0\
    );
tlast_o_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020000000000"
    )
        port map (
      I0 => \width_s_reg_n_0_[10]\,
      I1 => \width_s_reg_n_0_[9]\,
      I2 => \width_s_reg_n_0_[8]\,
      I3 => \width_s_reg_n_0_[1]\,
      I4 => \width_s_reg_n_0_[0]\,
      I5 => tlast_o_i_2_n_0,
      O => \__2\
    );
tlast_o_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \width_s_reg_n_0_[4]\,
      I1 => \width_s_reg_n_0_[5]\,
      I2 => \width_s_reg_n_0_[2]\,
      I3 => \width_s_reg_n_0_[3]\,
      I4 => \width_s_reg_n_0_[7]\,
      I5 => \width_s_reg_n_0_[6]\,
      O => tlast_o_i_2_n_0
    );
tlast_o_reg: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => tuser_o_i_2_n_0,
      D => \__2\,
      Q => tlast_o
    );
tuser_o_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => tuser_o_i_3_n_0,
      I1 => height_s(4),
      I2 => tuser_o_i_4_n_0,
      I3 => tuser_o_i_5_n_0,
      I4 => tuser_o_i_6_n_0,
      O => tuser_o_i_1_n_0
    );
tuser_o_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rst_i,
      O => tuser_o_i_2_n_0
    );
tuser_o_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FDFFFFFF"
    )
        port map (
      I0 => height_s(9),
      I1 => height_s(5),
      I2 => height_s(8),
      I3 => height_s(7),
      I4 => height_s(6),
      O => tuser_o_i_3_n_0
    );
tuser_o_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \width_s_reg_n_0_[10]\,
      I1 => \width_s_reg_n_0_[9]\,
      I2 => \width_s_reg_n_0_[8]\,
      O => tuser_o_i_4_n_0
    );
tuser_o_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => height_s(2),
      I1 => height_s(0),
      I2 => height_s(1),
      I3 => height_s(3),
      O => tuser_o_i_5_n_0
    );
tuser_o_i_6: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \width_s_reg_n_0_[6]\,
      I1 => \width_s[7]_i_2_n_0\,
      I2 => \width_s_reg_n_0_[7]\,
      O => tuser_o_i_6_n_0
    );
tuser_o_reg: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => tuser_o_i_2_n_0,
      D => tuser_o_i_1_n_0,
      Q => tuser_o
    );
tvalid_o_reg: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => '1',
      CLR => tuser_o_i_2_n_0,
      D => '1',
      Q => tvalid_o
    );
\width_s[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \width_s_reg_n_0_[0]\,
      O => width_s(0)
    );
\width_s[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA68"
    )
        port map (
      I0 => \width_s_reg_n_0_[10]\,
      I1 => \width_s_reg_n_0_[9]\,
      I2 => \width_s_reg_n_0_[8]\,
      I3 => tuser_o_i_6_n_0,
      O => width_s(10)
    );
\width_s[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55545555AAAAAAAA"
    )
        port map (
      I0 => \width_s_reg_n_0_[0]\,
      I1 => \width_s[1]_i_2_n_0\,
      I2 => \width_s[1]_i_3_n_0\,
      I3 => \width_s_reg_n_0_[8]\,
      I4 => \width_s_reg_n_0_[10]\,
      I5 => \width_s_reg_n_0_[1]\,
      O => width_s(1)
    );
\width_s[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \width_s_reg_n_0_[7]\,
      I1 => \width_s_reg_n_0_[0]\,
      I2 => \width_s_reg_n_0_[5]\,
      I3 => \width_s_reg_n_0_[6]\,
      O => \width_s[1]_i_2_n_0\
    );
\width_s[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => \width_s_reg_n_0_[3]\,
      I1 => \width_s_reg_n_0_[4]\,
      I2 => \width_s_reg_n_0_[2]\,
      I3 => \width_s_reg_n_0_[9]\,
      O => \width_s[1]_i_3_n_0\
    );
\width_s[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \width_s_reg_n_0_[1]\,
      I1 => \width_s_reg_n_0_[0]\,
      I2 => \width_s_reg_n_0_[2]\,
      O => width_s(2)
    );
\width_s[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \width_s_reg_n_0_[2]\,
      I1 => \width_s_reg_n_0_[0]\,
      I2 => \width_s_reg_n_0_[1]\,
      I3 => \width_s_reg_n_0_[3]\,
      O => width_s(3)
    );
\width_s[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \width_s_reg_n_0_[3]\,
      I1 => \width_s_reg_n_0_[1]\,
      I2 => \width_s_reg_n_0_[0]\,
      I3 => \width_s_reg_n_0_[2]\,
      I4 => \width_s_reg_n_0_[4]\,
      O => width_s(4)
    );
\width_s[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \width_s_reg_n_0_[4]\,
      I1 => \width_s_reg_n_0_[2]\,
      I2 => \width_s_reg_n_0_[0]\,
      I3 => \width_s_reg_n_0_[1]\,
      I4 => \width_s_reg_n_0_[3]\,
      I5 => \width_s_reg_n_0_[5]\,
      O => width_s(5)
    );
\width_s[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \width_s[7]_i_2_n_0\,
      I1 => \width_s_reg_n_0_[6]\,
      O => width_s(6)
    );
\width_s[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \width_s_reg_n_0_[6]\,
      I1 => \width_s[7]_i_2_n_0\,
      I2 => \width_s_reg_n_0_[7]\,
      O => width_s(7)
    );
\width_s[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \width_s_reg_n_0_[4]\,
      I1 => \width_s_reg_n_0_[2]\,
      I2 => \width_s_reg_n_0_[0]\,
      I3 => \width_s_reg_n_0_[1]\,
      I4 => \width_s_reg_n_0_[3]\,
      I5 => \width_s_reg_n_0_[5]\,
      O => \width_s[7]_i_2_n_0\
    );
\width_s[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA51"
    )
        port map (
      I0 => tuser_o_i_6_n_0,
      I1 => \width_s_reg_n_0_[10]\,
      I2 => \width_s_reg_n_0_[9]\,
      I3 => \width_s_reg_n_0_[8]\,
      O => width_s(8)
    );
\width_s[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \width_s_reg_n_0_[8]\,
      I1 => tuser_o_i_6_n_0,
      I2 => \width_s_reg_n_0_[9]\,
      O => width_s(9)
    );
\width_s_reg[0]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(0),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[0]\
    );
\width_s_reg[10]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(10),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[10]\
    );
\width_s_reg[1]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(1),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[1]\
    );
\width_s_reg[2]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(2),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[2]\
    );
\width_s_reg[3]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(3),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[3]\
    );
\width_s_reg[4]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(4),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[4]\
    );
\width_s_reg[5]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(5),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[5]\
    );
\width_s_reg[6]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(6),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[6]\
    );
\width_s_reg[7]\: unisim.vcomponents.FDPE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      D => width_s(7),
      PRE => tuser_o_i_2_n_0,
      Q => \width_s_reg_n_0_[7]\
    );
\width_s_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      CLR => tuser_o_i_2_n_0,
      D => width_s(8),
      Q => \width_s_reg_n_0_[8]\
    );
\width_s_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => pixel_clk_i,
      CE => tready_i,
      CLR => tuser_o_i_2_n_0,
      D => width_s(9),
      Q => \width_s_reg_n_0_[9]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_test_pattern_generat_0_0 is
  port (
    pixel_clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    tready_i : in STD_LOGIC;
    tdata_o : out STD_LOGIC_VECTOR ( 23 downto 0 );
    tuser_o : out STD_LOGIC;
    tlast_o : out STD_LOGIC;
    tvalid_o : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_test_pattern_generat_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_test_pattern_generat_0_0 : entity is "design_1_test_pattern_generat_0_0,test_pattern_generator,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_test_pattern_generat_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_test_pattern_generat_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of design_1_test_pattern_generat_0_0 : entity is "test_pattern_generator,Vivado 2019.2";
end design_1_test_pattern_generat_0_0;

architecture STRUCTURE of design_1_test_pattern_generat_0_0 is
  signal \^tdata_o\ : STD_LOGIC_VECTOR ( 23 downto 7 );
begin
  tdata_o(23) <= \^tdata_o\(23);
  tdata_o(22) <= \^tdata_o\(23);
  tdata_o(21) <= \^tdata_o\(23);
  tdata_o(20) <= \^tdata_o\(23);
  tdata_o(19) <= \^tdata_o\(23);
  tdata_o(18) <= \^tdata_o\(23);
  tdata_o(17) <= \^tdata_o\(23);
  tdata_o(16) <= \^tdata_o\(23);
  tdata_o(15) <= \^tdata_o\(15);
  tdata_o(14) <= \^tdata_o\(15);
  tdata_o(13) <= \^tdata_o\(15);
  tdata_o(12) <= \^tdata_o\(15);
  tdata_o(11) <= \^tdata_o\(15);
  tdata_o(10) <= \^tdata_o\(15);
  tdata_o(9) <= \^tdata_o\(15);
  tdata_o(8) <= \^tdata_o\(15);
  tdata_o(7) <= \^tdata_o\(7);
  tdata_o(6) <= \^tdata_o\(7);
  tdata_o(5) <= \^tdata_o\(7);
  tdata_o(4) <= \^tdata_o\(7);
  tdata_o(3) <= \^tdata_o\(7);
  tdata_o(2) <= \^tdata_o\(7);
  tdata_o(1) <= \^tdata_o\(7);
  tdata_o(0) <= \^tdata_o\(7);
U0: entity work.design_1_test_pattern_generat_0_0_test_pattern_generator
     port map (
      pixel_clk_i => pixel_clk_i,
      rst_i => rst_i,
      tdata_o(2) => \^tdata_o\(23),
      tdata_o(1) => \^tdata_o\(15),
      tdata_o(0) => \^tdata_o\(7),
      tlast_o => tlast_o,
      tready_i => tready_i,
      tuser_o => tuser_o,
      tvalid_o => tvalid_o
    );
end STRUCTURE;
