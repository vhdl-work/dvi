--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Fri Jul 17 19:48:44 2020
--Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    HDMI_en : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_led_o : out STD_LOGIC;
    dvi_clk_o_n : out STD_LOGIC;
    dvi_clk_o_p : out STD_LOGIC;
    dvi_o_n : out STD_LOGIC_VECTOR ( 2 downto 0 );
    dvi_o_p : out STD_LOGIC_VECTOR ( 2 downto 0 );
    pixel_clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    rst_led_o : out STD_LOGIC;
    tuser_led_o : out STD_LOGIC;
    valid_led_o : out STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    rst : in STD_LOGIC;
    dvi_o_p : out STD_LOGIC_VECTOR ( 2 downto 0 );
    dvi_o_n : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HDMI_en : out STD_LOGIC_VECTOR ( 0 to 0 );
    pixel_clk : in STD_LOGIC;
    dvi_clk_o_n : out STD_LOGIC;
    dvi_clk_o_p : out STD_LOGIC;
    valid_led_o : out STD_LOGIC;
    data_en_led_o : out STD_LOGIC;
    tuser_led_o : out STD_LOGIC;
    rst_led_o : out STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      HDMI_en(0) => HDMI_en(0),
      data_en_led_o => data_en_led_o,
      dvi_clk_o_n => dvi_clk_o_n,
      dvi_clk_o_p => dvi_clk_o_p,
      dvi_o_n(2 downto 0) => dvi_o_n(2 downto 0),
      dvi_o_p(2 downto 0) => dvi_o_p(2 downto 0),
      pixel_clk => pixel_clk,
      rst => rst,
      rst_led_o => rst_led_o,
      tuser_led_o => tuser_led_o,
      valid_led_o => valid_led_o
    );
end STRUCTURE;
