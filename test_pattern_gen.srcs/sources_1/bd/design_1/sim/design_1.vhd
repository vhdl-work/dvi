--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Fri Jul 17 19:48:43 2020
--Host        : eric-N551JX running 64-bit Ubuntu 20.04 LTS
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  port (
    HDMI_en : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_en_led_o : out STD_LOGIC;
    dvi_clk_o_n : out STD_LOGIC;
    dvi_clk_o_p : out STD_LOGIC;
    dvi_o_n : out STD_LOGIC_VECTOR ( 2 downto 0 );
    dvi_o_p : out STD_LOGIC_VECTOR ( 2 downto 0 );
    pixel_clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    rst_led_o : out STD_LOGIC;
    tuser_led_o : out STD_LOGIC;
    valid_led_o : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=6,numReposBlks=6,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=3,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_clk_wiz_0_0 is
  port (
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    locked : out STD_LOGIC
  );
  end component design_1_clk_wiz_0_0;
  component design_1_xlconstant_0_0 is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_xlconstant_0_0;
  component design_1_xlconstant_0_1 is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_xlconstant_0_1;
  component design_1_rgb2dvi_0_0 is
  port (
    TMDS_Clk_p : out STD_LOGIC;
    TMDS_Clk_n : out STD_LOGIC;
    TMDS_Data_p : out STD_LOGIC_VECTOR ( 2 downto 0 );
    TMDS_Data_n : out STD_LOGIC_VECTOR ( 2 downto 0 );
    aRst : in STD_LOGIC;
    aRst_n : in STD_LOGIC;
    vid_pData : in STD_LOGIC_VECTOR ( 23 downto 0 );
    vid_pVDE : in STD_LOGIC;
    vid_pHSync : in STD_LOGIC;
    vid_pVSync : in STD_LOGIC;
    PixelClk : in STD_LOGIC;
    SerialClk : in STD_LOGIC
  );
  end component design_1_rgb2dvi_0_0;
  component design_1_test_pattern_generat_0_0 is
  port (
    pixel_clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    tready_i : in STD_LOGIC;
    tdata_o : out STD_LOGIC_VECTOR ( 23 downto 0 );
    tuser_o : out STD_LOGIC;
    tlast_o : out STD_LOGIC;
    tvalid_o : out STD_LOGIC
  );
  end component design_1_test_pattern_generat_0_0;
  component design_1_time_gen_0_0 is
  port (
    pixel_clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    tdata_i : in STD_LOGIC_VECTOR ( 23 downto 0 );
    tuser_i : in STD_LOGIC;
    tlast_i : in STD_LOGIC;
    tvalid_i : in STD_LOGIC;
    tdata_o : out STD_LOGIC_VECTOR ( 23 downto 0 );
    hsync_o : out STD_LOGIC;
    vsync_o : out STD_LOGIC;
    data_en_o : out STD_LOGIC;
    tready_o : out STD_LOGIC
  );
  end component design_1_time_gen_0_0;
  signal clk_wiz_0_clk_out1 : STD_LOGIC;
  signal clk_wiz_0_clk_out2 : STD_LOGIC;
  signal pixel_clk_1 : STD_LOGIC;
  signal rgb2dvi_0_TMDS_Clk_n : STD_LOGIC;
  signal rgb2dvi_0_TMDS_Clk_p : STD_LOGIC;
  signal rgb2dvi_0_TMDS_Data_n : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal rgb2dvi_0_TMDS_Data_p : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal rst_1 : STD_LOGIC;
  signal test_pattern_generat_0_tdata_o : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal test_pattern_generat_0_tlast_o : STD_LOGIC;
  signal test_pattern_generat_0_tuser_o : STD_LOGIC;
  signal test_pattern_generat_0_tvalid_o : STD_LOGIC;
  signal time_gen_0_data_en_o : STD_LOGIC;
  signal time_gen_0_hsync_o : STD_LOGIC;
  signal time_gen_0_tdata_o : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal time_gen_0_tready_o : STD_LOGIC;
  signal time_gen_0_vsync_o : STD_LOGIC;
  signal xlconstant_0_dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xlconstant_1_dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_clk_wiz_0_locked_UNCONNECTED : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of pixel_clk : signal is "xilinx.com:signal:clock:1.0 CLK.PIXEL_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of pixel_clk : signal is "XIL_INTERFACENAME CLK.PIXEL_CLK, CLK_DOMAIN design_1_pixel_clk, FREQ_HZ 50000000, INSERT_VIP 0, PHASE 0.000";
begin
  HDMI_en(0) <= xlconstant_1_dout(0);
  data_en_led_o <= time_gen_0_data_en_o;
  dvi_clk_o_n <= rgb2dvi_0_TMDS_Clk_n;
  dvi_clk_o_p <= rgb2dvi_0_TMDS_Clk_p;
  dvi_o_n(2 downto 0) <= rgb2dvi_0_TMDS_Data_n(2 downto 0);
  dvi_o_p(2 downto 0) <= rgb2dvi_0_TMDS_Data_p(2 downto 0);
  pixel_clk_1 <= pixel_clk;
  rst_1 <= rst;
  rst_led_o <= rst_1;
  tuser_led_o <= test_pattern_generat_0_tuser_o;
  valid_led_o <= test_pattern_generat_0_tvalid_o;
clk_wiz_0: component design_1_clk_wiz_0_0
     port map (
      clk_in1 => pixel_clk_1,
      clk_out1 => clk_wiz_0_clk_out1,
      clk_out2 => clk_wiz_0_clk_out2,
      locked => NLW_clk_wiz_0_locked_UNCONNECTED,
      resetn => xlconstant_1_dout(0)
    );
rgb2dvi_0: component design_1_rgb2dvi_0_0
     port map (
      PixelClk => clk_wiz_0_clk_out1,
      SerialClk => clk_wiz_0_clk_out2,
      TMDS_Clk_n => rgb2dvi_0_TMDS_Clk_n,
      TMDS_Clk_p => rgb2dvi_0_TMDS_Clk_p,
      TMDS_Data_n(2 downto 0) => rgb2dvi_0_TMDS_Data_n(2 downto 0),
      TMDS_Data_p(2 downto 0) => rgb2dvi_0_TMDS_Data_p(2 downto 0),
      aRst => xlconstant_0_dout(0),
      aRst_n => xlconstant_1_dout(0),
      vid_pData(23 downto 0) => time_gen_0_tdata_o(23 downto 0),
      vid_pHSync => time_gen_0_hsync_o,
      vid_pVDE => time_gen_0_data_en_o,
      vid_pVSync => time_gen_0_vsync_o
    );
test_pattern_generat_0: component design_1_test_pattern_generat_0_0
     port map (
      pixel_clk_i => clk_wiz_0_clk_out1,
      rst_i => rst_1,
      tdata_o(23 downto 0) => test_pattern_generat_0_tdata_o(23 downto 0),
      tlast_o => test_pattern_generat_0_tlast_o,
      tready_i => time_gen_0_tready_o,
      tuser_o => test_pattern_generat_0_tuser_o,
      tvalid_o => test_pattern_generat_0_tvalid_o
    );
time_gen_0: component design_1_time_gen_0_0
     port map (
      data_en_o => time_gen_0_data_en_o,
      hsync_o => time_gen_0_hsync_o,
      pixel_clk_i => clk_wiz_0_clk_out1,
      rst_i => rst_1,
      tdata_i(23 downto 0) => test_pattern_generat_0_tdata_o(23 downto 0),
      tdata_o(23 downto 0) => time_gen_0_tdata_o(23 downto 0),
      tlast_i => test_pattern_generat_0_tlast_o,
      tready_o => time_gen_0_tready_o,
      tuser_i => test_pattern_generat_0_tuser_o,
      tvalid_i => test_pattern_generat_0_tvalid_o,
      vsync_o => time_gen_0_vsync_o
    );
xlconstant_0: component design_1_xlconstant_0_0
     port map (
      dout(0) => xlconstant_0_dout(0)
    );
xlconstant_1: component design_1_xlconstant_0_1
     port map (
      dout(0) => xlconstant_1_dout(0)
    );
end STRUCTURE;
