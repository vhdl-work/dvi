# Test Pattern Generator
This project aims at producing a video using an ALINX 7035 FPGA dev board (Artix-7 XC7A35T FGG484-1) through a DVI port. We followed the CEA-861D (http://read.pudn.com/downloads222/doc/1046129/CEA861D.pdf) format for video timing and the default values are set for 60Hz 1280 x 720 resolution. We have used the rgb2dvi ip written by digilent (https://github.com/Digilent/vivado-library/tree/master/ip/rgb2dvi). It is developed using Vivado.

# Design
This test pattern generator generates a coloured checkered pattern moving from bottom right to top left of the monitor. The video source follows the AXI-Stream video protocol. These video data are process by our timing generator which follows the CEA861-D protocol. The data is then tranferred to the rgb2dvi module which serialised the data following the TMDS protocol (DDR). Since we are doing a 60Hz video, the pixel clock is 74.25Hz and the serial clock is 371.25Hz (pixel_clk x5). We have chosen not to generate the serial clock inside the rgb2dvi module.

# Source Files
All the source files can be found in the `/test_pattern_gen.srcs/sources_1/new` directory. The following files are written by us:
- tpg.vhd  
This file generates the desired 24-bit colour data output following the AXI-stream video protocol. Default resolution is 1280x720 and the pattern is a coloured checkered board (RBG and Black) moving from bottom right to top left corner.
- time_gen.vhd
This file put the video data generated by `tpg.vhd` into the correct video timing format (follows CEA861-D protocol). Default settings are tailored for 1280x720p. 
- time_gen/time_gen.vhd
This is also a timing generator. It is written differently and are also tested to work with this board. It also follows the CEA861-D protocol.

We have tried to write our own TMDS encoder but we encounter difficulties in serialising the data. We have at the end chosen to use the provided `rgb2dvi` ip in examples to convert our 24-bit colour data to serialised 10-bit TMDS data. The following files are part of the `rgb2dvi` ip written by digilent. For more details, see (https://github.com/Digilent/vivado-library/tree/master/ip/rgb2dvi).
- ClockGen.vhd
- DVI_Constants.vhd
- OutputSERDES.vhd
- SyncAsync.vhd
- SyncAsyncReset.vhd
- TMDS_Encoder.vhd
- rgb2dvi.vhd

# Block Diagram
These ip's are connected in the diagram in the `/test_pattern_gen.srcs/sources_1/bd` directory.

# Simulations
There are two simulation provided in the  `/test_pattern_gen.srcs/sim_1/new` directory, with one testing `tpg.vhd` itself and the other testing when both `tpg.vhd` and `time_gen.vhd` are connected.

# Constraints
The constraints can be found in the  `/test_pattern_gen.srcs/constrs_1/new` directory. The pins are tailored for the ALINX 7035 FPGA dev board.

